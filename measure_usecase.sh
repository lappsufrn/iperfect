#!/bin/bash
#Remember to check /root/.ros/log/
###results folder
export RESULTS_IPE_PATH=/home/jetsonuob/Projects/hmp_jetson_results

roscore &> /dev/null&
pid=$!
sleep 3

./measurement/runtime_measurement.sh --parse-parameters json/rosusecase_mm1024_pareto.json
./measurement/runtime_measurement.sh --parse-parameters json/rosusecase_mm2048_pareto.json
./measurement/runtime_measurement.sh --parse-parameters json/tx2_ros_usecase.json
kill -9 $pid &> /dev/null&
pkill rosmaster
pkill rosout

#sleep 3
#./measurement/runtime_measurement.sh --parse-parameters json/tx2_mm_2048_pareto.json
