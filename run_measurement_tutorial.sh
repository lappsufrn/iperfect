#!/bin/bash

###results folder
#export RESULTS_IPE_PATH=/home/demetrios/Projects/tutorial_results 
export RESULTS_IPE_PATH=/home/demetrios/Projects/tutorial_results

###Run p3arsec without nornir
LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/home/demetrios/Projects/nornir/build/src/mammut_repo-prefix/src/mammut_repo-build/src/"
##change this according with the gcc type
LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/home/demetrios/Projects/p3arsec/pkgs/libs/hooks/inst/arm-linux.gcc-openmp/lib/"
##Nornir
LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/home/demetrios/Projects/nornir/build/src"
LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/home/demetrios/Projects/nornir/build/src/riff_repo-prefix/src/riff_repo-build/src"
export MAMMUT_SMARTPOWER2_PATH="/dev/ttyUSB0"
#export
export LD_LIBRARY_PATH
#P3ARSEC
export PARSECDIR=/home/demetrios/Projects/p3arsec

#./measurement/runtime_measurement.sh --parse-parameters json/mm_darknet_power_tutorial.json 
#./measurement/runtime_measurement.sh --parse-parameters json/roi_blackscholes_tutorial.json 

./measurement/runtime_measurement.sh --parse-parameters json/nornir_black_tutorial.json 
