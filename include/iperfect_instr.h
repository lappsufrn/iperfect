#ifndef _IPERFECT_INSTR_H_
#define _IPERFECT_INSTR_H_

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <signal.h>


long long sRoiTime;
char * sensors[5]; 
char * spusb[4]; 
char roitime_file[255];
pid_t pids[2];


void allocate_array_of_chars(char ** alocate,int elements,int size){
  for (int i = 0; i < elements; ++i){
  	alocate[i] = malloc (size);
    if (!alocate[i]) {    /* validate memory was allocated -- every time */
      printf ("error: i allocation failed, exiting.");
      exit (1);
    }
  }
}

void free_array(char ** free_chars,int elements){
  for (int i = 0; i < elements; ++i)
    free(free_chars[i]);
}

void get_args(char *buff,char ** args){
	int i=0;
	
	char * ptr = strtok (buff," ");
	while(ptr!= NULL) {
		//printf("%s %d\n",ptr,strlen(ptr)+1 );
		memcpy(args[i], ptr,strlen(ptr)+1);
		//printf("%d (%s)\n",i,args[i] );
		ptr = strtok (NULL, " ");
		i++;
	}
	
	args[++i] = NULL;
}

void get_line(FILE* file, char * buff){
  
	if(fgets (buff , 255 , file) == NULL ){
		printf("error to read line\n");
		exit(1);
	}

	buff[strcspn(buff, "\n")] = 0;
}

void do_fork(pid_t *pid, char**args){
	switch(*pid = fork()){
		case -1:
			printf ("fork error %s\n",args[0]);
			exit(1);
			break;
		case 0:
			/* This is processed by the child */		
			execv(args[0], args);//first is the path of the app
			//printf("execvp error (%s) (%s) (%s) (%s)\n",args[0],args[1],args[2],args[3]);//if print this it means error
			exit(1);
			break;
		default:
			/* This is processed by the parent */
			break;
	}
}

long long get_time (void){
    long            ms; // Microseconds
    time_t          s;  // Seconds
    struct timespec spec;

    clock_gettime(CLOCK_MONOTONIC, &spec);

    s  = spec.tv_sec;
    ms = round(spec.tv_nsec / 1.0e3); // Convert nanoseconds to microseconds
    if (ms > 999999) {
        s++;
        ms = 0;
    }

    long long st = (long long) s;
    
    return (st*1000000LL) + ms;
}

void save_roi_time(long long eRoiTime){
	FILE *roiOutput = fopen (roitime_file,"w");
	if(roiOutput == NULL) printf("save_roi_time - Error!");   
	fprintf(roiOutput, "Time(s),%f\n",(eRoiTime-sRoiTime)/(double)1000000);
	fclose(roiOutput);
}

void iperfect_roi_init(){
	printf("iperfect_roi_init\n");
	
	allocate_array_of_chars(sensors,5,255);

	FILE *info_file;
	char buff[255];
	int sensors_n=1;

   	if ((info_file = fopen("/tmp/sensors_info","r")) == NULL){
    	printf("Error! /tmp/sensors_info");
    	exit(1);
   	}
 	
	fscanf (info_file, "%d\n", &sensors_n);//number of sensors exec

	get_line(info_file, buff);//first sensor_xu3_tx2
	get_args(buff,sensors);

	if (sensors_n > 1 ){//if xu3 then load sp_usb exec
		allocate_array_of_chars(spusb,4,255);
		get_line(info_file, buff);
		get_args(buff,spusb);
	}

	get_line(info_file, buff);//get roitime file path
	memcpy(roitime_file, buff,strlen(buff)+1);
	
	fclose(info_file);
}

void iperfect_roi_begin(){
	sRoiTime = get_time();
	printf("iperfect_roi_begin\n");

 	if (*spusb){
 		//printf("begin - (%s) (%s) (%s) (%s)\n",spusb[0],spusb[1],spusb[2],spusb[3] );
 		do_fork(&pids[1],spusb);
 	}

 	//printf("begin - (%s) (%s) (%s) (%s) (%s)\n",sensors[0],sensors[1],sensors[2],sensors[3],sensors[4] );
 	do_fork(&pids[0],sensors);
}

void iperfect_roi_end(){
	long long eRoiTime = get_time();
	printf("iperfect_roi_end\n");
	
	kill(pids[0], SIGKILL);
	if (pids[1]) kill(pids[1], SIGKILL);
	
	save_roi_time(eRoiTime);	
	
	free_array(sensors,4);
	if (pids[1]) free_array(spusb,3);
}

#endif //_IPERFECT_INSTR_H_
