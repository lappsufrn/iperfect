# IPerfEcT

Identifying heterogeneous multi-processing configurations for parallel Performance and whole Energy device consumption Trade-Offs - IPerfEcT.

# Required Packages

- **cpuset** is a python application that forms a wrapper around the
standard Linux filesystem calls to make using the cpusets facilities
in the Linux kernel easier. You can install it using `sudo apt-get install cpuset`.
- **cpufrequtils**  is used to control the CPU frequency scaling deamon. You can install it using `sudo apt-get install cpufrequtils`.
- **jq**  is used to parse a Json data format file. You can install it using `sudo apt-get install jq`.


# How to install

`git clone https://gitlab.com/lappsufrn/iperfect.git`<br />
`cd iperfect/power_sensor`<br />
`mkdir bin`<br />
`make`<br />
`make spusb`<br />

# More Information
Our tutorial slides can be found here: [https://gitlab.com/lappsufrn/iperfect-tutorial](https://gitlab.com/lappsufrn/iperfect-tutorial)  <br />

If you use IPerfEcT for scientific purposes, please cite our paper: [_Performance and Energy Trade-Offs for Parallel Applications on Heterogeneous Multi-Processing Systems_](https://www.mdpi.com/1996-1073/13/9/2409), Demetrios A. M. Coutinho, Daniele De Sensi, Arthur Francisco Lorenzon, Kyriakos Georgiou,Jose Nunez-Yanez, Kerstin Eder and Samuel Xavier-de-Souza.
