#!/bin/bash

ROI_FILE=/tmp/roi

IPERFECT_PATH="/home/demetrios/Projects/iperfect/"
sensors_bin="$IPERFECT_PATH/power_sensor/sensors"
check_aff="$IPERFECT_PATH/tools/check_aff.sh"
check_temp="$IPERFECT_PATH/tools/check_temp.sh"
sp_monitor="$IPERFECT_PATH/power_sensor/sp_monitor"

is_roifile_exists=false
milisecnds=$1
sensors_milisec=$2
result_path_n=$3
app=$4
sleep_time=$5

echo "Watch roi started"

echo " $milisecnds ; $sensors_milisec ; $result_path_n ; $app"

while true; do
	if [[ -f "$ROI_FILE" && "$is_roifile_exists" == "false" ]]; then

  	      echo "starting checkings" # use this two just as debugs
          $check_aff $app $milisecnds > "$result_path_n/aff.csv" &
          PID_aff=$!

          $check_temp $milisecnds > "$result_path_n/temp.csv" &
          PID_temp=$!

          echo "Read power"
          $

          echo "starting INA sensors"
          $sensors_bin 1 1 $sensors_milisec > "$result_path_n/sensors.csv" & #same as smartpower
          PID_sensors=$!	    

	    is_roifile_exists=true
	fi

	if [[ ! -f "$ROI_FILE" && "$is_roifile_exists" == "true" ]]; then

          echo "killing sensor"
          kill $PID_sensors > /dev/null
          disown
  	
  	      echo "kiling checkers"
          kill $PID_aff > /dev/null
  	      disown
          
  	      kill $PID_temp > /dev/null
  	      disown

  	      echo "killing monitor"
          kill 
          disown
		break
	fi

	sleep $sleep_time #time in secs
done