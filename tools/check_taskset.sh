#!/bin/bash
#echo "COMM PID TID PSR %CPU"

pids=$(pgrep $1) #get pids

IFS=$'\n' read -r -d '' -a pids_array <<< "$pids" # create a array of the pids

tids=$(ps -p ${pids_array[0]} -L -o tid:1 --no-headers) # get threads ids from 'blackscholes'
IFS=$'\n' read -r -d '' -a tids_array <<< "$tids"

echo "[$1] Pinning ${tids_array[0]} to core 4" # bind the first process 
taskset -p -c 4 ${tids_array[0]}

for i in $(seq 1 $((${#tids_array[@]}-1)));do # all the remaing threads to core 0
	echo "[blackscholes-Nornir] Pinning ${tids_array[$i]} to core 1"
	taskset -p -c 1 ${tids_array[$i]}
done

tids=$(ps -p ${pids_array[1]} -L -o tid:1 --no-headers)  #get threads ids from manager
IFS=$'\n' read -r -d '' -a tids_array <<< "$tids"

echo "[Nornir] Pinning ${pids_array[1]} to core 1"
taskset -p -a -c 1 ${pids_array[1]}

#for i in $(seq 0 $((${#tids_array[@]}-1)));do
#	echo "[Nornir] Pinning ${tids_array[$i]} to core 2"
#	taskset -p -c 2 ${tids_array[$i]}
#done


while true;
do 
	pid=$(pgrep $1)

	for i in $pid;do

		if [[ ! -z $i ]]
		then
		#ps -mo ipd,tid,fname,user,psr,pcpu  -p $pid --no-headers;
			ps -p $i -L -o comm,pid:1,tid:1,psr:1,pcpu:1 #--no-headers
			#echo ""
		#usage="${str}"
		#echo $str
		fi

	done
	sleep $2;

done

#echo $usage

#avg_total_runtime[$count]=$(python -c 'import statistics;import sys; print(statistics.median(sorted('$total_runtime'))/'$TIME_CONVERT'); sys.exit(0)')
