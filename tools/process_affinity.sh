#!/bin/bash
pthreads=$4
DEBUG=$5

while true;do
	roi=$(pgrep "sensors")

	if [ "$roi" == "" ]; then continue;fi

	IFS=', ' read -r -a CORES_ARRAY <<< "$1"
	# cores list - name of the app - sleep time
	[ -n "$DEBUG" ] && echo "AFFINITY $1 - $2 - $3 - $pthreads"

	pids="$(pgrep "$2")"
	[ -n "$DEBUG" ] && echo "1- $pids"
	tids=""
	
	while [ "$pthreads" == true ] && [ "$tids" = "" ];do
		tids=$(ps -T -p "$pids" -o tid:1 --no-headers)
		[ -n "$DEBUG" ] && echo "2- $tids"
	done

	if [ "$pthreads" == true ]; then
		IFS=$'\n' read -r -d '' -a PID_ARRAY <<< "$tids"
	else
		IFS=$'\n' read -r -d '' -a PID_ARRAY <<< "$pids"
	fi

	echo "PID_ARRAY (${PID_ARRAY[*]})"

	if [ -n "${PID_ARRAY[*]}" ]; then

		taskset -p -c -a "${CORES_ARRAY[0]}" "${PID_ARRAY[0]}"

		pid_total="${#PID_ARRAY[@]}"
		core_total="${#CORES_ARRAY[@]}"
		for ((i=1; i<pid_total; i=i+1)); do #threads
			index=$((i%core_total))
			taskset -p -c "${CORES_ARRAY[index]}" "${PID_ARRAY[$i]}"
		done

		exit 0
	fi

	sleep "$3"
done
