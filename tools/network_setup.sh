#!/bin/bash
#iptables -A OUTPUT -p udp  -o eth0 --dport 67 --sport 1024:65535 -j ACCEPT
pkill dhclient
pkill wpa_supplicant 
ip link set dev wlan0 down
ip addr flush dev wlan0
sleep 1
echo "wpa connecting"
wpa_supplicant -f /var/log/wpa_supplicant.log -B -Dnl80211 -iwlan0 -c $1
echo "get ip addr"
ip link set dev wlan0 up
ip link set wlan0 mode default
sleep 3
dhclient -v wlan0
