#!/bin/bash

core=$1
freq=$2
result=$3
sample_rate=$4

is_roifile_exists=false
t1=0
t2=0

while true;
do

	if [[ -f "/tmp/roi" && "$is_roifile_exists" == "false" ]]; then
	
		t1=$(date +'%s%N')

		echo "Entering ROI"
		echo "t1 $t1"
		#echo "Changing frequency to $freq in core $core"

		#cpufreq-set -d $freq -u $freq -c $core
		#cpufreq-info -o

   		is_roifile_exists=true
	fi;

	if [[ ! -f "/tmp/roi" && "$is_roifile_exists" == "true" ]]; then
		t2=$(date +'%s%N')
		echo "Out roi"
		echo "t2 $t2"
	#	echo -e "Time(s)\t$t1\t$t2" > "$result"
		break
	fi;

	sleep $sample_rate

done

echo -e "Time\t$t1\t$t2" > "$result"
