#!/bin/bash
#Montar de acordo com a arquitetura, o unico que eh diferente eh o gpu, pegue tb o emc
MACHINE=$1
KERNEL=$2

case "$MACHINE" in
  odroid)
    case "$KERNEL" in
      "3.1"*)
        GPU_FREQ_FOLDER="/sys/bus/platform/drivers/mali/11800000.mali/clock"
      ;;
      "4.1"*)
        GPU_FREQ_FOLDER="/sys/bus/platform/drivers/mali/11800000.mali/devfreq/devfreq0/cur_freq"
      ;;
    esac
  ;;
  tegra-ubuntu)
    GPU_FREQ_FOLDER="/sys/devices/17000000.gp10b/devfreq/17000000.gp10b/cur_freq"
    EMC_FREQ_FOLDER="/sys/kernel/debug/bpmp/debug/clk/emc/rate"
  ;;
esac

# Main infinite loop
while true; do
  
  CPU_FREQ=()
  TEMP=()

  # CPU Governor
  CPU_GOVERNOR=$(cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor)

  #----scaling frequency and virtual temp -----
  #scaling freq

  for folder in /sys/devices/system/cpu/cpu*
  do
    f="$folder/cpufreq/scaling_cur_freq"
    if [ -f "$f" ];then
      CPU_FREQ+=( $(($(cat "$f")/1000)) )
    fi
  done

  #virtual temp
  for folder in /sys/devices/virtual/thermal/thermal_zone*
  do
    f="$folder/temp"
    if [ -f "$f" ];then
      TEMP+=( $(($(cat "$f")/1000)) )
    fi
  #   if (( ${TEMP[i]} > 100000 )); then
  #   		kill $2
  #   fi
  done
  
  GPU_FREQ=$(cat "$GPU_FREQ_FOLDER")

  if [ -n "$EMC_FREQ_FOLDER" ];then
    EMC_FREQ=$(($(cat "$EMC_FREQ_FOLDER")/1000000))
  else
    EMC_FREQ="-"
  fi

  OUTPUT="$(date +'%s%N')\t1"

  for cpu in "${CPU_FREQ[@]}";do 
    OUTPUT+="\t${cpu}"
  done

  OUTPUT+="\t$GPU_FREQ"
  OUTPUT+="\t$CPU_GOVERNOR"
  OUTPUT+="\t$EMC_FREQ"

  for t in "${TEMP[@]}";do 
    OUTPUT+="\t${t}"
  done   

  if [ -z "$ONCE" ]; then
       
    HEADER="timestamp\tVIRTUAL_SCALING"

    #for i in "${!CPU_FREQ[@]}"; do
    #  HEADER+="\tCPU${i}_F"
    #done
  for folder in /sys/devices/system/cpu/cpu*
  do
    f="$folder/cpufreq/scaling_cur_freq"
    if [ -f "$f" ];then
     HEADER+="\tCPU${folder: -1}_F"
    fi
  done

    HEADER+="\tGPU_FREQ\tCPU_GOVERNOR\tEMC_F"

    #for i in "${!TEMP[@]}"; do
    #  HEADER+="\tTEMP${i}"
    #done

    for folder in /sys/devices/virtual/thermal/thermal_zone*
    do
      f="$folder/temp"
      HEADER+="\tTEMP${folder: -1}"
    done    
  
    echo -e "$HEADER"
    ONCE=1
  fi

  echo -e "$OUTPUT"

  #----hardware frequency and class temp -----
  #cpuinfo freq

  CPU_FREQ=()
  for folder in /sys/devices/system/cpu/cpu*
  do
    f="$folder/cpufreq/cpuinfo_cur_freq"
    if [ -f "$f" ];then
      CPU_FREQ+=( $(($(cat "$f")/1000)) )
    fi
  done

  TEMP=()
  #class temp
  for folder in /sys/class/thermal/thermal_zone*
  do       
    f="$folder/temp"
    if [ -f "$f" ];then
      TEMP+=( $(($(cat "$f")/1000)) )
    # if (( ${TEMP[i]} > 100000 )); then
    # 		kill $2
    fi
  done

  OUTPUT="$(date +'%s%N') \t0"

  for cpu in "${CPU_FREQ[@]}";do 
    OUTPUT+="\t${cpu}"
  done

  OUTPUT+="\t$GPU_FREQ"
  OUTPUT+="\t$CPU_GOVERNOR"
  OUTPUT+="\t$EMC_FREQ"

  for t in "${TEMP[@]}";do 
    OUTPUT+="\t${t}"
  done   

  echo -e "$OUTPUT"

  sleep "$3"
done
