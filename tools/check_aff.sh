#!/bin/bash
#echo "COMM PID TID PSR %CPU"

apps=$1

while true;
do 
	for app in ${apps[*]}; do
		pid=$(pgrep "$app")
	
		for i in $pid;do

			if [ -n "$i" ]; then
				#ps -mo ipd,tid,fname,user,psr,pcpu  -p $pid --no-headers;
				ps -p "$i" -L -o comm,pid:1,tid:1,psr:1,pcpu:1 --no-headers
			fi

		done
	done	
	sleep "$2";
done
