/*
* Based on powprofiler project.
*/
#include "sensors.h"
#include <string>

#ifndef TX2SENSORS_H
#define TX2SENSORS_H

#define BUFFER_SIZE     31
#define TOTAL_MONITORS  5

#define MAIN_MODULE_IN  "/sys/bus/i2c/drivers/ina3221x/0-0041/iio_device/in_power0_input"
#define CPU_POWER_RAIL  "/sys/bus/i2c/drivers/ina3221x/0-0041/iio_device/in_power1_input"
#define CPU_VOLTA_RAIL  "/sys/bus/i2c/drivers/ina3221x/0-0041/iio_device/in_voltage1_input"
#define CPU_CURRE_RAIL  "/sys/bus/i2c/drivers/ina3221x/0-0041/iio_device/in_current1_input"
#define GPU_POWER_RAIL  "/sys/bus/i2c/drivers/ina3221x/0-0040/iio_device/in_power0_input"

enum    {
    TOTAL_POWER = 0,
    CPU_VOLTA,
    CPU_CURRE,
    CPU_POWER,
    GPU_POWER
};

class TX2Sensors : public Sensors{

private:
    int file_descriptors[TOTAL_MONITORS];
public:
    TX2Sensors();

    ~TX2Sensors(); 

    double* getSample();

	std::string getEMCFreq();

    void closeDescriptors();
};

#endif
