/*
* Based on powprofiler project.
*/
#ifndef SENSORS_H
#define SENSORS_H

#define CPU_NODE "/sys/devices/system/cpu/cpu"
#define THERMAL_ZONE "/sys/class/thermal/thermal_zone"

#include <string>

class Sensors {

protected:
	std::string gpuFreqNode;
public:

    std::string getCPUFreq(int i);
    std::string getCPUGovernor(int i);
    std::string getGPUFreq();
    std::string getThermalZone(int i);

};

#endif