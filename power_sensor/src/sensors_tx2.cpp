/*
* Based on powprofiler project.
*/
#include "../include/sensors_tx2.h"

#include <sys/fcntl.h>
#include <stdexcept>
#include <unistd.h>
#include <cstdlib>

using std::runtime_error;

TX2Sensors::TX2Sensors() {
    file_descriptors[TOTAL_POWER] = open(MAIN_MODULE_IN, O_RDONLY | O_NONBLOCK);
    file_descriptors[CPU_VOLTA] = open(CPU_VOLTA_RAIL, O_RDONLY | O_NONBLOCK); 
    file_descriptors[CPU_CURRE] = open(CPU_CURRE_RAIL, O_RDONLY | O_NONBLOCK); 
    file_descriptors[CPU_POWER] = open(CPU_POWER_RAIL, O_RDONLY | O_NONBLOCK); 
    file_descriptors[GPU_POWER] = open(GPU_POWER_RAIL, O_RDONLY | O_NONBLOCK); 
}

TX2Sensors::~TX2Sensors() {
    closeDescriptors();
}

void TX2Sensors::closeDescriptors() {
    close(file_descriptors[TOTAL_POWER]);
    close(file_descriptors[CPU_VOLTA]);
    close(file_descriptors[CPU_CURRE]);
    close(file_descriptors[CPU_POWER]);
    close(file_descriptors[GPU_POWER]);
}

double* TX2Sensors::getSample() {
    int i, bytes_read;
    char buffer[BUFFER_SIZE];

    double * power_sample = new double[TOTAL_MONITORS];

    if (file_descriptors[TOTAL_POWER] * file_descriptors[CPU_VOLTA] * file_descriptors[CPU_CURRE] * file_descriptors[CPU_POWER] * file_descriptors[GPU_POWER] < 0)
        throw runtime_error("unable to open file descriptor");

    for (i = 0; i < TOTAL_MONITORS; i++) {
        lseek(file_descriptors[i], 0, 0);
        if ((bytes_read = read(file_descriptors[i], buffer, BUFFER_SIZE + 1)) > 0) {
            buffer[bytes_read] = 0;
            power_sample[i] = (strtod(buffer, NULL) / 1000);
        } else
            throw runtime_error("unable to get data from file descriptor");
    } 

    return power_sample;
}
