#define _POSIX_C_SOURCE 200809L

#include "../include/sensors_tx2.h"
#include "../include/getnode.h"
#include <inttypes.h>
#include <math.h>
#include <stdio.h>
#include <errno.h>
#include <fstream>
#include <time.h>
#include <csignal>
#include <unistd.h>
#include <string>
#include <iostream>
#include <cstdlib>


using namespace std;

TX2Sensors tx2;
string machine;
GetNode* getNode;

void signal_handler(int sig){

    if (machine.compare("tegra-ubuntu") == 0)

    	tx2.closeDescriptors();
       
    exit(0);
}

void get_timestamp(timespec *ts,time_t *s,long *micro){
	*s  = ts->tv_sec * 1000000;
	*micro = (ts->tv_nsec / 1000);
	if (*micro > 999999) {
		(*s)++;
		*micro = 0;
	}
}

int main(int argc, char* argv[])
{

	signal(SIGTERM,signal_handler); /* catch kill signal */

	if (argc < 4) {
        cerr << "Usage: "<< argv[0] << " sleep(microsseconds) machine output_sensor" << endl;
        return 1;
    }
	
	//cout << argv[0] << " " << argv[1] << " " << argv[2] << " " << argv[3] << endl;

	int sleep_time = atoi(argv[1]);
	machine = argv[2];
	char * file = argv[3];
   
    
	string outputSensor="echo TimeStamp,";
    if (machine.compare("tegra-ubuntu") == 0)
    	outputSensor+="Total Power'('W')',CPU Volta'('V')',CPU Curre'('A')',CPU Power'('W')',GPU Power'('W')'";
    else
    	outputSensor+="A7 Voltage'('V')',A7 Current'('A')',A7 Power'('W')',A15 Voltage'('V')',A15 Current'('A')',A15 Power'('W')',GPU Power'('W')'";

    outputSensor+=" > ";
    outputSensor+=file;
   	system(outputSensor.c_str());

    if (machine.compare("tegra-ubuntu") == 0){
	    while (true){
	    	
		    double *data = tx2.getSample();

		    outputSensor="date +'%s%N'";

	        outputSensor+="," + to_string(data[TOTAL_POWER]) + "," + to_string(data[CPU_VOLTA]) + ",";
	        outputSensor+=to_string(data[CPU_CURRE]) + "," + to_string(data[CPU_POWER]) + ",";
	        outputSensor+=to_string(data[GPU_POWER]); 

	        outputSensor+=" >> ";
    		outputSensor+=file;
   			system(outputSensor.c_str());

		    delete [] data;
		     	
		  	usleep(sleep_time);
		}
	}else{

		getNode = new GetNode();
		
		if (getNode->OpenINA231()) {
			cerr << "OpenINA231 error" << endl;
			exit(1);
		}

		usleep(sleep_time);

	    while (true){
	    	
			//Get sensor information
        	getNode->GetINA231();


        	outputSensor="date +'%s%N'";
	        outputSensor+="," + to_string(getNode->kfcuV) + "," + to_string(getNode->kfcuA);
	        outputSensor+="," + to_string(getNode->kfcuW) + "," + to_string(getNode->armuV);
	        outputSensor+="," + to_string(getNode->armuA) + "," + to_string(getNode->armuW);
	        outputSensor+="," + to_string(getNode->g3duW);
			
			outputSensor+=" >> ";
    		outputSensor+=file;
   			system(outputSensor.c_str());

			usleep(sleep_time);
		}
	}

	return 0;
}
