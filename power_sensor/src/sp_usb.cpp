/**
* Library to get power reading of smart power2 device
**/
#define _POSIX_C_SOURCE 200809L

#include <inttypes.h>
#include <math.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <fstream>
#include <time.h>
#include <csignal>
#include <string>
#include <iostream>
#include <cstdlib>
#include <string.h>

using namespace std;

double * data = NULL;

#define DEFAULT_DEV_USB "/dev/ttyUSB0"

FILE * usb_file = NULL;

void signal_handler(int sig){

	delete [] data;
	fclose(usb_file);
    
    exit(0);
}

int init_usb(const char* usb) {
    
    usb_file = fopen(usb, "r");
    
    if (usb_file == NULL) {
        printf ("problem to open %s. errno = %d\n",usb, errno);
        return -1;
    }    

    return 0;
}


int  get_data(double *data){
	//v,a,p,w;

	int count = fscanf(usb_file,"%5lf,%5lf,%5lf,%5lf",&data[0],&data[1],&data[2],&data[3]);

	if (count == EOF) {
	    if (ferror(usb_file)) {
	        perror("fscanf");
	    }
	    else {
	        fprintf(stderr, "Error: fscanf reached end of file, no matching characters, no matching failure\n");
	    }
	    return -1;
	}
	else if (count != 4) {

		double trash=0;
		count = fscanf(usb_file,"%5lf,%5lf,%5lf,%5lf,%5lf",&trash,&data[0],&data[1],&data[2],&data[3]);

		if (count != 5) {
	    	fprintf(stderr, "Error: fscanf successfully matched and assigned %i input items, 5 expected\n", count);
	    	return -1;
	    }else
			fprintf(stderr, "Error: fscanf successfully matched and assigned %i input items, 4 expected\n", count);
	}

	return count;
}

int main(int argc, char* argv[]){

	signal(SIGTERM,signal_handler); /* catch kill signal */
	char buff[50];

	if (argc < 3) {
        cerr << "Usage: "<< argv[0] << " sleep(microsseconds) output_sensor" << endl;
        return 1;
    }	

    //cout << argv[0] << " " << argv[1] << " " << argv[2] << endl;

	int sleep_time = atoi(argv[1]);
	char * file = argv[2];

    if(init_usb(DEFAULT_DEV_USB) == -1 ) exit(1);

	data = new double[4];

	string outputSensor = "echo Timestamp,Voltage'('V')',Ampere'('A')',Power'('W')',Watt Hour";

	outputSensor+=" > ";
	outputSensor+=file;
	system(outputSensor.c_str());

	while(1){

		//get_data(data);
   		fgets(buff, 50, (FILE*)usb_file);

   		if (buff[0] == '\n') continue;

   		outputSensor="date +'%s%N'";

   		buff[strcspn(buff, "\n")] = 0;
   		int len = strlen(buff);
   		
   		if ( len > 28){
   			char *ret = strchr(buff, ',');
			outputSensor+=ret;//it returns the first comma
   			
		//if(get_data(data) == -1){
	   	//	cout << "Problem on reading power: " << endl;
	   	//}else{
	   	//	outputSensor ="date +'%s%N'";
	   	//	outputSensor+="," + to_string(data[0]) + "," + to_string(data[1]);
	   	//	outputSensor+="," + to_string(data[2]) + "," + to_string(data[3]);		
			
	   	//}
   		}else if (len == 23 || len == 24){

	   		outputSensor+=",";
	   		outputSensor+=buff;

	   		//printf("usb: |%s|\n", buff);

	  		//char * pch;
	  		//printf("spliting:\n");
	  		

			//printf("size 2: %d\n", strlen(buff));
			/*
	  		pch = strtok (buff,",\n");
	  		while (pch != NULL)
	  		{
	    		printf ("|%s|\n",pch);
	    		pch	 = strtok (NULL, ",\n");
	  		}   		
	  		printf("end\n");*/
   		}else
   			continue;
        
        outputSensor+=" >> ";
		outputSensor+=file;

		system(outputSensor.c_str());	  
		
	  	usleep(sleep_time);//sample rate
	}	

	return 0;

}


