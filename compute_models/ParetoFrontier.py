#! /usr/bin/env python3
from configs_generation import to_frequency_format
from pprint import pprint
from itertools import product
from PowerModel import arch_info
from BaseModel import ArchType
from EnergyModel import EnergyModel
from BaseModel import WORK_PATH
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import csv
import json


class ParetoFrontier:

	def __init__(self,energyModel,energyModelB=None):
		self.energyModel = energyModel
		self.energyModelB = energyModelB

	def generate_all_configs(self,arch,is_two_apps,freq_zero_core):  	
		li = arch_info[arch]['CL'] 
		bi = arch_info[arch]['Cb']
		fl = arch_info[arch]['FL']
		fb = arch_info[arch]['Fb'] 

		if is_two_apps:
			#ba,la,bb,lb,fb,fl
			p = list(product(range(0,bi+1),range(0,li+1),range(0,bi+1),range(0,li+1),fb,fl))
		else:	
			#b,l,fb,fl
			p = list(product(range(1,bi+1),range(1,li+1),fb,fl))

			#when one core is zero consider only the its respective lowest frequency
			if freq_zero_core:
				p_big_zero = list(product([0],range(1,li+1),[fb[0]],fl))
				p_little_zero = list(product(range(1,bi+1),[0],fb,[fl[0]]))

				p = p+p_big_zero+p_little_zero
		

		combinations = np.array(p)

		delete_filter = np.all(combinations[:,0:2] == 0,axis=1)# ensure that at least one core is avalaible to run the app
		if is_two_apps:
			delete_filter += np.all(combinations[:,2:4] == 0,axis=1)# ensure that at least one core is avalaible to run the second app
			delete_filter += np.sum(combinations[:,0:3:2],axis=1) > bi# sum of cores is bigger than the total available in cluster big
			delete_filter += np.sum(combinations[:,1:4:2],axis=1) > li# sum of cores is bigger than the total available in cluster little	

		combinations = np.delete(combinations,delete_filter,axis=0)

		return combinations

	def model_validation(self,arch,is_two_apps,freq_zero_core):
		#pprint('model validation')

		all_combinations = self.generate_all_configs(arch,is_two_apps,freq_zero_core)
		
		if is_two_apps:
			#ba,la,bb,lb,fb,fl
			all_comb_A = all_combinations[:,0:2]
			all_comb_A = np.concatenate((all_comb_A,all_combinations[:,4:]),axis=1)
			all_comb_B = all_combinations[:,2:4]
			all_comb_B = np.concatenate((all_comb_B,all_combinations[:,4:]),axis=1)		
			self.energyModel.set_configs(all_comb_A)
			self.energyModelB.set_configs(all_comb_B)	

			perf = np.maximum(self.energyModel.perfoModel.predict(),self.energyModelB.perfoModel.predict())
			ener = self.energyModel.predict() + self.energyModelB.predict()
		else:
			self.energyModel.set_configs(all_combinations)
			perf = self.energyModel.perfoModel.predict()
			ener = self.energyModel.predict()

		return [all_combinations,perf,ener]

	"""
	:param costs: An (n_points, n_costs) array
	:param return_mask: True to return a mask, False to return integer indices of efficient points.
	:return: An array of indices of pareto-efficient points.
		If return_mask is True, this will be an (n_points, ) boolean array
		Otherwise it will be a (n_efficient_points, ) integer array of indices.
	"""      
	def pareto_frontier_selection(self,costs, return_mask = True):  # <- Fastest for many points
		#pprint('pareto_frontier_selection')

		is_efficient = np.arange(costs.shape[0])#Return evenly spaced values within a given interval.
		n_points = costs.shape[0]
		next_point_index = 0  # Next index in the is_efficient array to search for

		while next_point_index<len(costs):#it stops when the number of pareto points found is lower the number of remaining costs
			nondominated_point_mask = np.any(costs<=costs[next_point_index], axis=1)#Checking whether any [time,energy] costs<=costs[next_point_index] in each row                  
			is_efficient = is_efficient[nondominated_point_mask]  # Remove dominated points            
			costs = costs[nondominated_point_mask]
			next_point_index = np.sum(nondominated_point_mask[:next_point_index])+1#count how many points are non dominated. It advance for the next point that is lower in any column than the current
		
		if return_mask:
		    is_efficient_mask = np.zeros(n_points, dtype = bool)
		    is_efficient_mask[is_efficient] = True
		    return is_efficient_mask
		else:
		    return is_efficient

	def get_pareto_optimal_configs(self,arch,is_two_apps=False,freq_zero_core=False):

		[all_configs,performance_costs,energy_costs] = self.model_validation(arch,is_two_apps,freq_zero_core)#TODO test if the pareto is correct
		costs = np.concatenate((np.array([performance_costs]).T,np.array([energy_costs]).T),axis=1)
		
		ind = np.lexsort((costs[:,1],costs[:,0]))# Sort by performance then energy, this makes pareto faster
		costs = costs[ind]
		all_configs = all_configs[ind]

		pareto_index = self.pareto_frontier_selection(costs)
		all_configs = np.concatenate((all_configs,costs),axis=1)
		pareto_configs = all_configs[pareto_index]

		return [all_configs,pareto_configs]

	def show_pareto_frontier(self,all_configs,pareto,onlyfrontier = False,is_two_apps = False,executed = None,governor= None,lim_tuples=None,plot_name_file = None): 

		#pprint('show pareto')

		start=4
		end=6
		startPareto=0   
		endPareto=all_configs.shape[1]    
		timeidx=4
		energyidx=5
		s_comp=0
		e_comp=4

		pprint('all_configs')
		pprint(all_configs)
		pprint(all_configs.shape)   
		
		if is_two_apps:
			start=6
			end=8
			startPareto=2
			endPareto=8
			timeidx=4
			energyidx=5
			s_comp=0
			e_comp=6           
		
		costs = all_configs[:,start:end]
		x  = costs[:,0]
		y  = costs[:,1]

		pprint('costs')
		pprint(costs)
		pprint(costs.shape)   
		pprint(x)
		pprint(y)      

		pareto = pareto[:,startPareto:endPareto]
		x2 = pareto[:,timeidx]
		y2 = pareto[:,energyidx] 

		pprint('pareto')
		pprint(pareto)
		pprint(pareto.shape)   
		pprint(x2)
		pprint(y2)      

		labelSize = 40
		legSize = 27
		tickSize= 35

		fig, ax = plt.subplots()

		gov_settings={'ondemand':{'marker':'^','color':'#FFDC00'},'interactive':{'marker':'P','color':'b'}, \
		'performance':{'marker':'H','color':'green'}, 'powersave':{'marker':'d','color':'#416165'}, \
		'conservative':{'marker':'X','color':'brown'}}

		if not onlyfrontier:
			ax.scatter(x, y, c='#9C9D9D',s=10,marker='o',edgecolors='none', label='All estimated configurations.')

		ax.scatter(x2,y2,marker='.', edgecolors='#0F0F0F',facecolors='none', linewidths=6,s=500, label='Estimated Pareto frontier.')

		if executed is not None:
			x3 = executed[:,timeidx]
			y3 = executed[:,energyidx] 
			#pprint(x3)
			#pprint(len(x3)) 
			#pprint(y3)
			#pprint(len(y3))          
			ax.scatter(x3, y3,linewidths=6,s=500,marker='.',facecolors='r', edgecolors='r',label='Measured Pareto frontier.')#cd3f3e
			#Fix the dashed line between measured and modeled  np.all(a[0:3] == b[1,1:4])  
		if onlyfrontier:    
			if executed is not None:
				for ex in executed: 
					for est in pareto:               

						#print('-----')
						#pprint(est)
						#pprint('')
						#pprint(ex)
						#pprint('')
						#print('-----')

						ret = np.all(est[s_comp:e_comp] == ex[s_comp:e_comp])
						if ret:                                  
							#if (ex[0] == int(est[0]/1000000)) and (ex[1] == int(est[1]/1000000)):                    
							x4 = [est[timeidx],ex[timeidx]]
							y4 = [est[energyidx],ex[energyidx]]  
							#pprint('###')
							#pprint(ret)
							#pprint('equal configurations:')
							#pprint(ex[s_comp:e_comp])
							#pprint(est[s_comp:e_comp])
							#pprint(str(x4)+' - '+str(y4))
							ax.plot(x4, y4, '--',color='#B7B7B7',linewidth=0.7) 

			#if governor is not None:
			#	for g in governor:
			#		ax.plot([g[1],g[1]],[g[2]+offset,0], '-.',color=gov_settings[g[0]]['color'],linewidth=3) 
				#ax.plot([governor[0][1],governor[0][1]],[governor[0][2]+offset,0], '-.',color='#FFDC00',linewidth=3) 
				#ax.plot([governor[1][1],governor[1][1]],[governor[1][2]+offset,0], '-.',color='b',linewidth=3) 
				#ax.plot([governor[2][1],0],[governor[2][2],governor[2][2]], '-.',color='green',linewidth=3) 
					
		if governor is not None:
			#governor = np.array(governor,dtype=object)
			for g in governor:
				ax.scatter(g[1], g[2], s=400,marker=gov_settings[g[0]]['marker'],linewidths=1,c=gov_settings[g[0]]['color'], alpha=1, label=g[0])				
			#ax.scatter(governor[0,1], governor[0,2], s=500,marker='^',linewidths=6,c='#FFDC00', alpha=1, label=governor[0,0])#Performance '#FFDC00'
			#ax.scatter(governor[1,1], governor[1,2], s=500,marker='P',linewidths=6,c='b', alpha=1,label=governor[1,0])#ondemand '#3B00FF'
			#ax.scatter(governor[2,1], governor[2,2], s=500,marker='H',linewidths=6,c='green', alpha=1, label=governor[2,0])#powersave '#72FF95'            

		plt.xlabel("Time(s)",fontsize=labelSize)
		plt.ylabel("Energy(J)",fontsize=labelSize)
		plt.xticks(fontsize = tickSize)
		plt.yticks(fontsize = tickSize)
		if lim_tuples is not None:
			plt.xlim(lim_tuples[0])
			plt.ylim(lim_tuples[1])

		ax.legend(fontsize=legSize)
		ax.grid(True)

		if plot_name_file is not None:
			fig.set_size_inches(18, 10,forward=True)
			fig.savefig(WORK_PATH + "figs/" + plot_name_file, dpi=200)

		plt.show()

	def load_measured_pareto(self,file_name,ty):
		d_energy= self.energyModel.load_energy_data(file_name,ty)
		d_perf  = self.energyModel.perfoModel.load_perf_data(file_name)
		return  np.concatenate((np.asarray(d_energy['configs']),np.asarray([d_perf['data']]).T,np.asarray([d_energy['data']]).T),axis=1)

	def load_governor(self,file_name):

		ret = []
		with open(WORK_PATH + 'Analysed_results/' +file_name) as csvfile:
			reader = csv.DictReader(csvfile)
			for row in reader:
				ret.append([row['Governor'],float(row['Run Time (s)']),float(row['Energy(J)'])])

		return ret

	def save_csv(self,np_array,file_name,is_two_apps=False,is_battery=False):
		path = WORK_PATH + 'data/pareto/' + file_name 

		fmt = ['%d','%d','%d','%d']

		if is_two_apps:
			header = 'bA,LA,bB,LB,Fb,FL,Time,'
			fmt+=['%d','%d']
		else:
			header = 'bi,Li,Fb,FL,Time,'

		fmt.extend(['%.4f','%.4f'])    		
			
		header+=("Battery" if is_battery else "Energy")

		np.savetxt(path,np_array,header=header,comments='',fmt=fmt,delimiter=",")		

