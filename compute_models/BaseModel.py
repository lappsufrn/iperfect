#! /usr/bin/env python3

from enum import Enum
from json import JSONEncoder
from scipy.optimize import least_squares
from abc import ABC, abstractmethod
from pprint import pprint
from sklearn.svm import SVR
from sklearn.neural_network import MLPRegressor
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import QuantileTransformer
from sklearn.pipeline import make_pipeline
from sklearn.tree import DecisionTreeRegressor
import joblib

import pandas as pd
import numpy as np
import json
import os

from parsecpy import CoupledAnnealer


#CHANGE YOUR WORK PATH
WORK_PATH = os.environ['HOME'] + '/Dropbox/big.Little_optimal_frequencies/'

class ArchType(str, Enum):
    TX2_SINGLE : str = "TX2_SINGLE"
    TX2_TWO    : str = "TX2_TWO"
    XU3_SINGLE : str = "XU3_SINGLE" 
    TX2        : str = "tx2"
    XU3_V3     : str = "xu3_v3"
    XU3_V4     : str = "xu3_v4"

class NumpyArrayEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return JSONEncoder.default(self, obj)

class BaseModel(ABC):
	
    #@abstractmethod
    #def objective_function(x_meas, y_meas):
        #function needed for csa
    #    pass

    @abstractmethod
    def equation(self,x):
        pass

    @abstractmethod
    def fit_dict_update(self,x,loss,f_scale,method,bounds,x_scale,verb):
        pass    

    @abstractmethod
    def parallel_term(self,x):
        pass

    @abstractmethod
    def sequential_term(self,x):
        pass

    @abstractmethod
    def from_json(cls,jsonfile):
        pass      

    @classmethod
    def fit_csa(self,csa_json):
        pprint('fit csa')

        parameter = self.load_json(csa_json)

        if parameter['model'] == 'Performance':
            df = self.load_perf_data(file_name=parameter['measured_file'],ty=parameter['type']) 
        else:
            df = self.load_power_data(file_name=parameter['measured_file'],ty=parameter['type']) 
     
        initial_state = np.array([np.random.uniform(size=parameter['dimension']) for _ in range(parameter['size'])])

        optm = CoupledAnnealer(initial_state,
                                       modelcodefilepath=parameter['modelcodefilepath'],
                                       size=parameter['size'],
                                       steps=parameter['steps'],
                                       update_interval=parameter['update_interval'],
                                       tgen_initial=parameter['tgen_initial'],
                                       tgen_upd_factor=parameter['tgen_upd_factor'],
                                       tacc_initial=parameter['tacc_initial'],
                                       alpha=parameter['alpha'],
                                       desired_variance=parameter['desired_variance'],
                                       lowervalues=parameter['lowervalues'],
                                       uppervalues=parameter['uppervalues'],
                                       threads=parameter['threads'],
                                       verbosity=parameter['verbosity'],
                                       x_meas = df['configs'],
                                       y_meas=df['data'])

        return optm.run()

    def fit_dict(self,x,loss,f_scale,method,bounds,x_scale,verb):
        ret = self.fit_least_squares(x,loss,f_scale,method,bounds,x_scale,verb)
        gs = self.goodness(ret.fun)
        return self.fitted_info(ret,gs,loss,f_scale,method,x_scale)  

    def residual(self,x):   
        return self.equation(x) - self.measured_data        
    
    def fitted_info(self,fitted_object,goodness,loss,fscale,method,x_scale):
        return {'fitted_object':fitted_object,'goodness':goodness,'loss':loss,'fscale':fscale, \
                'method':method,'x_scale':x_scale}

    def fit_least_squares(self,x0,loss,f_scale,method,bounds,x_scale,verb):
        return least_squares(self.residual,x0,loss=loss, f_scale=f_scale,method=method, \
                            bounds=bounds,x_scale=x_scale,verbose=verb) 
                            #([min_values],[max_values]) case scalar it will be bound the same value for all variables    

    def fit_best_least_squares(self,x,bounds,verbose=0):
        losses = ['linear','soft_l1','huber','cauchy','arctan']
        methods = ['trf', 'dogbox']  

        min_v = np.Infinity
        min_result = {}
        for x_scale in (np.arange(0.1,3,0.1),['jac']):
            for xcle in x_scale:
                for m in methods:
                    for l in losses:
                        for fscale in np.arange(0.1,3,0.1): 
                            try:       
                                #pprint(str(xcle) + ' ' + m + ' ' + l + ' ' + str(fscale))   
                                ret = self.fit_dict_update(x,l,fscale,m,bounds,xcle,verbose)
                            except ValueError:
                                pprint('error')
                                pprint({'loss':l,'fscale':fscale,'method':m,'x_scale':xcle})
                                continue    
                            gs = ret['goodness']
                            if gs[0] < min_v :
                                min_result = ret
                                min_v = gs[0]
                                pprint('Actual best fit')
                                pprint( str(min_result['fitted_object'].x) + str(min_result['goodness'])+' '+str(min_result['loss'])+' '+str(min_result['fscale']) \
                                    +' '+str(min_result['method'])+' '+str(min_result['x_scale']))

    def goodness(self,fun,y_measured=None):
        #.fun is Vector of residuals at the solution.
        #Measuring MSE

        if y_measured is None:
        	y_measured = self.measured_data

        mse = (fun ** 2).mean()
        rmse = np.sqrt(mse)
        mape = (np.abs(fun)/y_measured).mean()

        return [mse,rmse,mape]  

    def fit_model_ML(self,model_ty):
        alpha=10.0**-np.arange(1,7)

        if model_ty == 'Perf':
            gs_ml=make_pipeline(QuantileTransformer(),GridSearchCV(MLPRegressor(batch_size=64,hidden_layer_sizes=(128,64,32,16),verbose=True,solver='sgd',max_iter=5000,random_state=1,learning_rate='adaptive',learning_rate_init=0.0001,momentum=0.9),cv=5,param_grid={"alpha":alpha},n_jobs=-1)) #perf
        else:
            #gs_ml=make_pipeline(QuantileTransformer(),GridSearchCV(MLPRegressor(batch_size=64,hidden_layer_sizes=(128,64,32,16),verbose=True,solver='sgd',max_iter=5000,random_state=1,learning_rate='adaptive',learning_rate_init=0.000001,momentum=0.99),cv=5,param_grid={"alpha":alpha},n_jobs=-1)) #ener 164
            #gs_ml = DecisionTreeRegressor(random_state=0, max_depth=100)
            gs_ml = GridSearchCV(SVR(),cv=5,param_grid={"C": [0.1,0.5,0.8]})
        gs_ml.fit(self.configs, self.measured_data)
 
        y_predict = self.predict(model_ml=gs_ml)
        errors =    self.goodness(self.measured_data-y_predict)

        return (gs_ml,errors,y_predict)        

    def fit_model(self,x,bounds,verbose=0,csa_json=None,mode='least_squares'):

        if mode == 'least_squares':
            fitted_model= fit_best_least_squares(x,bounds,verbose)
        if mode == 'ML':
            fitted_model= fit_model_ML()
        if mode == 'csa':
            fitted_model= fit_csa(csa_json,configs)

        return fitted_model
    
    def predict(self,model_ml=None,x=None):
        
        if model_ml is None and self.model_ml is None:
            return self.equation(x)

        if x is None:
            x = self.configs
        
        if model_ml is None:
            model_ml = self.model_ml

        return np.array(model_ml.predict(x))
    
    #def predict(self,x=None):
        res = self.residual(x)
        gs = self.goodness(res)     
        return [res,gs]

    def predict_ML(self, model,configs,measured_data):
        y_predict = np.array(model.predict(configs))
        g = self.goodness(y_predict-measured_data,measured_data)
        #mse = mean_squared_error(measured_data, y_predict)
        #rmse = np.sqrt(mse)
        #mape = (np.abs(measured_data-y_predict)/measured_data).mean()

        return (g,y_predict)
    
    @classmethod
    def load_json(self,file_name):
        with open(WORK_PATH + 'data/json/' + file_name, "r") as jsonfile:
            return json.load(jsonfile)
    
    @classmethod
    def load_data(self,file_name,data_index,ty=None):

        t = ty if ty is not None else self.ty

        if file_name is None:
            return {'data':[],'configs': []}

        df = pd.read_csv(WORK_PATH + 'Analysed_results/' + file_name,skiprows=1,header=None)

        data = df[data_index].values 

        if t == ArchType.TX2_SINGLE:    
            convert_to_hertz=1000
        elif t == ArchType.XU3_SINGLE:
            convert_to_hertz=1000000
        else:
            convert_to_hertz=1

        configs = np.dstack((df[3].values,df[2].values,df[1].values*convert_to_hertz,df[0].values*convert_to_hertz))

        return {'data':data,'configs': configs[0,:,:]}    

    def save_table(self,file_name,fitted_x,goodness,column_names):

        if self.ty == ArchType.TX2_SINGLE or self.ty == ArchType.XU3_SINGLE:        
            p = pd.DataFrame(self.configs,columns=['bi','Li','Fb','FL'])
            
            p = pd.concat([p,pd.Series(self.measured_data,name=column_names[0])],axis=1)
            p = pd.concat([p,pd.Series(self.equation(fitted_x),name=column_names[1])],axis=1)
            p = p.append(pd.DataFrame({'MSE':[goodness[0]],'RMSE':[goodness[1]],'MAPE':[goodness[2]]}))
            
            p = p.round(4)
            p = p.convert_dtypes()  
            
            p.to_csv(WORK_PATH + 'data/fitted/' + file_name ,index=False)
        else:
            p = pd.DataFrame({})
            p.to_csv(WORK_PATH + 'data/fitted/' + file_name)

    def save_table_ML(self,file_name,configs,measured_data,y_predict,errors,column_names):
        header='bi,Li,Fb,FL,'+column_names

        fmt=['%d','%d','%d','%d','%.4f','%.4f']

        data = np.concatenate((np.array(configs),np.array([measured_data]).T,np.array([y_predict]).T),axis=1)
        np.savetxt(WORK_PATH + 'data/fitted/' + file_name,data,comments='',fmt=fmt,header=header,delimiter=",")
        pprint(np.array(errors))
        np.savetxt(WORK_PATH + 'data/fitted/' + 'errors_'+file_name,np.array([errors]),comments='',fmt=['%.8f','%.8f','%.8f'],header='MSE,RMSE,MAPE',delimiter=",")   

    def save_json(self,dic,file_name):
        with open(WORK_PATH + 'data/json/' + file_name, 'w') as jsonfile:
            json.dump(dic, jsonfile,cls=NumpyArrayEncoder)

    @classmethod
    def save_model_ML(self,model,file_name):
        joblib.dump(model, WORK_PATH + 'data/json/' + file_name)
        
    @classmethod
    def load_model_ML(self,file_name):
        return joblib.load(WORK_PATH + 'data/json/' + file_name)
