#! /usr/bin/env python3

from pprint import pprint
from BaseModel import WORK_PATH
from copy import deepcopy
import numpy as np
import pandas as pd
import csv
import os

def write_frequencies(file,f):
	#pprint(f)
	f = f/1000
	file.write(str(int(f[0])))
	for i in range(1,f.shape[0]):
		file.write(',')
		file.write(str(int(f[i])))
	file.write('\n')

def to_frequency_format(np_array,file_name,is_two_apps=False):


	arr_copy = deepcopy(np_array) 
	pprint(arr_copy)
	with open(WORK_PATH+'/data/frequencies/frequencies_'+file_name+'.data', 'w') as file:
		while arr_copy.size != 0:
			el = arr_copy[0]
			pprint(el)
			key = str(int(el[0])) + '\n' + str(int(el[1])) + '\n'
			if is_two_apps:
				key += str(int(el[2])) + '\n' + str(int(el[3])) + '\n'
			pprint(key)
			#write bi,li
			file.write(key)
			#get all freq with the same cores arrangement
			if is_two_apps:
				freq_idx=((arr_copy[:,0] == el[0]) & (arr_copy[:,1] == el[1]) & (arr_copy[:,2] == el[2]) & (arr_copy[:,3] == el[3]))
			else:
				freq_idx=((arr_copy[:,0] == el[0]) & (arr_copy[:,1] == el[1]))
			pprint(freq_idx)
			#write the frequencies
			if is_two_apps:
				fb_i = 4
				fl_i = 5
			else:
				fb_i = 2
				fl_i = 3				
			write_frequencies(file,arr_copy[freq_idx,fb_i])
			write_frequencies(file,arr_copy[freq_idx,fl_i])
			#delete elements already wrote in the file
			arr_copy = np.delete(arr_copy,freq_idx,axis=0)


			

	