# -*- coding: utf-8 -*-
"""
    Model function example to use with parsecpy runmodel script.
    Model for variation of input size (n) and number of cores (p).

    Speedup:
        S = 1 / ( ( 1-f(p,n) ) + f(p,n)/p + Q(p,n) )
 Fraction:
        f(p,n) = max( min((f1) + (f2)/p + (f3)*(f4)^n,1 ),0 )

    Overhead:
        Q(p,n) = (f5) + ( (f6)*p )/( (f7)^n )

"""
from pprint import pprint
import random
import math
import numpy as np
import json
import os
from BaseModel import WORK_PATH
from sklearn.metrics import mean_squared_error

from PerformanceModel import PerformanceModel
from PowerModel import PowerModel

#from scipy.stats import cauchy

print('modelfunc')
csa_json = WORK_PATH + 'data/json/csa_power.json'
#-----
with open(csa_json, 'r') as parameter:
    parameter = json.load(parameter)

if parameter['model'] == 'Performance':
    model = PerformanceModel(ty=parameter['type'],F=parameter['F'],measured_file=parameter['measured_file'])
else:
    model = PowerModel(ty=parameter['type'],table_file=parameter['table_file'],is_core_disable=parameter['is_core_disable'],measured_file=parameter['measured_file'])

print(model)
#-----
def probe_function(par, tgen):
    """
    Constraint function that would be considered on model.
    It used at csa method.

    :param par: Actual parameters values
    :param tgen: Temperature of generation
    :param args: Positional arguments passed for objective
                 and constraint functions
    :return: A new probe solution based on tgen and a random function
    """

    t = np.tan(np.pi * (np.random.uniform(size=len(par))-0.5))
    probe_solution = 2*np.mod((par + t * tgen + 1)/2, 1) - 1
    return probe_solution

def objective_function(par, x_meas, y_meas, **kwargs):
    """

    Objective function (target function) to minimize.
    It used at csa method.

    :param par: Actual parameters values
    :param kwargs: keyworded arguments passed for objective
                 and constraint functions
    :return: Mean squared error between measures and predicts
    """
    #pprint('objective_function')
    #pprint(model)
    pred = model.equation(par)
    #pprint('PRED')
    #pprint(pred)
    er = mean_squared_error(y_meas, pred)
    
    #print('Par: ', par)
    #print('ER: {0}\n'.format(er))
    return er         