#! /usr/bin/env python3

from pprint import pprint
import numpy as np
import pandas as pd
import csv
import json
from copy import deepcopy
from BaseModel import WORK_PATH
from BaseModel import BaseModel
from BaseModel import ArchType
from BaseModel import NumpyArrayEncoder

arch_info = BaseModel.load_json('arch_info.json')

class VoltageFrequencyTable:
    def __init__(self,file_name):
        self.table_file_name = file_name
        self.table = self.load_table(file_name) #Hz:microvolts
    
    def load_table(self,file_name):
        if file_name == None:
            return None 
        with open(WORK_PATH + 'data/json/' + file_name) as jsonfile:
            return json.load(jsonfile)

    def get_big_voltages(self,frequencies):
        return self.get_voltages('big',frequencies)

    def get_little_voltages(self,frequencies):
        return self.get_voltages('little',frequencies)

    def get_voltages(self,core,frequencies):
        return [self.table[core][str(f)]/1000000 for f in frequencies]#[thing for thing in list_of_things]

class PowerModel(BaseModel):
    #equations  
    #you can generate the CPU model by zero theta
    Pbexp     = lambda self,gammab,b,bleak,Fb,vFb,Ileakb: b*gammab*(vFb**2)*Fb + bleak*vFb*Ileakb
    PLexp     = lambda self,gammaL,L,Lleak,FL,vFL,IleakL: L*gammaL*(vFL**2)*FL + Lleak*vFL*IleakL
    PHMPexp_p = lambda self,gamma,Ileak,theta,coresleak,vF: self.Pbexp(gamma[0],self.configs[:,0],coresleak[0],self.configs[:,2],vF[0],Ileak[0]) + \
                                                            self.PLexp(gamma[1],self.configs[:,1],coresleak[1],self.configs[:,3],vF[1],Ileak[1]) + theta

    def __init__(self,ty,measured_file = None,table_file = None,is_core_disable = True,fitted_x=None):
        
        self.ty = ty
        self.is_core_disable = is_core_disable
        self.fitted_x = np.asarray(fitted_x)

        self.voltage_table = VoltageFrequencyTable(table_file)

        df = self.load_power_data(file_name=measured_file,ty=ty)
        self.configs = df['configs']
        self.measured_data = df['data']   


    @classmethod #is a static method
    def from_json(cls,jsonfile):
        DictSettings = PowerModel.load_json(jsonfile)
        return cls(DictSettings['ty'],table_file=DictSettings['vf_table'],is_core_disable=DictSettings['disable_cores_mode'],fitted_x=DictSettings['fitted_object']['x'])            
 
    def __get_voltage_or_frequency(self,mask_LITTLE_zero):

        if self.voltage_table.table == None:
            vb = self.configs[:,2]
            vL = self.configs[:,3]
            v_lzero = self.configs[mask_LITTLE_zero,3]
        else:
            vb = self.voltage_table.get_big_voltages(self.configs[:,2])
            vL = self.voltage_table.get_little_voltages(self.configs[:,3])
            v_lzero = self.voltage_table.get_little_voltages(self.configs[mask_LITTLE_zero,3])

        return (np.array([vb,vL]),v_lzero)

    def __get_coresleak(self):
        if self.is_core_disable:
            bleak = self.configs[:,0]
            Lleak = self.configs[:,1]
        else:
            ones = np.ones(self.configs.shape[0], dtype = int)
            if self.ty == ArchType.XU3_SINGLE or self.ty == ArchType.XU3_TWO:
               key = ArchType.XU3_V3
            else:
                key = ArchType.TX2

            bleak = ones * arch_info[key]['Cb']
            Lleak = ones * arch_info[key]['CL']
        return np.array([bleak,Lleak])

    def equation(self,x):    
        return self.parallel_term(x)

    def parallel_term(self,x):

        #pprint('Power parallel_term')        
        gamma = np.array(x[0:2])
        Ileak = np.array(x[2:4])
        if x.shape[0] > 4:
            theta = np.array(x[4])
        else:
            theta = 0
        '''
        pprint(gamma)
        pprint(Ileak)
        pprint(theta)
        pprint(self.ty)
        pprint(self.is_core_disable)
        pprint(self.voltage_table.table)
        pprint(self.configs)
        '''
        mask_LITTLE_zero = (self.configs[:,1] == 0)

        (vF,v_lzero) = self.__get_voltage_or_frequency(mask_LITTLE_zero)

        coresleak = self.__get_coresleak()
        '''
        pprint('(v,v_lzero)')
        pprint((vF,v_lzero))

        pprint('coresleak')
        pprint(coresleak)
        '''       
        p_hmp = self.PHMPexp_p(gamma,Ileak,theta,coresleak,vF)

        if self.is_core_disable:                 
            p_hmp[mask_LITTLE_zero] += self.PLexp(gamma[1],0,1,0,np.array(v_lzero),Ileak[1]) # adding the static power of the zero Little core which is never disable.
        
        #pprint('p_hmp')
        #pprint(p_hmp)
        
        return p_hmp 

    def sequential_term(self,x):
        #pprint('Power sequential_term')        
        gamma = np.array(x[0:2])
        Ileak = np.array(x[2:4])
        if x.shape[0] > 4:
            theta = np.array(x[4])
        else:
            theta = 0

        mask_big_zero = (self.configs[:,0] == 0)
        mask_LITTLE_zero = (self.configs[:,1] == 0)
        
        '''
        pprint(gamma)
        pprint(Ileak)
        pprint(theta)
        pprint(self.ty)
        pprint(self.is_core_disable)
        pprint(self.voltage_table.table)
        pprint(self.configs)
        '''
        tmp_configs = deepcopy(self.configs)
        #configurations is b,L,fb,FL
        #where big is zero, the sequential run in one LITTLE core, so little must be 1
        self.configs[(mask_big_zero),1] = np.ones(sum(mask_big_zero))#little
        #where big is not zero, the sequential run in one big core, so little must be 0
        self.configs[np.invert(mask_big_zero),0] = np.ones(sum(np.invert(mask_big_zero)))#big
        self.configs[np.invert(mask_big_zero),1] = np.zeros(sum(np.invert(mask_big_zero)))#little
        
        # pprint('seq configs')
        # pprint(self.configs)

        coresleak = self.__get_coresleak()

        #pprint('coresleak')
        #pprint(coresleak)        
        
        (vF,v_lzero) = self.__get_voltage_or_frequency(mask_LITTLE_zero)

        #pprint('vF')
        #pprint(vF)

        p_hmp_s = self.PHMPexp_p(gamma,Ileak,theta,coresleak,vF)
        
        self.configs = deepcopy(tmp_configs)
        
        #pprint('Return configs')
        #pprint(self.configs)
        #pprint(p_hmp_s)
        
        
        return p_hmp_s

    def fit_dict_update(self,x,loss,f_scale,method,bounds,x_scale,verb):
        ret = self.fit_dict(x,loss,f_scale,method,bounds,x_scale,verb)
        ret.update({'disable_cores_mode':self.is_core_disable,'vf_table': \
                    self.voltage_table.table_file_name,'ty':self.ty})
        return ret

    @classmethod
    def load_power_data(self,file_name,ty=None):
        return self.load_data(file_name,4,ty=ty)

#self,ty,measured_file = None,table_file = None,is_core_disable = True,fitted_x=None
#table_file="voltage_frequency_xu3.json"
#fitted=np.asarray([2.3617063535901224e-29, 3.020127049191087e-29, 7.726858874956832e-10, 3.8624747844551166e-10, 1.2750695542277741])#[1.6357776839524391e-28, 6.010972317736056e-29, 3.2690228464426014e-10, 7.688647165583356e-11, 2.886707297619943])
#pm = PowerModel(ArchType.TX2_SINGLE,measured_file='rosusecase_halton_tx2_16022020_tx2.csv',fitted_x=fitted)
#d = pm.find_best_fit([np.random.rand(),np.random.rand(),np.random.rand(),np.random.rand(),np.random.rand()*4+1],(0,np.inf),0)
#[res,gs]=pm.predict(x=fitted)
#pm.save_table('rosusecase_halton_tx2_16022020_tx2_power.csv',fitted,gs,['Power(W)','Est_Power(W)'])
#pm.save_json(d,'stressng_halton_20102020_sp_table_none')        

