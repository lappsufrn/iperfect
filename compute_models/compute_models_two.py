#! /usr/bin/env python3
from preprocessing import analyse_configs_files
from preprocessing import analyse_governor_files
from PerformanceModel import PerformanceModel
from PowerModel import PowerModel
from EnergyModel import EnergyModel
from ParetoFrontier import ParetoFrontier
from BaseModel import ArchType
from BaseModel import NumpyArrayEncoder
from BaseModel import WORK_PATH
from configs_generation import to_frequency_format
import numpy as np
import os

DATA_PATH = os.environ['HOME'] + '/Projects/hmp_meas_results/'

#process raw data - inputs(folder to the data, list of folders)
#analyse_configs_files(DATA_PATH + "power_measurement/", ['stress_tx2_tutorial','darknet_tutorial','mm_1024_tutorial'])

##------------
#fitting power model with stress-ng
##------------

"""
:param ty: architecture type
:param measured_file: measured data
:param table_file: f/v table information
:param is_core_disable:  unused core feature (Default: true)
:param fitted_x: array of fitted parameters
"""    
#pm = PowerModel(ArchType.TX2_SINGLE,measured_file='stress_tx2_tutorial_tx2.csv')
"""
:param x: array of an initial guess
:param bounds: lower and upper bounds for each parameter 
""" 
#d = pm.fit_model([np.random.rand(),np.random.rand(),np.random.rand(),np.random.rand(),np.random.rand()*4+1],(0,np.inf))
"""
:param file_name: 
:param fitted_x: fitted parameters
:param goodness: Array with MSE, RMSE and MAPE
:param column_names:  Column names of the measured and estimated data
""" 
#pm.save_table('stress_tx2_tutorial_tx2.csv',d['fitted_object'].x,d['goodness'],['Power(W)','Est_Power(W)'])
"""
:param dic: output result dictionary of the fit_model
:param file_name: 
""" 
#pm.save_json(d,'stress_tx2_tutorial_tx2.json')

##------------
#fitting performance model
##------------
"""
:param ty: architecture type
:param F: 
:param Tl: 
:param measured_file:  
:param fitted_f:
"""
#Matrix Multiplication
'''
pm = PerformanceModel(ArchType.TX2_SINGLE,1113600000,measured_file='mm_1024_tutorial_tx2.csv')

d = pm.fit_model([0.1,1],([0, 0.1], (1,np.inf)))

pm.save_table('mm_1024_tutorial_tx2.csv',d['fitted_object']['x'],d['goodness'],['Run_Time(s)','Est_Time(S)'])
pm.save_json(d,'mm_1024_tutorial_tx2.json')
'''
#DARKNET
'''
pm = PerformanceModel(ArchType.TX2_SINGLE,1113600000,measured_file='darknet_tutorial_tx2.csv')

d = pm.fit_model([0.5,1],([0, 0.1], (1,np.inf)))

pm.save_table('darknet_tutorial_tx2.csv',d['fitted_object']['x'],d['goodness'],['Run_Time(s)','Est_Time(S)'])
pm.save_json(d,'darknet_tutorial_tx2.json')
'''
##------------
#Energy model
##------------

"""
:param perf_json_file: json file to fitted performance model 
:param pow_json_file: json file to fitted power model
"""
#Matrix Multiplication
em_mm = EnergyModel.from_json('mm_1024_tutorial_tx2.json','stress_tx2_tutorial_tx2.json')
"""
:param file_name: measured data
:param ty: architecture type
"""
#d_energy = em_mm.load_energy_data('mm_1024_tutorial_tx2.csv',ArchType.TX2_SINGLE)

#goodness = em_mm.calculate_goodness(d_energy)
#em_mm.save_table('mm_1024_tutorial_tx2_energy.csv',None,goodness,['Energy(J)','Est_Energy(J)'])

#DARKNET
em_d = EnergyModel.from_json('darknet_tutorial_tx2.json','stress_tx2_tutorial_tx2.json')
"""
:param file_name: measured data
:param ty: architecture type
"""
#d_energy = em_d.load_energy_data('darknet_tutorial_tx2.csv',ArchType.TX2_SINGLE)

#goodness = em_d.calculate_goodness(d_energy)
#em_d.save_table('darknet_tutorial_tx2_energy.csv',None,goodness,['Energy(J)','Est_Energy(J)'])


##------------
#Pareto
##------------

"""
:param energyModel: EnergyModel object
"""
pf = ParetoFrontier(em_mm,em_d)

"""
:param arch: architecture type
:param is_two_apps: Pareto for multiple applications (default false) 
"""
[all_configs,pareto] = pf.get_pareto_optimal_configs(ArchType.TX2,is_two_apps=True)

"""
:param np_array: array to be saved as csv file 
:param file_name: 
:param is_two_apps: 
"""
#pf.save_csv(all_configs,'mm_darknet_tutorial_allconfigs.csv',is_two_apps=True)
#pf.save_csv(pareto,'mm_darknet_tutorial_pareto.csv',is_two_apps=True)


"""
:param all_configs: all_configs array
:param pareto: pareto array
:param onlyfrontier: show only the Pareto Frontier points (Default: False)
:param is_two_apps:   (Default: False)
:param executed:  Array with the measured Pareto points(Default: None)
:param governor:  Array with the measured governors(Default: None)
:param lim_tuples:  set axis limits [(x1,x2),(y1,y2)](Default: None)
:param plot_name_file: (Default: None)
"""  
pf.show_pareto_frontier(all_configs,pareto,is_two_apps=True,plot_name_file='mm_darknet_tutorial_1.png')
pf.show_pareto_frontier(all_configs,pareto,is_two_apps=True,onlyfrontier=True,plot_name_file='mm_darknet_tutorial_2.png')




