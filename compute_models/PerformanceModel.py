#! /usr/bin/env python3

from pprint import pprint
import numpy as np
import pandas as pd
import csv
import json
from BaseModel import WORK_PATH
from BaseModel import BaseModel
from BaseModel import ArchType
from BaseModel import NumpyArrayEncoder


class PerformanceModel(BaseModel):
    #equations
    TbLexp_p = lambda self,f,Perf,F,Tl: Tl*( ( f*F )/( self.configs[:,0]*Perf*self.configs[:,2] + self.configs[:,1]*self.configs[:,3] ) )
    TbLexp_s = lambda self,f,Perf,F,Tl: Tl*( ( (1-f)*F )/( Perf*self.configs[:,2] ) )
    TbLexp_s_b_zero = lambda self,f,F,Tl: Tl*( ( (1-f)*F )/( self.configs[:,3] ) )

    def __init__(self,ty=None,F=None,Tl=None,Perf=None,measured_file=None,fitted_f=None,model_ml=None):
        self.Perf=Perf
        self.F=F
        self.ty=ty
        self.fitted_x = fitted_f
        self.model_ml = model_ml

        df = self.load_perf_data(file_name=measured_file,ty=ty) 
        self.configs = df['configs']
        self.measured_data = df['data']           

        self.Tl= self.__get_Tl() if Tl is None else Tl

    def __get_Tl(self):
        if self.configs == []:
            return None
        one_little = np.all(self.configs[:,0:2] == [0,1],axis=1)
        F_ind = (self.configs[:,-1] == self.F)
        return self.measured_data[one_little & F_ind][0]

    @classmethod #is a static method
    def from_json(cls,jsonfile):
        DictSettings = PerformanceModel.load_json(jsonfile)
        return cls(DictSettings['ty'],DictSettings['F'],Tl=DictSettings['Tl'],Perf=DictSettings['perf'],fitted_f=(DictSettings['fitted_object']['x']))

    def equation(self,x=None):  
        #pprint('peformance equation')
        x0 = (self.fitted_x if x is None else x)
        #pprint('x0 ' + str(x0))
        return self.sequential_term(x0) + self.parallel_term(x0)

    def parallel_term(self,x):
        '''
        pprint('peformance parralel')
        pprint(x)  
        pprint(self.F)
        pprint(self.Tl)
        pprint(self.Perf)
        pprint(self.ty)
        pprint(self.configs)
        '''
        [f,perf] = self.__input_test(x)
         
        r = self.TbLexp_p(f,perf,self.F,self.Tl)
       
        return  r

    def sequential_term(self,x):
        '''
        pprint('peformance sequential')
        pprint(x)
        pprint(self.F)
        pprint(self.Tl)
        pprint(self.Perf)
        pprint(self.ty)
        pprint(self.configs) 
        '''
        [f,perf] = self.__input_test(x)

        mask_big_zero = (self.configs[:,0] == 0)
        mask_inverse = np.invert(mask_big_zero)

        seq_b_zero = self.TbLexp_s_b_zero(f,self.F,self.Tl)
        seq = self.TbLexp_s(f,perf,self.F,self.Tl)        

        sequential = np.zeros(self.configs.shape[0])        
        sequential[mask_big_zero] = seq_b_zero[mask_big_zero] #get seq time that b == 0
        sequential[mask_inverse] = seq[mask_inverse] #get seq time that b != 0

        #pprint(sequential)
        #input('')

        return sequential

    def __input_test(self,x):
        f=x[0]
        
        if len(x) > 1:
            perf = x[1]
        elif self.Perf is None:
             raise ValueError("Perf is None")
        else:
            perf = self.Perf
        
        #pprint([f,perf])

        return [f,perf]

    def fit_dict_update(self,x,loss,f_scale,method,bounds,x_scale,verb):
        ret = self.fit_dict(x,loss,f_scale,method,bounds,x_scale,verb)
        ret.update({'F':self.F,'Tl':self.Tl,'perf':self.Perf,'ty':self.ty})
        return ret
    
    @classmethod
    def load_perf_data(self,file_name,ty=None):
        
        t = ty if ty is not None else self.ty

        if t == ArchType.XU3_SINGLE:
            data_index = 5
        else:
            data_index = 7

        return self.load_data(file_name,data_index,ty=ty)

