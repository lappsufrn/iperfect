#! /usr/bin/env python3
from pprint import pprint
from decimal import Decimal
from BaseModel import WORK_PATH
import numpy as np
import csv
import os
import pandas as pd

def load_cpu(file_name,run):

	d = dict()

	df = pd.read_csv(file_name,skiprows=1,header=None)
	
	d = {run:{'#Timestamp': df[0].values,'A7 Power(W)':df[3].values,'A15 Power(W)':df[6].values}}

	return d

def load_sp(file_name,run):

	d = dict()

	df = pd.read_csv(file_name,skiprows=1,header=None)

	d = {run : {'#Timestamp': df[0].values,'Power':df[3].values}}

	return d

def load_tx2(file_name,run):

	d = dict()

	df = pd.read_csv(file_name,skiprows=1,header=None)

	d = {run : {'#Timestamp': df[0].values,'Total_Power':df[1].values,'CPU_Power':df[4].values,'GPU_Power':df[5].values}}

	return d

def load_powprofile(file_name,run):

	d = dict()

	df = pd.read_csv(file_name,skiprows=1,header=None)
	#total,cpu,gpu 
	d = {run : {'Power':[df[0].values[0],df[1].values[0],df[2].values[0]], \
				'Energy':[df[3].values[0],df[4].values[0],df[5].values[0]], \
				'Battery':[df[6].values[0],df[7].values[0],df[8].values[0]]}}

	return d	

def load_benchmark(file_name,run):

	d = dict()

	df = pd.read_csv(file_name,sep='[ \t]', engine='python',header=None)

	d = {run : {'Start': int(df[1].values),'End': int(df[2].values),'Time':float(int(df[2].values) - int(df[1].values))/1000000000}}

	return d

def load_roitime(file_name,run):
	df = pd.read_csv(file_name,header=None)

	d = {run : {'roitime': float(df[1].values) }}

	return d

def load_log(file_name,run):

	d = dict()

	df = open(file_name,"r").readlines()

	roi_lines = [string for string in df if 'roi.' in string]
	
	if not roi_lines:
		return None

	d = {run : {'logroi': float(roi_lines[0].split("|")[1].strip('\n')),'logjoules':float(roi_lines[1].split("|")[2].strip('\n'))}}

	return d  


def load_nornir_stats(file_name,run):

	d = dict()

	df = pd.read_csv(file_name, sep='\t', engine='python',skiprows=1,header=None)

	d = {run:{'#Timestamp': df[0].values,'Power':df[12].values}}

	return d

def load_nornir_summary(file_name,run):
	
	d = dict()

	df = pd.read_csv(file_name, sep='\t', engine='python',skiprows=1,header=None)
	
	d = {run:{'Throughput': int(df[1].values),'CompletionTimeSec':float(df[2].values)}}

	return d

def create_time_spaced_array(d,mode='other'):
	if mode == "nornir":
		div = 1000
	#elif mode == 'tx2':
	#	div = 1000000
	else:
		div = 1000000000

	for key, run in d.items():	
		for key, values in run.items():
			if key == '#Timestamp':
				acumtime_spaced = [0]
				for i in range(1,len(values)):
					temp = float((values[i] - values[i-1]))/div + acumtime_spaced[i-1]
					acumtime_spaced.append(temp)
					
		run['time_spaced'] = acumtime_spaced

def calculate_nornir_summary(d):
	
	through_by_run =[]
	timesec_by_run = []

	for key, run in d.items():
		if str(key).isdigit():
			through_by_run.append(run['Throughput'])
			timesec_by_run.append(run['CompletionTimeSec'])

	d['throughput_all_run'] = np.median(through_by_run)
	d['timesec_all_run'] = np.median(timesec_by_run)	

def calculate_benchmark(d):

	for key, app in d.items():
		if key == 'freq_pair':
			continue
		
		time_by_run = []
		for key2, run in app.items():
			time_diff = run['Time']
			time_by_run.append( time_diff )
			run['time_diff'] = time_diff

		app['median'] = np.median(time_by_run)

def calculate_energy_cpu(d):
	
	energy = []
	power_a7_by_run = []
	power_a15_by_run = []
	time_by_run = []

	for key, run in d.items():

		if key == 'freq_pair':
			continue
		
		ea = np.trapz(run['A7 Power(W)'],x=run['time_spaced'])
		eb = np.trapz(run['A15 Power(W)'],x=run['time_spaced'])
		energy.append(ea + eb)

		time_diff = float((run['#Timestamp'][-1]-run['#Timestamp'][0]))/1000000000
		av7 = ea/time_diff
		av15 = eb/time_diff
		#av7 = np.median(run['A7 Power(W)'])
		#av15 = np.median(run['A15 Power(W)'])

		power_a7_by_run.append(av7)
		power_a15_by_run.append(av15)
		time_by_run.append(time_diff)

		run['A7 Power Ave'] = av7
		run['A15 Power Ave'] = av15
		run['time'] = time_diff
		run['energy'] = ea + eb

	d['energy_all_run'] = np.median(energy)
	d['powera7'] = np.median(power_a7_by_run)
	d['powera15'] = np.median(power_a15_by_run)
	d['runtime'] = np.median(time_by_run)

	#pprint(energy)
	#pprint(power_a7_by_run)
	#pprint(power_a15_by_run)
	#pprint(time_by_run)
		#pprint("***")
		#pprint(freq_pair['freq_pair'])
		#pprint('times '+   str(sorted(time_by_run))+ ' : '     +str(freq_pair['runtime']))
		#pprint('power7 '+  str(sorted(power_a7_by_run))+ ' : ' +str(freq_pair['powera7']))
		#pprint('powera15 '+str(sorted(power_a15_by_run))+ ' : '+str(freq_pair['powera15']))
		#pprint('energy '+  str(sorted(energy))+ ' : '          +str(freq_pair['energy_by_freq']))

def calculate_energy_sp(d,mode='other'):

	energy = []
	power_by_run = []
	
	time_by_run = []

	if mode == "nornir":
		div = 1000
	else:
		div = 1000000000

	for key, run in d.items():
		if key == 'freq_pair':
			continue

		e = np.trapz(run['Power'],x=run['time_spaced'])
	
		energy.append(e)

		time_diff = float((run['#Timestamp'][-1]-run['#Timestamp'][0]))/div
		p = e/time_diff
	
		power_by_run.append(p)
	
		time_by_run.append(time_diff)

		run['Power_ave'] = p
		run['time'] = time_diff
		run['energy'] = e

	d['energy_all_run'] = np.median(energy)
	d['power_all_run'] = np.median(power_by_run)
	d['runtime'] = np.median(time_by_run)

def calculate_energy_tx2(d):

	pprint('calculate_energy_tx2')

	ene_total_run = []
	ene_cpu_run = []
	ene_gpu_run = []

	power_total_run = []
	power_cpu_run = []
	power_gpu_run = []
	
	time_by_run = []

	for key, run in d.items():
		if key == 'freq_pair':
			continue

		ene_total = np.trapz(run['Total_Power'],x=run['time_spaced'])
		ene_cpu = np.trapz(run['CPU_Power'],x=run['time_spaced'])
		ene_gpu = np.trapz(run['GPU_Power'],x=run['time_spaced'])
	
		ene_total_run.append(ene_total)
		ene_cpu_run.append(ene_cpu)
		ene_gpu_run.append(ene_gpu)

		time_diff = float((run['#Timestamp'][-1]-run['#Timestamp'][0]))/1000000000
		
		p_total = ene_total/time_diff
		p_cpu = ene_cpu/time_diff
		p_gpu = ene_gpu/time_diff
	
		power_total_run.append(p_total)
		power_cpu_run.append(p_cpu)
		power_gpu_run.append(p_gpu)
	
		time_by_run.append(time_diff)

		run['time'] = time_diff

		run['total_power_Ave'] = p_total
		run['cpu_power_Ave'] = p_cpu
		run['gpu_power_Ave'] = p_gpu
		
		run['energy_total'] = ene_total
		run['energy_cpu'] = ene_cpu
		run['energy_gpu'] = ene_gpu

	d['runtime'] = np.median(time_by_run)

	d['energy_total_all_run'] = np.median(ene_total_run)
	d['energy_cpu_all_run'] = np.median(ene_cpu_run)
	d['energy_gpu_all_run'] = np.median(ene_gpu_run)

	d['total_power_all_run'] = np.median(power_total_run)
	d['cpu_power_all_run'] = np.median(power_cpu_run)
	d['gpu_power_all_run'] = np.median(power_gpu_run)

def save_tab_result_cpu(d, file_name):

	pprint('SAVE CPU')

	with open(file_name, 'w') as csvfile:
		fieldnames = ['Fl','Fb','li','bi','A7 Power(W)','A15 Power(W)','Run Time (s)','energy']
		writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
		writer.writeheader()

		for key, values in d.items():
			pprint(key)

			writer.writerow({'Fl':int(values['freq_pair'][1]/1000),'Fb':int(values['freq_pair'][0]/1000),'li':values['freq_pair'][3],'bi':values['freq_pair'][2],'A7 Power(W)': round(values['powera7'],4) ,'A15 Power(W)': round(values['powera15'],4),'Run Time (s)': values['runtime'] ,'energy': round(values['energy_all_run'],4)})

def save_tab_all_result_cpu(d, file_name):

	pprint('SAVE CPU ALL')

	with open(file_name, 'w') as csvfile:
		fieldnames = ['Fl','Fb','li','bi','Run','A7 Power(W)','A15 Power(W)','Run Time (s)','energy']
		writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
		writer.writeheader()

		for key_v, values in d.items():
			pprint('keyv ' + str(key_v))

			for key_r, run in values.items():
				pprint('key_r '+str(key_r))
				if str(key_r).isdigit():	
					writer.writerow({'Fl':int(values['freq_pair'][1]/1000),'Fb':int(values['freq_pair'][0]/1000),'li':values['freq_pair'][3],'bi':values['freq_pair'][2],'Run':key_r, 'A7 Power(W)': round(run['A7 Power Ave'],4) ,'A15 Power(W)': round(run['A15 Power Ave'],4),'Run Time (s)': run['time'] ,'energy': round(run['energy'],4)})

def save_tab_result_sp(d, file_name):

	pprint('SAVE SP')

	with open(file_name, 'w') as csvfile:
		fieldnames = ['Fl','Fb','li','bi','Power(W)','Run Time (s)','energy(J)']
		writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
		writer.writeheader()

		for key, values in d.items():
			pprint('key2 ' + str(key))				
			writer.writerow({'Fl':int(values['freq_pair'][1]/1000),'Fb':int(values['freq_pair'][0]/1000),'li':values['freq_pair'][3],'bi':values['freq_pair'][2],'Power(W)':round(values['power_all_run'],4),'Run Time (s)' : values['runtime'] ,'energy(J)': round(values['energy_all_run'],4)})
		
def save_tab_all_result_sp(d, file_name):

	pprint('SAVE SP ALL')

	with open(file_name, 'w') as csvfile:
		fieldnames = ['Fl','Fb','Run','li','bi','Power(W)','Run Time (s)','energy(J)']
		writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
		writer.writeheader()

		for key, values in d.items():
			pprint('key1 ' + str(key))
			for run_key,run_values in values.items():
				if str(run_key).isdigit():	
					writer.writerow({'Fl':int(values['freq_pair'][1]/1000),'Fb':int(values['freq_pair'][0]/1000),'li':values['freq_pair'][3],'bi':values['freq_pair'][2],'Run':run_key,'Power(W)':round(run_values['Power_ave'],4),'Run Time (s)' : run_values['time'] ,'energy(J)': round(run_values['energy'],4)})


def save_tab_result_nornir_sp(d, file_name):

	pprint('SAVE SP')

	with open(file_name, 'w') as csvfile:
		fieldnames = ['Scenario','Fl','Fb','li','bi','Power(W)','Run Time (s)','energy(J)']
		writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
		writer.writeheader()

		for key, values in d.items():
			pprint('key2 ' + str(key))				
			writer.writerow({'Scenario':key[5:14] ,'Fl':int(values['freq_pair'][1]/1000),'Fb':int(values['freq_pair'][0]/1000),'li':values['freq_pair'][3],'bi':values['freq_pair'][2],'Power(W)':round(values['power_all_run'],4),'Run Time (s)' : values['runtime'] ,'energy(J)': round(values['energy_all_run'],4)})
		
def save_tab_all_result_nornir_sp(d, file_name):

	pprint('SAVE SP ALL')

	with open(file_name, 'w') as csvfile:
		fieldnames = ['Scenario','Fl','Fb','Run','li','bi','Power(W)','Run Time (s)','energy(J)']
		writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
		writer.writeheader()

		for key, values in d.items():
			pprint('key1 ' + str(key))
			for run_key,run_values in values.items():
				if str(run_key).isdigit():	
					writer.writerow({'Scenario':key[5:14] ,'Fl':int(values['freq_pair'][1]/1000),'Fb':int(values['freq_pair'][0]/1000),'li':values['freq_pair'][3],'bi':values['freq_pair'][2],'Run':run_key,'Power(W)':round(run_values['Power_ave'],4),'Run Time (s)' : run_values['time'] ,'energy(J)': round(run_values['energy'],4)})

def save_tab_result_tx2(d, file_name,is_two_apps):

	pprint('SAVE TX2')

	with open(file_name, 'w') as csvfile:
		fieldnames = ['Fl','Fb']
		if is_two_apps:
			fieldnames.extend(['li_A','bi_A','li_B','bi_B'])
		else:
			fieldnames.extend(['li','bi'])

		fieldnames.extend(['Total Power(W)','CPU Power(W)','GPU Power(W)','Run Time (s)','Total energy(J)','CPU energy(J)','GPU energy(J)'])
		writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
		writer.writeheader()

		for key, values in d.items():
			pprint('key ' + str(key))	
			d_row = {'Fl':int(values['freq_pair'][1]),'Fb':int(values['freq_pair'][0])}
			
			if is_two_apps: 
				d_row.update({'li_A':values['freq_pair'][3],'bi_A':values['freq_pair'][2],'li_B':values['freq_pair'][5],'bi_B':values['freq_pair'][4]})
			else:
				d_row.update({'li':values['freq_pair'][3],'bi':values['freq_pair'][2]})

			d_row.update({'Total Power(W)':round(values['total_power_all_run'],4),'CPU Power(W)':round(values['cpu_power_all_run'],4),'GPU Power(W)':round(values['gpu_power_all_run'],4),'Run Time (s)' : round(values['runtime'],4) ,'Total energy(J)': round(values['energy_total_all_run'],4),'CPU energy(J)': round(values['energy_cpu_all_run'],4),'GPU energy(J)': round(values['energy_gpu_all_run'],4)})					
			writer.writerow(d_row)
		
def save_tab_all_result_tx2(d, file_name,is_two_apps):

	pprint('SAVE TX2 ALL')

	with open(file_name, 'w') as csvfile:
		fieldnames = ['Fl','Fb','Run']
		if is_two_apps:
			fieldnames.extend(['li_A','bi_A','li_B','bi_B'])
		else:
			fieldnames.extend(['li','bi'])

		fieldnames.extend(['Total Power(W)','CPU Power(W)','GPU Power(W)','Run Time (s)','Total energy(J)','CPU energy(J)','GPU energy(J)'])
		writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
		writer.writeheader()

		for key, values in d.items():
			pprint('key1 ' + str(key))
			for run_key,run_values in values.items():
				if str(run_key).isdigit():	  
					d_row = {'Fl':int(values['freq_pair'][1]),'Fb':int(values['freq_pair'][0])}
					
					if is_two_apps: 
						d_row.update({'li_A':values['freq_pair'][3],'bi_A':values['freq_pair'][2],'li_B':values['freq_pair'][5],'bi_B':values['freq_pair'][4]})
					else:
						d_row.update({'li':values['freq_pair'][3],'bi':values['freq_pair'][2]})
					
					d_row.update({'Run':run_key,'Total Power(W)':round(run_values['total_power_Ave'],4),'CPU Power(W)':round(run_values['cpu_power_Ave'],4),'GPU Power(W)':round(run_values['gpu_power_Ave'],4),'Run Time (s)' : run_values['time'] ,'Total energy(J)': round(run_values['energy_total'],4),'CPU energy(J)': round(run_values['energy_cpu'],4),'GPU energy(J)': round(run_values['energy_gpu'],4)})
					writer.writerow(d_row)

def save_tab_result_benchmark(d, file_name,is_two_apps):

	pprint('SAVE BENCH') 

	with open(file_name, 'w') as csvfile:
		fieldnames = ['Fl','Fb']
		if is_two_apps:
			fieldnames.extend(['li_A','bi_A','li_B','bi_B'])
		else:
			fieldnames.extend(['li','bi'])

		fieldnames.extend(['Application','Run Time (s)'])
		writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
		writer.writeheader()

		for key, values in d.items():
			pprint('key ' + str(key))	

			d_row = {'Fl':int(values['freq_pair'][1]),'Fb':int(values['freq_pair'][0])}
			
			if is_two_apps: 
				d_row.update({'li_A':values['freq_pair'][3],'bi_A':values['freq_pair'][2],'li_B':values['freq_pair'][5],'bi_B':values['freq_pair'][4]})
			else:
				d_row.update({'li':values['freq_pair'][3],'bi':values['freq_pair'][2]})

			for k,t in values.items():
				pprint('key2 ' + str(k))
				if k == 'freq_pair':
					continue 

				d_row.update({'Application': k,'Run Time (s)' : round(t['median'],4)})					
				writer.writerow(d_row)

def save_tab_all_result_benchmark(d, file_name,is_two_apps):

	pprint('SAVE BENCH ALL') 

	with open(file_name, 'w') as csvfile:
		fieldnames = ['Fl','Fb','Run']
		if is_two_apps:
			fieldnames.extend(['li_A','bi_A','li_B','bi_B'])
		else:
			fieldnames.extend(['li','bi'])

		fieldnames.extend(['Application','Run Time (s)'])
		writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
		writer.writeheader()

		for key, values in d.items():
			pprint('key ' + str(key))	
			d_row = {'Fl':int(values['freq_pair'][1]),'Fb':int(values['freq_pair'][0])}
			
			if is_two_apps: 
				d_row.update({'li_A':values['freq_pair'][3],'bi_A':values['freq_pair'][2],'li_B':values['freq_pair'][5],'bi_B':values['freq_pair'][4]})
			else:
				d_row.update({'li':values['freq_pair'][3],'bi':values['freq_pair'][2]})
			
			for k,t in values.items():
				pprint('k ' + str(k))
				
				if k == 'freq_pair':
					continue 				
					
				for k2,r in t.items():
					pprint('k2 ' + str(k2))
					if k2 == 'median':
						continue
					d_row.update({'Run': k2 ,'Application': k,'Run Time (s)' : round(r['time_diff'],4)})					
					writer.writerow(d_row)

def save_tab_result_logroi(d, file_name):
	pprint('SAVE LOGROI')

	with open(file_name, 'w') as csvfile:
		fieldnames = ['Fl','Fb','li','bi','RoiTime(S)','ParsecRoi(S)','ParsecEnergy(J)']
		writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
		writer.writeheader()

		for key, values in d.items():
			pprint('key ' + str(key))
			line = {}
			line.update({'Fl':int(values['freq_pair'][1]/1000),'Fb':int(values['freq_pair'][0]/1000)})
			line.update({'li':values['freq_pair'][3],'bi':values['freq_pair'][2]})
			line.update({'RoiTime(S)':round(values['RoiTime'],4), 'ParsecRoi(S)' : values['LogRoi']})
			line.update({'ParsecEnergy(J)': round(values['LogJoules'] if values['LogJoules'] else 0.0,4)})
			writer.writerow(line)

def save_tab_all_result_logroi(d, file_name):

	pprint('SAVE LOGROI ALL')

	with open(file_name, 'w') as csvfile:
		fieldnames = ['Fl','Fb','Run','li','bi','RoiTime(S)','ParsecRoi(S)','ParsecEnergy(J)']
		writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
		writer.writeheader()

		for key, values in d.items():
			pprint('key1 ' + str(key))
			for run_key,run_values in values.items():
				line = {}
				if str(run_key).isdigit():	
					line.update({'Fl':int(values['freq_pair'][1]/1000),'Fb':int(values['freq_pair'][0]/1000)})
					line.update({'li':values['freq_pair'][3],'bi':values['freq_pair'][2],'Run':run_key})
					line.update({'RoiTime(S)':round(run_values['roitime'],4),'ParsecRoi(S)' : run_values['logroi']})
					line.update({'ParsecEnergy(J)': round(run_values['logjoules'],4)})
					writer.writerow(line)

def save_tab_result_powprofiler(d,file_name,is_two_apps=False):
	pprint('SAVE POWPROFILER')	

	with open(file_name, 'w') as csvfile:
		fieldnames = ['Fl','Fb']
		if is_two_apps:
			fieldnames.extend(['li_A','bi_A','li_B','bi_B'])
		else:
			fieldnames.extend(['li','bi'])

		fieldnames.extend(['Power(W)','Energy(J)','Battery(%)'])
		writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
		writer.writeheader()		

		for key, values in d.items():
			pprint('key ' + str(key))

			line = {'Fl':int(values['freq_pair'][1]),'Fb':int(values['freq_pair'][0])}
			
			if is_two_apps: 
				line.update({'li_A':values['freq_pair'][3],'bi_A':values['freq_pair'][2],'li_B':values['freq_pair'][5],'bi_B':values['freq_pair'][4]})
			else:
				line.update({'li':values['freq_pair'][3],'bi':values['freq_pair'][2]})

			line.update({'Power(W)':round(values['median'][0][0],4),'Energy(J)':round(values['median'][1][0],4),'Battery(%)':round(values['median'][2][0],4)})

			writer.writerow(line)

def save_tab_result_nornir(d, file_name):

	pprint('SAVE nornir')

	with open(file_name, 'w') as csvfile:
		fieldnames = ['Scenario','Configuration(l_b_fl_fb)','Throughput','Power(W)','ROI Time (s)','energy(J)']
		writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
		writer.writeheader()

		for key_1, values in d.items():
			writer.writerow({'Scenario':key_1[5:14] ,'Configuration(l_b_fl_fb)':key_1[15:],'Power(W)': round(values['power_all_run'],4),'Throughput': int(values['throughput_all_run']),'ROI Time (s)': round(values['timesec_all_run'],4) ,'energy(J)': round(values['energy_all_run'],4)})

def save_tab_all_result_nornir(d, file_name):

	pprint('save_all nornir')

	with open(file_name, 'w') as csvfile:
		fieldnames = ['Scenario','Run','Configuration(l_b_fl_fb)','Throughput','Power(W)','ROI Time (s)','Energy(J)']
		writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
		writer.writeheader()

		for key_v, values in d.items():
			for key_r, run in values.items():
				if str(key_r).isdigit():
					writer.writerow({'Scenario':key_v[5:14] ,'Run':key_r,'Configuration(l_b_fl_fb)':key_v[15:], 'Power(W)': round(run['Power_ave'],4),'Throughput':run['Throughput'], 'ROI Time (s)': round(run['CompletionTimeSec'],4) ,'Energy(J)': round(run['energy'],4)})					

def is_parsec_benchmark(filename): 
	parsec = ['blackscholes','bodytrack','freqmine','dedup','fluidanimate']
	return (True if filename[:-4] in parsec else False)

def save_tab_governor_sp(d, file_name):

	pprint('SAVE_SP')

	with open(file_name, 'w') as csvfile:
		fieldnames = ['Governor','bi','li','Power(W)','Run Time (s)','Energy(J)']
		writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
		writer.writeheader()

		for g, values in d.items():
			pprint(g)
			writer.writerow({'Governor':g.split('_')[0],'bi':g.split('_')[1],'li':g.split('_')[2],'Power(W)':round(values['power_all_run'],4),'Run Time (s)' : values['runtime'] ,'Energy(J)': round(values['energy_all_run'],4)})

def save_tab_all_governor_sp(d, file_name):

	pprint('save_all SP')

	with open(file_name, 'w') as csvfile:
		fieldnames = ['Governor','bi','li','Run','Power(W)','Run Time (s)','Energy(J)']
		writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
		writer.writeheader()

		for g, values in d.items():
			for key_r, run_values in values.items():
				if str(key_r).isdigit():
					writer.writerow({'Governor':g.split('_')[0],'bi':g.split('_')[1],'li':g.split('_')[2],'Run':key_r,'Power(W)':round(run_values['Power_ave'],4),'Run Time (s)' : run_values['time'] ,'Energy(J)': round(run_values['energy'],4)})

def save_tab_governor_logroi(d, file_name):
	pprint('SAVE LOGROI')

	with open(file_name, 'w') as csvfile:
		fieldnames = ['Governor','bi','li','RoiTime(S)','ParsecRoi(S)','ParsecEnergy(J)']
		writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
		writer.writeheader()

		for g, values in d.items():
			pprint('key ' + str(g))
			line = {'Governor':g.split('_')[0],'bi':g.split('_')[1],'li':g.split('_')[2]}
			line.update({'RoiTime(S)':round(values['RoiTime'],4), 'ParsecRoi(S)' : values['LogRoi']})
			line.update({'ParsecEnergy(J)': round(values['LogJoules'] if values['LogJoules'] else 0.0,4)})
			writer.writerow(line)

def save_tab_all_governor_logroi(d, file_name):

	pprint('SAVE LOGROI ALL')

	with open(file_name, 'w') as csvfile:
		fieldnames = ['Governor','bi','li','Run','RoiTime(S)','ParsecRoi(S)','ParsecEnergy(J)']
		writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
		writer.writeheader()

		for g, values in d.items():
			pprint('key1 ' + str(g))
			for run_key,run_values in values.items():
				line = {}
				if str(run_key).isdigit():	
					line.update({'Governor':g.split('_')[0],'bi':g.split('_')[1],'li':g.split('_')[2],'Run':run_key})
					line.update({'RoiTime(S)':round(run_values['roitime'],4),'ParsecRoi(S)' : run_values['logroi']})
					line.update({'ParsecEnergy(J)': round(run_values['logjoules'],4)})
					writer.writerow(line)

def analyse_configs_files(path,dir_names,is_two_apps=False):
	for dir_name in dir_names:
		pprint(dir_name)
		DIR = path+dir_name
		
		dic_final_sp = {dir_name : {}}
		dic_final_cpu = {dir_name : {}}
		dic_final_tx2 = {dir_name : {}}
		dic_final_bench = {dir_name : {}}
		dic_final_log_roi = {dir_name : {}}
		dic_final_powprofi = {dir_name : {}}

		for core_comb in os.listdir(DIR):
			pprint("---cores---")
			pprint(core_comb)

			if os.path.isfile(DIR+'/'+core_comb): 
				continue
			bi = []
			Li = []

			if is_two_apps:
				try:
					bi.append(int(core_comb.split('_')[2]))
					Li.append(int(core_comb.split('_')[3]))
					bi.append(int(core_comb.split('_')[4]))
					Li.append(int(core_comb.split('_')[5]))
				except ValueError:
					pprint(ValueError.str())				
			else:
				try:
					bi.append(int(core_comb.split('_')[1]))
					Li.append(int(core_comb.split('_')[2]))
				except ValueError:
					pprint(ValueError.str())

			gointo=DIR+'/'+core_comb

			for freq_comb in os.listdir(gointo):
				pprint('frequencies')
				pprint(freq_comb)

				if os.path.isfile(gointo+'/'+freq_comb): 
					continue
				try:
					fb = int(freq_comb.split('_')[0])
					fl = int(freq_comb.split('_')[1])		
				except ValueError:
					pprint(ValueError.str())

				key=core_comb+'_'+freq_comb

				if is_two_apps:
					temp_cores = [fb,fl,bi[0],Li[0],bi[1],Li[1]]
				else:
					temp_cores = [fb,fl,bi[0],Li[0]]

				dic_final_sp[dir_name].update({key : {'freq_pair' : temp_cores}})
				dic_final_cpu[dir_name].update({key : {'freq_pair' : temp_cores}})
				dic_final_tx2[dir_name].update({key : {'freq_pair' : temp_cores}})
				dic_final_bench[dir_name].update({key : {'freq_pair' : temp_cores}})
				dic_final_log_roi[dir_name].update({key : {'freq_pair' : temp_cores}})
				dic_final_powprofi[dir_name].update({key : {'freq_pair' : temp_cores}})
				
				roi_runs = {'roitime':[],'logjoules':[], 'logroi':[]}
				pow_runs = {'power':[],'energy':[], 'battery':[]}

				for run in os.listdir(gointo + '/'+ freq_comb):	
					pprint('run ' + run)

					if os.path.isfile(gointo + '/'+ freq_comb+ '/'+run): 
						continue
					
					try:
						run_i = int(run.split('_')[1])		
					except ValueError:
						pprint(ValueError.str())

					for filename in os.listdir(gointo + '/'+ freq_comb + '/' + run):
						
						if filename == 'sp_data.csv':
							pprint(filename)
							d_sp = load_sp(gointo + '/'+ freq_comb + '/'+ run + '/' + filename,run_i)
					
							create_time_spaced_array(d_sp,1)							
							dic_final_sp[dir_name][key].update(d_sp)							

						if filename == 'sensors.csv':
							pprint(filename)
							d_cpu = load_cpu(gointo + '/'+ freq_comb + '/'+ run + '/' + filename,run_i)
						
							create_time_spaced_array(d_cpu,3)							
							dic_final_cpu[dir_name][key].update(d_cpu)							

						if filename == 'power.csv':
							pprint(filename)
							d_tx2 = load_tx2(gointo + '/'+ freq_comb + '/'+ run + '/' + filename,run_i)
							
							create_time_spaced_array(d_tx2,'tx2')
							dic_final_tx2[dir_name][key].update(d_tx2)

						if "benchmark.csv" in filename:		
							#pprint(filename)
							d_benchmark = load_benchmark(gointo + '/'+ freq_comb + '/'+ run + '/' + filename,run_i)
							
							app_name = filename.split('_')[0]

							#pprint('app ' + app_name)

							if app_name in dic_final_bench[dir_name][key].keys():
								dic_final_bench[dir_name][key][app_name].update(d_benchmark)
							else:
								dic_final_bench[dir_name][key].update({app_name : d_benchmark})					

						if is_parsec_benchmark(filename):

							#pprint(filename)

							d_log = load_log(gointo + '/'+ freq_comb + '/'+ run + '/' + filename,run_i)	

							if not d_log:
								with open('./problems/d_log_problems_'+dir_name+'.txt', 'a') as file:
									line = gointo + '/'+ freq_comb + '/'+ run + '/' + filename + '\n'
									file.write(line)
								continue

							if run_i in dic_final_log_roi[dir_name][key].keys():
								dic_final_log_roi[dir_name][key][run_i].update(d_log[run_i])
							else:
								dic_final_log_roi[dir_name][key].update(d_log)						
							
							roi_runs['logjoules'].append(d_log[run_i]['logjoules'])
							roi_runs['logroi'].append(d_log[run_i]['logroi'])


						if filename == 'roitime':
							#pprint(filename)

							d_roitime = load_roitime(gointo + '/'+ freq_comb + '/'+ run + '/' + filename,run_i)
							
							if run_i in dic_final_log_roi[dir_name][key].keys():
								dic_final_log_roi[dir_name][key][run_i].update(d_roitime[run_i])
							else:
								dic_final_log_roi[dir_name][key].update(d_roitime)	

							roi_runs['roitime'].append(d_roitime[run_i]['roitime'])
						if filename == 'twoApps.csv':

							d_powprofi = load_powprofile(gointo + '/'+ freq_comb + '/'+ run + '/' + filename,run_i)
							
							dic_final_powprofi[dir_name][key].update(d_powprofi)
							
							pow_runs['power'].append(d_powprofi[run_i]['Power'])
							pow_runs['energy'].append(d_powprofi[run_i]['Energy'])
							pow_runs['battery'].append(d_powprofi[run_i]['Battery'])

				dic_final_log_roi[dir_name][key].update({'RoiTime':np.median(roi_runs['roitime'])})
				dic_final_log_roi[dir_name][key].update({'LogJoules':np.median(roi_runs['logjoules'])})
				dic_final_log_roi[dir_name][key].update({'LogRoi':np.median(roi_runs['logroi'])})
				
				dic_final_powprofi[dir_name][key].update({'median':[np.median(pow_runs['power'],axis=0), \
														np.median(pow_runs['energy'],axis=0),\
														np.median(pow_runs['battery'],axis=0)]})			
				#input('')
				#expression_if_true if condition else expression_if_false 
				calculate_energy_sp(dic_final_sp[dir_name][key]) if 1 in dic_final_sp[dir_name][key] else None
				calculate_energy_cpu(dic_final_cpu[dir_name][key]) if 1 in dic_final_cpu[dir_name][key] else None
				calculate_energy_tx2(dic_final_tx2[dir_name][key]) if 1 in dic_final_tx2[dir_name][key] else None

				calculate_benchmark(dic_final_bench[dir_name][key]) if app_name else None
	
		save_tab_result_sp(dic_final_sp[dir_name],WORK_PATH + 'Analysed_results/' + dir_name + '_sp.csv')	          if 1 in dic_final_sp[dir_name][key] else None	
		save_tab_all_result_sp(dic_final_sp[dir_name],WORK_PATH + 'Analysed_results/' + dir_name + '_all_sp.csv')	  if 1 in dic_final_sp[dir_name][key] else None
		save_tab_result_cpu(dic_final_cpu[dir_name],WORK_PATH + 'Analysed_results/' + dir_name + '_cpu.csv')	      if 1 in dic_final_cpu[dir_name][key] else None	
		save_tab_all_result_cpu(dic_final_cpu[dir_name],WORK_PATH + 'Analysed_results/' + dir_name + '_all_cpu.csv')  if 1 in dic_final_cpu[dir_name][key] else None	
		save_tab_result_tx2(dic_final_tx2[dir_name],WORK_PATH + 'Analysed_results/' + dir_name + '_tx2.csv',is_two_apps)	      if 1 in dic_final_tx2[dir_name][key] else None	
		save_tab_all_result_tx2(dic_final_tx2[dir_name],WORK_PATH + 'Analysed_results/' + dir_name + '_all_tx2.csv',is_two_apps)  if 1 in dic_final_tx2[dir_name][key] else None		
		save_tab_result_benchmark(dic_final_bench[dir_name],WORK_PATH + 'Analysed_results/' + dir_name + '_bench_tx2.csv',is_two_apps)	      if app_name else None	
		save_tab_all_result_benchmark(dic_final_bench[dir_name],WORK_PATH + 'Analysed_results/' + dir_name + '_bench_all_tx2.csv',is_two_apps)  if app_name else None				
		save_tab_result_logroi(dic_final_log_roi[dir_name],WORK_PATH + 'Analysed_results/' + dir_name + '_log_roi_xu3.csv')	      if 1 in dic_final_log_roi[dir_name][key] else None	
		save_tab_all_result_logroi(dic_final_log_roi[dir_name],WORK_PATH + 'Analysed_results/' + dir_name + '_log_roi_xu3_all.csv')   if 1 in dic_final_log_roi[dir_name][key] else None				
		save_tab_result_powprofiler(dic_final_powprofi[dir_name],WORK_PATH + 'Analysed_results/' + dir_name + '_powprofiler.csv',is_two_apps=True)	      if 1 in dic_final_powprofi[dir_name][key] else None	
		#save_tab_all_result_powprofiler(dic_final_powprofi[dir_name],WORK_PATH + 'Analysed_results/' + dir_name + '_powprofiler_all.csv',is_two_apps=True)   if 1 in dic_final_powprofi[dir_name][key] else None				

def analyse_nornir_files(path,dir_names):
	is_two_apps=False

	for dir_name in dir_names:
		pprint(dir_name)
		DIR = path+dir_name
		
		dic_final_sp = {dir_name : {}}
		dic_final_cpu = {dir_name : {}}
		dic_final_tx2 = {dir_name : {}}
		dic_final_bench = {dir_name : {}}
		dic_final_log_roi = {dir_name : {}}
		dic_final_nornir =  {dir_name : {}}
		
		for scenarios in os.listdir(DIR):

			if os.path.isfile(scenarios): 
				continue

			pprint("---scenarios---")
			pprint(scenarios)

			temp_cores= [int(scenarios.split('_')[6])*1000,int(scenarios.split('_')[5])*1000,int(scenarios.split('_')[4]),int(scenarios.split('_')[3])]

			dic_final_sp[dir_name].update({scenarios:{'freq_pair' : temp_cores}})
			dic_final_cpu[dir_name].update({scenarios:{'freq_pair' : temp_cores}})
			dic_final_tx2[dir_name].update({scenarios:{'freq_pair' : temp_cores}})
			dic_final_bench[dir_name].update({scenarios:{'freq_pair' : temp_cores}})
			dic_final_log_roi[dir_name].update({scenarios:{'freq_pair' : temp_cores}})
			dic_final_nornir[dir_name].update({scenarios:{}})

			gointo=DIR+'/'+scenarios
			roi_runs = {'roitime':[],'logjoules':[], 'logroi':[]}
			for run in os.listdir(gointo):	
				pprint('run ' + run)

				if os.path.isfile(run): 
					continue
				
				try:
					run_i = int(run.split('_')[1])		
				except ValueError:
					pprint(ValueError.str())

				for filename in os.listdir(gointo + '/' + run):

					if filename == 'sp_data.csv':
						pprint(filename)
						d_sp = load_sp(gointo + '/' + run + '/' + filename,run_i)
				
						create_time_spaced_array(d_sp)							
						dic_final_sp[dir_name][scenarios].update(d_sp)							

					if filename == 'sensors.csv':
						pprint(filename)
						d_cpu = load_cpu(gointo + '/' + run + '/' + filename,run_i)
					
						create_time_spaced_array(d_cpu)							
						dic_final_cpu[dir_name][scenarios].update(d_cpu)							

					if filename == 'power.csv':
						pprint(filename)
						d_tx2 = load_tx2(gointo + '/' + run + '/' + filename,run_i)
						
						create_time_spaced_array(d_tx2,"tx2")
						dic_final_tx2[dir_name][scenarios].update(d_tx2)

					if "benchmark.csv" in filename:		
						pprint(filename)
						d_benchmark = load_benchmark(gointo + '/' + run + '/' + filename,run_i)
						
						app_name = filename.split('_')[0]

						pprint('app ' + app_name)

						if app_name in dic_final_bench[dir_name][scenarios].keys():
							dic_final_bench[dir_name][scenarios][app_name].update(d_benchmark)
						else:
							dic_final_bench[dir_name][scenarios].update({app_name : d_benchmark})					

					if is_parsec_benchmark(filename):

						pprint(filename)

						d_log = load_log(gointo + '/' + run + '/' + filename,run_i)	
						'''
						if not d_log:
							with open('./problems/d_log_problems_'+dir_name+'.txt', 'a') as file:
								line = gointo + '/'+ + run + '/' + filename + '\n'
								file.write(line)
							continue
						'''
						if run_i in dic_final_log_roi[dir_name][scenarios].keys():
							dic_final_log_roi[dir_name][scenarios][run_i].update(d_log[run_i])
						else:
							dic_final_log_roi[dir_name][scenarios].update(d_log)						
						
						roi_runs['logjoules'].append(d_log[run_i]['logjoules'])
						roi_runs['logroi'].append(d_log[run_i]['logroi'])

					if filename == 'roitime':
						pprint(filename)

						d_roitime = load_roitime(gointo + '/' + run + '/' + filename,run_i)
						
						if run_i in dic_final_log_roi[dir_name][scenarios].keys():
							dic_final_log_roi[dir_name][scenarios][run_i].update(d_roitime[run_i])
						else:
							dic_final_log_roi[dir_name][scenarios].update(d_roitime)	

						roi_runs['roitime'].append(d_roitime[run_i]['roitime'])

					if filename == 'summary.csv':
						pprint(filename)
						d_nornir = load_nornir_summary(gointo + '/'+ run + '/' + filename,run_i)
						dic_final_nornir[dir_name][scenarios].update(d_nornir)	
						
					if filename == 'stats.csv':
						pprint(filename)
						d_nornir = load_nornir_stats(gointo + '/'+ run + '/' + filename,run_i)
						create_time_spaced_array(d_nornir,"nornir")
						dic_final_nornir[dir_name][scenarios][run_i].update(d_nornir[run_i])						
			

			dic_final_log_roi[dir_name][scenarios].update({'RoiTime':np.median(roi_runs['roitime'])})
			dic_final_log_roi[dir_name][scenarios].update({'LogJoules':np.median(roi_runs['logjoules'])})
			dic_final_log_roi[dir_name][scenarios].update({'LogRoi':np.median(roi_runs['logroi'])})
			
			#input('')
			#expression_if_true if condition else expression_if_false 
			calculate_energy_sp(dic_final_sp[dir_name][scenarios]) if 1 in dic_final_sp[dir_name][scenarios] else None
			calculate_energy_cpu(dic_final_cpu[dir_name][scenarios]) if 1 in dic_final_cpu[dir_name][scenarios] else None
			calculate_energy_tx2(dic_final_tx2[dir_name][scenarios])  if 1 in dic_final_tx2[dir_name][scenarios] else None
			calculate_benchmark(dic_final_bench[dir_name][scenarios]) if app_name else None
			calculate_energy_sp(dic_final_nornir[dir_name][scenarios],'nornir') if 1 in dic_final_nornir[dir_name][scenarios] else None
			calculate_nornir_summary(dic_final_nornir[dir_name][scenarios]) if 1 in dic_final_nornir[dir_name][scenarios] else None			

		save_tab_result_nornir_sp(dic_final_sp[dir_name],WORK_PATH + 'Analysed_results/' + dir_name + '_sp.csv')	          if 1 in dic_final_sp[dir_name][scenarios] else None	
		save_tab_all_result_nornir_sp(dic_final_sp[dir_name],WORK_PATH + 'Analysed_results/' + dir_name + '_all_sp.csv')	  if 1 in dic_final_sp[dir_name][scenarios] else None
		#save_tab_result_cpu(dic_final_cpu[dir_name],WORK_PATH + 'Analysed_results/' + dir_name + '_cpu.csv')	      if 1 in dic_final_cpu[dir_name][scenarios] else None	
		#save_tab_all_result_cpu(dic_final_cpu[dir_name],WORK_PATH + 'Analysed_results/' + dir_name + '_all_cpu.csv')  if 1 in dic_final_cpu[dir_name][scenarios] else None	
		#save_tab_result_tx2(dic_final_tx2[dir_name],WORK_PATH + 'Analysed_results/' + dir_name + '_tx2.csv',is_two_apps)	      if 1 in dic_final_tx2[dir_name][scenarios] else None	
		#save_tab_all_result_tx2(dic_final_tx2[dir_name],WORK_PATH + 'Analysed_results/' + dir_name + '_all_tx2.csv',is_two_apps)  if 1 in dic_final_tx2[dir_name][scenarios] else None		
		#save_tab_result_benchmark(dic_final_bench[dir_name],WORK_PATH + 'Analysed_results/' + dir_name + '_bench_tx2.csv',is_two_apps)	      if app_name else None	
		#save_tab_all_result_benchmark(dic_final_bench[dir_name],WORK_PATH + 'Analysed_results/' + dir_name + '_bench_all_tx2.csv',is_two_apps)  if app_name else None				
		#save_tab_result_logroi(dic_final_log_roi[dir_name],WORK_PATH + 'Analysed_results/' + dir_name + '_log_roi_xu3.csv')	      if 1 in dic_final_log_roi[dir_name][scenarios] else None	
		#save_tab_all_result_logroi(dic_final_log_roi[dir_name],WORK_PATH + 'Analysed_results/' + dir_name + '_log_roi_xu3_all.csv')   if 1 in dic_final_log_roi[dir_name][scenarios] else None				
		#save_tab_result_nornir(dic_final_nornir[dir_name],WORK_PATH + 'Analysed_results/' + dir_name + '_nornir.csv')	if 1 in dic_final_nornir[dir_name][scenarios] else None
		#save_tab_all_result_nornir(dic_final_nornir[dir_name],WORK_PATH + 'Analysed_results/' + dir_name + '_all_nornir.csv')		if 1 in dic_final_nornir[dir_name][scenarios] else None

def analyse_governor_files(path,dir_names):
	is_two_apps=False

	for dir_name in dir_names:
		pprint("**"+dir_name+"**")

		DIR = path+dir_name
		
		dic_final_sp = {dir_name : {}}
		dic_final_cpu = {dir_name : {}}
		dic_final_tx2 = {dir_name : {}}
		dic_final_bench = {dir_name : {}}
		dic_final_log_roi = {dir_name : {}}
		

		for governor in os.listdir(DIR):
			
			if os.path.isfile(governor): 
				continue

			dic_final_sp[dir_name][governor] = dict()
			dic_final_cpu[dir_name][governor] = dict()
			dic_final_tx2[dir_name][governor] = dict()
			dic_final_bench[dir_name][governor] = dict()
			dic_final_log_roi[dir_name][governor] = dict()

			gointo=DIR+'/'+governor

			pprint(governor)

			roi_runs = {'roitime':[],'logjoules':[], 'logroi':[]}
			for run in os.listdir(gointo):
				
				if os.path.isfile(run): 
					continue

				pprint(run)

				try:
					run_i = int(run.split('_')[1])		
				except ValueError:
					pprint(ValueError.str())

				for filename in os.listdir(gointo + '/'+ run):	
				
					if filename == 'sp_data.csv':
						pprint(filename)
						d_sp = load_sp(gointo + '/' + run + '/' + filename,run_i)
				
						create_time_spaced_array(d_sp)							
						dic_final_sp[dir_name][governor].update(d_sp)							

					if filename == 'sensors.csv':
						pprint(filename)
						d_cpu = load_cpu(gointo + '/' + run + '/' + filename,run_i)
					
						create_time_spaced_array(d_cpu)							
						dic_final_cpu[dir_name][governor].update(d_cpu)							

					if filename == 'power.csv':
						pprint(filename)
						d_tx2 = load_tx2(gointo + '/' + run + '/' + filename,run_i)
						
						create_time_spaced_array(d_tx2,"tx2")
						dic_final_tx2[dir_name][governor].update(d_tx2)

					if "benchmark.csv" in filename:		
						pprint(filename)
						d_benchmark = load_benchmark(gointo + '/' + run + '/' + filename,run_i)
						
						app_name = filename.split('_')[0]

						pprint('app ' + app_name)

						if app_name in dic_final_bench[dir_name][governor].keys():
							dic_final_bench[dir_name][governor][app_name].update(d_benchmark)
						else:
							dic_final_bench[dir_name][governor].update({app_name : d_benchmark})					

					if is_parsec_benchmark(filename):

						pprint(filename)

						d_log = load_log(gointo + '/' + run + '/' + filename,run_i)	

						if not d_log:
							with open('./problems/d_log_problems_'+dir_name+'.txt', 'a') as file:
								line = gointo + '/'+ + run + '/' + filename + '\n'
								file.write(line)
							continue

						if run_i in dic_final_log_roi[dir_name][governor].keys():
							dic_final_log_roi[dir_name][governor][run_i].update(d_log[run_i])
						else:
							dic_final_log_roi[dir_name][governor].update(d_log)						
						
						roi_runs['logjoules'].append(d_log[run_i]['logjoules'])
						roi_runs['logroi'].append(d_log[run_i]['logroi'])

					if filename == 'roitime':
						pprint(filename)

						d_roitime = load_roitime(gointo + '/' + run + '/' + filename,run_i)
						
						if run_i in dic_final_log_roi[dir_name][governor].keys():
							dic_final_log_roi[dir_name][governor][run_i].update(d_roitime[run_i])
						else:
							dic_final_log_roi[dir_name][governor].update(d_roitime)	

						roi_runs['roitime'].append(d_roitime[run_i]['roitime'])

			dic_final_log_roi[dir_name][governor].update({'RoiTime':np.median(roi_runs['roitime'])})
			dic_final_log_roi[dir_name][governor].update({'LogJoules':np.median(roi_runs['logjoules'])})
			dic_final_log_roi[dir_name][governor].update({'LogRoi':np.median(roi_runs['logroi'])})

			calculate_energy_sp(dic_final_sp[dir_name][governor]) if 1 in dic_final_sp[dir_name][governor] else None
			calculate_energy_cpu(dic_final_cpu[dir_name][governor]) if 1 in dic_final_cpu[dir_name][governor] else None
			calculate_energy_tx2(dic_final_tx2[dir_name][governor])  if 1 in dic_final_tx2[dir_name][governor] else None
			calculate_benchmark(dic_final_bench[dir_name][governor]) if app_name else None

		save_tab_governor_sp(dic_final_sp[dir_name],WORK_PATH + 'Analysed_results/' + dir_name + '_sp.csv')		
		save_tab_all_governor_sp(dic_final_sp[dir_name],WORK_PATH + 'Analysed_results/' + dir_name + '_all_sp.csv')	
		save_tab_governor_logroi(dic_final_log_roi[dir_name],WORK_PATH + 'Analysed_results/' + dir_name + '_log_roi_xu3.csv')	      if 1 in dic_final_log_roi[dir_name][governor] else None	
		save_tab_all_governor_logroi(dic_final_log_roi[dir_name],WORK_PATH + 'Analysed_results/' + dir_name + '_log_roi_xu3_all.csv')   if 1 in dic_final_log_roi[dir_name][governor] else None				

#------------------------------------------------------
DATA_PATH = os.environ['HOME'] + '/Projects/hmp_meas_results/'
#analyse_configs_files(DATA_PATH + "powprofiler/", ['node_matrixmul_pareto'],is_two_apps=True)
#analyse_nornir_files(DATA_PATH+'nornir/',['black_nornir_p_spurious_16112021']) #generate the rest of random
#analyse_governor_files(DATA_PATH+'governors/',['mm_1024_governor_tx2_13032021'])



