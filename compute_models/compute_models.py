#! /usr/bin/env python3
from preprocessing import analyse_configs_files
from preprocessing import analyse_governor_files
from preprocessing import analyse_nornir_files
from PerformanceModel import PerformanceModel
from PowerModel import PowerModel
from EnergyModel import EnergyModel
from ParetoFrontier import ParetoFrontier
from BaseModel import ArchType
from BaseModel import NumpyArrayEncoder
from pprint import pprint
#Change in BaseModel.py the variable WORK_PATH  to save fitting and processed results.
from BaseModel import WORK_PATH
from configs_generation import to_frequency_format
import numpy as np
import os
from multiprocessing import freeze_support
from multiprocessing import set_start_method



#Path to raw data
#DATA_PATH = os.environ['HOME'] + '/Projects/hmp_meas_results/'

#process raw data - inputs(folder to the data, list of folders)
#analyse_configs_files(DATA_PATH + "power_measurement/", ['stressng_tutorial','blackscholes_roi_tutorial'])
#analyse_governor_files(DATA_PATH+'governors/',['blackscholes_governors_tutorial'])
#analyse_nornir_files(DATA_PATH+'nornir/',['blackscholes_nornir_pareto_tutorial'])

##------------
#fitting power model with stress-ng
##------------

"""
:param ty: architecture type
:param measured_file: measured data
:param table_file: f/v table information
:param is_core_disable:  disabling core feature (true or false)
:param fitted_x: array of fitted parameters
"""    
#pm = PowerModel(ArchType.XU3_SINGLE,measured_file='stressng_halton_20102020_all_sp.csv')
"""
:param x: array of an initial guess
:param bounds: lower and upper bounds for each parameter 
""" 
#d = pm.fit_model([np.random.rand(),np.random.rand(),np.random.rand(),np.random.rand(),np.random.rand()*4+1],(0,np.inf))

#r = pm.fit_csa(self,initial_guess,csa_configs,y_measured)

#pprint(r)
def main():
	csa_configs = 'csa_power.json'

	error,solution = PowerModel.fit_csa(csa_configs)
	pprint(error)

	parameter = PowerModel.load_json(csa_configs)

	pm = PowerModel(ty=parameter['type'],table_file=parameter['table_file'],is_core_disable=parameter['is_core_disable'],measured_file=parameter['measured_file'])

	g = pm.goodness(pm.residual(solution))
	parameter.update({'goodness':g})
	parameter.update({'solution':solution})

	pprint(parameter)

	pm.save_table(parameter['measured_file'],solution,g,['Power(W)','Est_Power(W)'])
	pm.save_json(parameter,parameter['measured_file'].split(".")[0]+'.json')

if __name__ == '__main__':
	set_start_method("spawn")
	freeze_support()
	main()

"""
:param file_name: 
:param fitted_x: fitted parameters
:param goodness: Array with MSE, RMSE and MAPE
:param column_names:  Column names of the measured and estimated data
""" 
#pm.save_table('stressng_halton_tx2_16032021_tx2.csv',d['fitted_object'].x,d['goodness'],['Power(W)','Est_Power(W)'])
"""
:param dic: output result dictionary of the fit_model
:param file_name: 
""" 
#pm.save_json(d,'stressng_halton_tx2_16032021_tx2.json')


##------------
#fitting performance model
##------------
"""
:param ty: architecture type
:param F: 
:param Tl: 
:param measured_file:  
:param fitted_f:
"""
#pm = PerformanceModel(ArchType.TX2_SINGLE,1113600000,measured_file='mm_1024_halton_tx2_16032021_tx2.csv')
'''
pm = PerformanceModel(ty=ArchType.XU3_SINGLE,F=800000000,measured_file='blackscholes_roi_xu3_29_09_sp.csv')
d = pm.fit_model([0.5,1],([0, 0.1], (1,np.inf)))
pm.save_table('blackscholes_roi_xu3_29_09_sp.csv',d['fitted_object'].x,d['goodness'],['Run_Time(s)','Est_Time(S)'])
pm.save_json(d,'blackscholes_roi_xu3_29_09_sp.json')
'''
#data = PerformanceModel.load_perf_data('blackscholes_roi_xu3_29_09_sp.csv',ty=ArchType.XU3_SINGLE)
'''
csa_configs = 'csa_test.json'

error,solution = PerformanceModel.fit_csa(csa_configs)

pprint(error)

parameter = PerformanceModel.load_json(csa_configs)

pm = PerformanceModel(ty=parameter['type'],F=parameter['F'],measured_file=parameter['measured_file'])

g = pm.goodness(pm.residual(solution))
parameter.update({'goodness':g})
parameter.update({'solution':solution})

pprint(parameter)

pm.save_table(parameter['measured_file'],solution,g,['Run_Time(s)','Est_Time(S)'])
pm.save_json(parameter,parameter['measured_file'].split(".")[0]+'.json')
'''

##------------
#Energy model
##------------

"""
:param perf_json_file: json file to fitted performance model 
:param pow_json_file: json file to fitted power model
"""
#em = EnergyModel.from_json('mm_1024_halton_tx2_16032021_tx2.json','stressng_halton_tx2_16032021_tx2.json')
"""
:param file_name: measured data
:param ty: architecture type
"""
#d_energy = em.load_energy_data('mm_1024_halton_tx2_16032021_tx2.csv',ArchType.TX2_SINGLE)

#goodness = em.calculate_goodness(d_energy)

#em.save_table('mm_1024_halton_tx2_16032021_tx2_ener.csv',None,goodness,['Energy(J)','Est_Energy(J)'])


##------------
#Pareto
##------------

"""
:param energyModel: EnergyModel object
"""
#pf = ParetoFrontier(em)

"""
:param arch: architecture type
:param is_two_apps: Pareto for multiple applications (default false) 
"""
#[all_configs,pareto] = pf.get_pareto_optimal_configs(ArchType.TX2)

"""
:param np_array: array to be saved as csv file 
:param file_name: 
:param is_two_apps: 
"""
#pf.save_csv(all_configs,'mm_1024_halton_tx2_16032021_tx2_allconfigs.csv')
#pf.save_csv(pareto,'mm_1024_halton_tx2_16032021_tx2_pareto.csv')

#to_frequency_format(pareto,'blackscholes_roi_halton_tutorial_pareto')

#ex = pf.load_measured_pareto('blackscholes_roi_pareto_tutorial_sp.csv',ArchType.XU3_SINGLE)
#gov = pf.load_governor('blackscholes_governors_tutorial_sp.csv')

"""
:param all_configs: all_configs array
:param pareto: pareto array
:param onlyfrontier: show only the Pareto Frontier points (Default: False)
:param is_two_apps:   (Default: False)
:param executed:  Array with the measured Pareto points(Default: None)
:param governor:  Array with the measured governors(Default: None)
:param lim_tuples:  set axis limits [(x1,x2),(y1,y2)](Default: None)
:param plot_name_file: (Default: None)
"""  
#pf.show_pareto_frontier(all_configs,pareto,plot_name_file='mm_1024_halton_tx2_1.png')
#pf.show_pareto_frontier(all_configs,pareto,onlyfrontier=True,plot_name_file='mm_1024_halton_tx2_2.png')
#pf.show_pareto_frontier(all_configs,pareto,executed=ex,governor=[g for g in gov if g[0] != 'powersave'],onlyfrontier=True,plot_name_file='blackscholes_tutorial_3.png')




