#! /usr/bin/env python3

from pprint import pprint
from PerformanceModel import PerformanceModel
from PowerModel import PowerModel
from BaseModel import WORK_PATH
from BaseModel import BaseModel
from BaseModel import ArchType
from BaseModel import NumpyArrayEncoder
from copy import deepcopy
import numpy as np
import pandas as pd
import csv
import json

class EnergyModel(BaseModel):
	
    def __init__(self,performance_model,power_model,file_name=None,ty=None,model_ml=None):
        self.perfoModel = performance_model
        self.powerModel = power_model
        self.model_ml = model_ml
        self.ty = ty

        df = self.load_energy_data(file_name,ty)
        self.configs = df['configs']
        self.measured_data = df['data']    

    @classmethod 
    def from_json(cls, perf_json_file, pow_json_file): #is a static method
        return cls(PerformanceModel.from_json(perf_json_file), PowerModel.from_json(pow_json_file))

    def equation(self,x=None):  
        return self.sequential_term() + self.parallel_term()

    def sequential_term(self):   
        return self.perfoModel.sequential_term(self.perfoModel.fitted_x) * self.powerModel.sequential_term(self.powerModel.fitted_x)

    def parallel_term(self):
        return self.perfoModel.parallel_term(self.perfoModel.fitted_x) * self.powerModel.parallel_term(self.powerModel.fitted_x)

    def set_configs(self, configurations):
        if self.perfoModel is not None:
            self.perfoModel.configs = deepcopy(configurations) 
        if self.powerModel is not None:
            self.powerModel.configs = deepcopy(configurations) 
        self.configs = deepcopy(configurations)

    def fit_dict_update(self,x,loss,f_scale,method,bounds,x_scale,verb):
        return {}

    def calculate_goodness(self,d):
        self.set_configs(d['configs'])
        self.measured_data = d['data']  
        y_predict = self.predict()
        gs  =       super().goodness(self.measured_data-y_predict)        
        
        return gs

    @classmethod
    def load_energy_data(self,file_name,ty):

        if ty is None:
            return {'data':[],'configs': []}

        if ty == ArchType.XU3_SINGLE:
            data_index = 6
        else:
            data_index = 8

        self.ty =ty

        return self.load_data(file_name,data_index)

    	