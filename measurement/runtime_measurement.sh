#!/bin/bash

#set -o nounset #to exit when script tries to use undeclared variables.
#set -o errexit #to make script exit when a command fails
#set -o xtrace #to trace what gets executed
#set -o verbose #To produce a trace of every command executed run
usage()
{

	if [ "$1" != "" ]; then
		echo -e "$1"
	fi

	exit 0
}

terminate()
{

	[ -n "$DEBUG" ] && echo "terminating..."

	if [ "$1" != "" ]; then
		echo -e "$1"
	fi
	
	[ "$2" != "hotplug" ] && do_hotplug RESET
	[ "$2" != "change_freq" ] && \
		change_freq "$BIG_MIN_F" "$BIG_MAX_F" "$LITTLE_MIN_F" "$LITTLE_MAX_F"
	
	change_temp_limits RESTORE
	turn_fan OFF

	pkill -9 -c -e "check"
	pkill -9 -c -e "sensors"
	#pkill -9 -c -e "watch_roi"

	local app=
	for app in "${APP_NAME[@]}";do pkill -9 -c -e "$app";done

	[ "$MACHINE" == "tegra-ubuntu" ] && nvpmodel -m "$NVP_M"
	do_emc_gpu 'RESTORE'
	exit 0
}

set_variables()
{
	#get version and platform to update variables
	#Linux tegra-ubuntu 4.4.38-tegra #1 SMP PREEMPT Thu May 17 00:15:19 PDT 2018 aarch64 aarch64 aarch64 GNU/Linux
	# R28 (release), REVISION: 2.1, GCID: 11272647, BOARD: t186ref, EABI: aarch64, DATE: Thu May 17 07:29:06 UTC 2018
	#Linux odroid 3.10.106-154 #1 SMP PREEMPT Tue Feb 5 20:56:46 UTC 2019 armv7l armv7l armv7l GNU/Linux
	#uname -n print the name that the system is known by to a network. It works as long as they have the same name of the system

	[ -n "$DEBUG" ] && echo "setting global variables"

	case "$MACHINE" in
		odroid)

			CLUSTER_B_CORES=(4 5 6 7)
			CLUSTER_L_CORES=(0 1 2 3)
			#Freq in MHz
			#BIG_AVAIL_FREQ=(200 300 400 500 600 700 800 900 1000 1100 1200 1300 1400 1500 1600 1700 1800 1900 2000)
			#LITTLE_AVAIL_FREQ=(200 300 400 500 600 700 800 900 1000 1100 1200 1300 1400)
		
			case "$KERNEL_VERSION" in
				"3.1"*)
					FAN_MODE="/sys/devices/odroid_fan.14/fan_mode"
					FAN_SPEED="/sys/devices/odroid_fan.14/pwm_duty"	

					CLUSTER_BIG=${CLUSTER_B_CORES[0]} 
					CLUSTER_LITTLE=${CLUSTER_L_CORES[0]}					

					THERMAL_ZONES=(0)
					TRIP_POINTS=(0 1 2 3)
					STANDARD_LIMIT_TEMPS=(90000 95000 100000 110000)
					UP_LIMIT_TEMPS=(120000 120000 120000 120000)	

					BIG_AVAIL_FREQ=(200000 300000 400000 500000 600000 700000 800000 900000 1000000 1100000 1200000 1300000 1400000 1500000 1600000 1700000 1800000)
					LITTLE_AVAIL_FREQ=(200000 300000 400000 500000 600000 700000 800000 900000 1000000 1100000 1200000 1300000 1400000)				
				;;
				"4.1"*)
					#LITTLE_AVAIL_FREQ+=( 1500 )

					CLUSTER_BIG=${CLUSTER_B_CORES[0]} 
					CLUSTER_LITTLE=${CLUSTER_L_CORES[0]}

					FAN_MODE="/sys/devices/platform/pwm-fan/hwmon/hwmon0/automatic"
					FAN_SPEED="/sys/devices/platform/pwm-fan/hwmon/hwmon0/pwm1"

					THERMAL_ZONES=(0 1 2 3)
					TRIP_POINTS=(0 1 2 4 5 6)
					STANDARD_LIMIT_TEMPS=(60000 70000 80000 85000 90000 95000)
					UP_LIMIT_TEMPS=(120000 120000 120000 120000 120000 120000)
					BIG_AVAIL_FREQ=( $(cat "/sys/devices/system/cpu/cpu${CLUSTER_BIG}/cpufreq/scaling_available_frequencies") )
					LITTLE_MAX_FVAIL_FREQ=( $(cat "/sys/devices/system/cpu/cpu${CLUSTER_LITTLE}/cpufreq/scaling_available_frequencies") )
				;;
				*)
					echo "Error: kenel version not found"; exit 1
				;;								
				esac
			GOVERNORS_LIST=( $(cat "/sys/devices/system/cpu/cpu${CLUSTER_BIG}/cpufreq/scaling_available_governors") )				
		;;
		#TX2	
		tegra-ubuntu)

			CLUSTER_B_CORES=(1 2)
			CLUSTER_L_CORES=(0 3 4 5)
			
			CLUSTER_BIG=${CLUSTER_B_CORES[0]} 
			CLUSTER_LITTLE=${CLUSTER_L_CORES[0]}
		
			#considering the denver cores as "BIG" and A57 as "LITTLE"
			#BIG_AVAIL_FREQ=(346 499 653 806 960 1110 1270 1420 1570 1730 1880 2040)
			#LITTLE_AVAIL_FREQ=(346 499 653 806 960 1110 1270 1420 1570 1730 1880 2040)
			
			if [ "$KERNEL_VERSION" == "R32" ]; then
				FAN_SPEED="/sys/devices/pwm-fan/target_pwm"
				FAN_MODE="/sys/devices/pwm-fan/temp_control" #
			else
				FAN_SPEED="/sys/kernel/debug/tegra_fan/target_pwm" #There is not a FAN_MODE in TX2
			fi

			NVP_M=$(nvpmodel -q)
			NVP_M=${NVP_M:${#NVP_M}-1:${#NVP_M}} #get the last character which is the model number

			nvpmodel -m 0 &> /dev/null #MAXN is the NONE power model to release all constraints

			BIG_AVAIL_FREQ=( $(cat "/sys/devices/system/cpu/cpu${CLUSTER_BIG}/cpufreq/scaling_available_frequencies") )
			LITTLE_AVAIL_FREQ=( $(cat "/sys/devices/system/cpu/cpu${CLUSTER_LITTLE}/cpufreq/scaling_available_frequencies") )
	
			GOVERNORS_LIST=( $(cat "/sys/devices/system/cpu/cpu${CLUSTER_BIG}/cpufreq/scaling_available_governors") )
			
		;;
		*)
			echo "Error: platform not found"; exit 1
		;;
		esac

	local g
	for i in "${!GOVERNORS_LIST[@]}"; do
		if [ "${GOVERNORS_LIST[i]}" == "powersave" ];then
			local temp
			temp="${GOVERNORS_LIST[i]}"
			GOVERNORS_LIST[i]="${GOVERNORS_LIST[-1]}"
			GOVERNORS_LIST[-1]="$temp"
			break
		fi
	done

	#init global variables
	BIG_MAX_F=${BIG_AVAIL_FREQ[-1]}
	BIG_MIN_F=${BIG_AVAIL_FREQ[0]}
	LITTLE_MAX_F=${LITTLE_AVAIL_FREQ[-1]}
	LITTLE_MIN_F=${LITTLE_AVAIL_FREQ[0]}

	#paths
	IPER_PATH="$(dirname "$(dirname "$(realpath "$0")")")" #it gets the path of the project	
	CHECK_AFF="$IPER_PATH/tools/check_aff.sh"
	CHECK_TEMP="$IPER_PATH/tools/check_temp.sh"
	PROCE_AFFI="$IPER_PATH/tools/process_affinity.sh"
	EXEC_APP_SCRIPT="$IPER_PATH/measurement/exec_app.sh"
	POWER_SENSORS="$IPER_PATH/power_sensor/bin/sensors_tx2_xu3"
	SP_SENSOR="$IPER_PATH/power_sensor/bin/sp_usb"
	WATCH_ROI="$IPER_PATH/measurement/watch_roi.sh"
	NORNIR_PARAM="$IPER_PATH/nornir_parameters/"
	SENSORS_INFO="/tmp/sensors_info"

	SLEEP_SENSORS=500000 #microsseconds
	SLEEP_SP=10000 #microsseconds
	SLEEP_CHECK_=1.5 #secs
	SLEEP_PERF=0.5

	[ -n "$DEBUG" ] && show	
}

do_emc_gpu()
{

	[ "$MACHINE" != "tegra-ubuntu" ] && return


	case "$1" in
		MAX)
			[ -n "$DEBUG" ] && echo "TX2 - EMC MAX FREQ"
			#EMC#
			local EMC_ISO_CAP="/sys/kernel/nvpmodel_emc_cap/emc_iso_cap"
			local EMC_MAX_FREQ="/sys/kernel/debug/bpmp/debug/clk/emc/max_rate"
			local EMC_UPDATE_FREQ="/sys/kernel/debug/bpmp/debug/clk/emc/rate"
			EMC_FREQ_OVERRIDE="/sys/kernel/debug/bpmp/debug/clk/emc/mrq_rate_locked"
			
			emc_foverride=`cat "${EMC_FREQ_OVERRIDE}"`
			
			local emc_cap=`cat "${EMC_ISO_CAP}"`
			local emc_fmax=`cat "${EMC_MAX_FREQ}"`
			if [ "$emc_cap" -gt 0 ] && [ "$emc_cap" -lt  "$emc_fmax" ]; then
				EMC_MAX_FREQ="${EMC_ISO_CAP}"
			fi
			
			echo 1 > "${EMC_FREQ_OVERRIDE}"
			cat "${EMC_MAX_FREQ}" > "${EMC_UPDATE_FREQ}"

			[ -n "$DEBUG" ] && cat "${EMC_UPDATE_FREQ}"
			#---END---#

			if [ -n "$GPU_MODE" ]; then
				[ -n "$DEBUG" ] && echo "TX2 - GPU MAX FREQ"

				GPU_MIN_FREQ="/sys/class/devfreq/17000000.gp10b/min_freq"
				GPU_MAX_FREQ="/sys/class/devfreq/17000000.gp10b/max_freq"
				GPU_RAIL_GATE="/sys/class/devfreq/17000000.gp10b/device/railgate_enable"

				gpu_minf=`cat "${GPU_MIN_FREQ}"`
				gpu_maxf=`cat "${GPU_MAX_FREQ}"`
				gpu_railg=`cat "${GPU_RAIL_GATE}"`

				echo 0 > "${GPU_RAIL_GATE}"
				echo "$gpu_maxf" > "${GPU_MIN_FREQ}"

				ret=$?
				if [ ${ret} -ne 0 ]; then
					echo "Error: Failed to max GPU frequency!"
				fi	
				sleep 1
				[ -n "$DEBUG" ] && cat "${GPU_MIN_FREQ}"			
			fi
		;;
		RESTORE)
			[ -n "$DEBUG" ] && echo "TX2 - EMC RESTORE"
			echo "${emc_foverride}" > "${EMC_FREQ_OVERRIDE}"

			if [ -n "$GPU_MODE" ]; then
				[ -n "$DEBUG" ] && echo "TX2 - GPU RESTORE"

				echo "$gpu_railg" > "${GPU_RAIL_GATE}"
				echo "${gpu_minf}" > "${GPU_MIN_FREQ}"
				ret=$?
				if [ ${ret} -ne 0 ]; then
					echo "Error: Failed to min GPU frequency!"
				fi				
			fi
		;;
		esac


}

set_app_exec_variable()
{
	unset APP_EXEC 
	
	[ -n "$DEBUG" ] && echo "set app exec"

	local i=

	for i in "${!APP_NAME[@]}"; do

		TASKSETS["$i"]="${ARRAY_CORE_RUN[$i]}"
		THREADS["$i"]="${ARRAY_TOTAL_CORES[$i]}"

		ROOTS["$i"]="$(dirname "${APP_PATH[$i]}")"

		APP_EXEC["$i"]=""
		
		#if  ! "${APP_PATH[$i]}" &> /dev/null
		#then 
		#		terminate "Error: ${APP_PATH[$i]} path invalid."
		#fi
		local CSET_TYPE=$1

		case "$CSET_TYPE" in
			shield)
				APP_EXEC["$i"]="cset shield -e "
			;;
			proc)
				APP_EXEC["$i"]="cset proc -s cpuset$i -e "
			;;
		esac

		APP_EXEC["$i"]+="${APP_PATH[$i]}"
		
		if [ "$CSET_TYPE" != "false" ]; then
			APP_EXEC["$i"]+=" -- "
		fi 

		APP_EXEC["$i"]+=" ${INPUTS[$i]}"

	done 

	if [ "$SPURIOUS_PROCESS" = true ]; then
		s_cpu=(2 2 2 4 4 4 4 4 6 6 6)
		s_taskset=("0,1" "0,4" "4,5" "0,1,2,3" "0,1,2,4" "0,1,4,5" "0,4,5,6" "4,5,6,7" "0,1,4,5,6,7" "0,1,2,4,5,6" "0,1,2,3,4,5")
	fi 

	[ -n "$DEBUG" ] && echo "APP_EXEC=${APP_EXEC[*]}" && echo "ROOTS=${ROOTS[*]}" \
	&& echo "TASKSETS=${TASKSETS[*]}" && echo "THREADS=${THREADS[*]}"
}

show()
{
	echo "---Show variables---"

	echo "MACHINE=${MACHINE}" \
		"KERNEL_VERSION=${KERNEL_VERSION}"
	echo ""
	
	echo "BIG_AVAIL_FREQ=${BIG_AVAIL_FREQ[*]}"
	echo "LITTLE_AVAIL_FREQ=${LITTLE_AVAIL_FREQ[*]}"
	
	echo ""
	echo "FAN_MODE=${FAN_MODE}" \
		"FAN_SPEED=${FAN_SPEED}"
	
	echo ""
	
	echo "BIG_MAX_F=${BIG_MAX_F}" \
		"BIG_MIN_F=${BIG_MIN_F}" \
		"LITTLE_MAX_F=${LITTLE_MAX_F}" \
		"LITTLE_MIN_F=${LITTLE_MIN_F}" \
		"CLUSTER_BIG=${CLUSTER_BIG}" \
		"CLUSTER_LITTLE=${CLUSTER_LITTLE}"
	echo "GOVERNORS_LIST=${GOVERNORS_LIST[*]}"

	echo ""

	echo "IPER_PATH=${IPER_PATH}" 
	echo "RESULTS_IPE_PATH=${RESULTS_IPE_PATH}"

	echo "EXEC_APP_SCRIPT=$EXEC_APP_SCRIPT"

	echo ""
	local app
	for app in "${APP_NAME[@]}";do echo "APP_NAME=$app ";done
	

	echo ""
	echo "POWER_SENSORS=$POWER_SENSORS"
	echo "SP_SENSOR=$SP_SENSOR"
	echo "GPU_MODE=$GPU_MODE"

	if [ "$MACHINE" == "tegra-ubuntu" ]; then
		echo "TX2 model Previous $NVP_M"
		echo "Actual model"
		nvpmodel -q
	fi

	echo "---"
}

turn_fan()
{
	echo "Turn fan $1"

	case "$1" in
		ON)
			#Start manual mode
			[ -n "$FAN_MODE" ] && echo 0 > "${FAN_MODE}"
			#Put fan on MAX RPM
			echo 255 > "${FAN_SPEED}"		
			;;
		OFF)
			[ -n "$FAN_MODE" ] && echo 1 > "${FAN_MODE}"
			echo 0 > "${FAN_SPEED}"

			;;
	esac
}

#important to avoid cpu throtling
change_temp_limits()
{

	echo "[$1] Change temperature limits"

	local temp_i=0
	local tz=
	local tp=

	case "$MACHINE" in

		odroid)
			[[ "$KERNEL_VERSION" == *"3.1"* ]] && return

			case "$1" in
				MAX)
					
					for tz in "${THERMAL_ZONES[@]}";do #${!THERMAL_ZONES[@]}; loop over array indices
						for tp in "${TRIP_POINTS[@]}";do
							echo -n "${UP_LIMIT_TEMPS[$temp_i]}" > "/sys/devices/virtual/thermal/thermal_zone$tz/trip_point_${tp}_temp"
							(( temp_i++ ))
						done
					done
				;;
				RESTORE)
					for tz in "${THERMAL_ZONES[@]}";do #${!THERMAL_ZONES[@]}; loop over array indices
						for tp in "${TRIP_POINTS[@]}";do
							echo -n "${STANDARD_LIMIT_TEMPS[$temp_i]}" > "/sys/devices/virtual/thermal/thermal_zone$tz/trip_point_${tp}_temp"
							(( temp_i++ ))
						done
					done
				;;
			esac
			;;
		tegra-ubuntu)
			echo "[TEMP_LIMITS] Not supported to TX2 yet"
		;;			
	esac
}

define_cores_list()
{
   
   CORE_RUN=
   local BIG_CORES=$1
   local LITTLE_CORES=$2   

   echo "BIG LITTLE CORES: $BIG_CORES - $LITTLE_CORES" 

   [ "$BIG_CORES" -gt "${#CLUSTER_B_CORES[@]}" ] || \
   [ "$LITTLE_CORES" -gt "${#CLUSTER_L_CORES[@]}" ] \
   && terminate "Error: Number of cores higher than it is available in a cluster."

   TOTAL_CORES=$(( BIG_CORES+LITTLE_CORES ))

   [ -n "$DEBUG" ] && echo "TOTAL_CORES $TOTAL_CORES"

   local i=
   for ((i=0; i<BIG_CORES; i=i+1))
   do 
      CORE_RUN+="${CLUSTER_B_CORES[$i]}"
      CORE_RUN+=","
   done   

   for ((i=0; i<LITTLE_CORES; i=i+1))
   do 
     
      CORE_RUN+="${CLUSTER_L_CORES[$i]}"
      CORE_RUN+=","
   done   

   CORE_RUN="${CORE_RUN%?}" # removes last character

	ARRAY_CORE_RUN[0]=$CORE_RUN
	ARRAY_TOTAL_CORES[0]=$TOTAL_CORES    

   [ -n "$DEBUG" ] && echo "$BIG_CORES $LITTLE_CORES -> $CORE_RUN"
}

define_core_two_apps()
{

	local cluster_big_temp=("${CLUSTER_B_CORES[@]}")
	local cluster_little_temp=("${CLUSTER_L_CORES[@]}")
	local sum_big=0
	local sum_little=0

	for big_i in "${!LIST_BIG_CORES[@]}";do
		
		local core_run_by_app=""

	   	for ((i=0; i<LIST_BIG_CORES[$big_i]; i=i+1))
	   	do 		
	    	core_run_by_app+="${cluster_big_temp[$i]}"
	    	core_run_by_app+=","
	   	done   

	   	cluster_big_temp=("${cluster_big_temp[@]:${LIST_BIG_CORES[$big_i]}}")

	   	for ((i=0; i<LIST_LITTLE_CORES[$big_i]; i=i+1))
	   	do
	    	core_run_by_app+="${cluster_little_temp[$i]}"
	    	core_run_by_app+=","
		done   

		cluster_little_temp=("${cluster_little_temp[@]:${LIST_LITTLE_CORES[$big_i]}}")

		core_run_by_app="${core_run_by_app%?}"

		ARRAY_CORE_RUN["$big_i"]=$core_run_by_app
		ARRAY_TOTAL_CORES["$big_i"]=$((LIST_BIG_CORES[$big_i]+LIST_LITTLE_CORES[$big_i]))  	
		((sum_big+="${LIST_BIG_CORES[$big_i]}"))
		((sum_little+="${LIST_LITTLE_CORES[$big_i]}"))

	done

 	[ "$sum_big" -gt "${#CLUSTER_B_CORES[@]}" ] || \
   	[ "$sum_little" -gt "${#CLUSTER_L_CORES[@]}" ] \
   	&& terminate "Error: Number of cores higher than it is available in a cluster." "hotplug"	
}

do_hotplug()
{

	echo "Hotplug Action=$1 cores=$2"
	
	local file=
	local cpu_hotplug=
	local online_cores=
	local oc=
	local i=
	local sum_big=0
	local sum_little=0
	local big_i=
	local core_i=

	case $1 in
		SHIELD)
			#First cset then disable any number of cores
			cset shield -c "$2" -k on --force
			[ $? -ne 0 ] &&  terminate "Error: Problem with cset. Is it installed?" "hotplug"
			is_shielded=1
		;;

		SET)
			define_core_two_apps

			for core_i in "${!ARRAY_CORE_RUN[@]}";do 
		    	cset set -c "${ARRAY_CORE_RUN[$core_i]}" -s "cpuset$core_i"
			done

			[ -n "$DEBUG" ] && cset set -l
			#IF ONE DAY WE DECIDE TO KEEP ONE CORE AVALAIBLE TO RUN ALL OTHER PROCESSES/THREADS, REMEMBER TO CREATE A
			#SYSTEM CPUSET TO ISOLATE KTHREADS.
		;;
		HOTPLUG)			
			# This kernel does not disable cores
			[[ "$KERNEL_VERSION" == *"4.1"* ]] && return

			[ "$MACHINE" == "tegra-ubuntu" ] && \
			# echo "Disable CPU Idle" \ # ASCII 1 corresponds to “disabled,” and 0 to “enabled.” 

			for file in /sys/devices/system/cpu/cpu*/cpuidle/state*/disable; do echo 1 > "$file"; done

			case "$MACHINE" in
				odroid)	
					cpu_hotplug=(1 0 0 0 0 0 0 0)
				;;
				tegra-ubuntu)
					cpu_hotplug=(1 0 0 0 0 0)
				;;
			esac

			IFS=', ' read -r -a online_cores <<< "$2"

			[ -n "$DEBUG" ] && echo "online_cores=${online_cores[*]}"

			for oc in "${online_cores[@]}"; do cpu_hotplug["$oc"]=1; done

			[ -n "$DEBUG" ] && echo "cpu_hotplug=${cpu_hotplug[*]}"

			for i in "${!cpu_hotplug[@]}"; do
				
				file="/sys/devices/system/cpu/cpu${i}/online"

				[ -n "$DEBUG" ] && echo "cpu_hotplug[$i]=${cpu_hotplug[$i]}" \
					&& echo "cpu$i/online=$(cat $file)"

				if [ "$(cat "$file")" -ne "${cpu_hotplug[$i]}" ]; then
					echo "${cpu_hotplug[$i]}" > "$file"
				fi
  				
			done
			;;
		RESET)
			#echo "Enabling all cores again"
			for file in /sys/devices/system/cpu/cpu*/online; do 
				[ -n "$DEBUG" ] && echo "[reset Hotplug] file=$file"
				if [ "$(cat "$file")" -eq "0" ]; then
					echo "1" > "$file"
				fi				
			done

			[ "$MACHINE" == "tegra-ubuntu" ] && \
			# echo "Ensable CPU Idle" \ # ASCII 1 corresponds to “disabled,” and 0 to “enabled.” 
			for file in /sys/devices/system/cpu/cpu*/cpuidle/state*/disable; do echo "0" > "$file"; done			

			[ -n "$is_shielded" ] && cset shield --reset --force && return

			for i in "${!ARRAY_CORE_RUN[@]}";do
		    	cset set -d "cpuset$i"
			done
			
		;;
	esac

	[ -n "$DEBUG" ] && echo "Online CPUs: $(cat /sys/devices/system/cpu/online)"
}

change_freq()
{
	echo "[$# variables] Change frequency"
	local g=

	[ -n "$DEBUG" ] && echo "$@"

	if [ $# == 3 ]; then
		cpufreq-set -d "$1" -u "$2" -c "$3"    -g performance &> /dev/null
		[ $? -ne 0 ] &&  \
			terminate "Error: Problem with cpufreq-set. Is it installed?" "change_freq"
	elif [ $# -ge 4 ]; then

		if [ $# == 5 ]; then
			g="$5"
		else
			g="performance"
		fi

		cpufreq-set -d "$1" -u "$2" -c "$CLUSTER_BIG"    -g $g &> /dev/null
		cpufreq-set -d "$3" -u "$4" -c "$CLUSTER_LITTLE" -g $g &> /dev/null
		[ $? -ne 0 ] && \
			terminate "Error: Problem with cpufreq-set. Is it installed?" "change_freq"
	else
		terminate "$# Wrong number of arguments $#" "change_freq"
	fi

	cpufreq-info -o
}

omp_places_string() #$1 is the number of threads
{
	local i=
	local index=
	local core_array=
	local thr=$1

	[ -n "$DEBUG" ] && echo "threads $thr"

	IFS=', ' read -r -a core_array <<< "$CORE_RUN"

	PLACES_STRING="{${core_array[0]}}"

	for ((i=1; i<thr; i=i+1))
	do 
	 index=$((i%TOTAL_CORES))
	 PLACES_STRING+=",{${core_array[$index]}}"
	done

	#export OMP_PROC_BIND=true
	#export OMP_PLACES="$PLACES_STRING"

	#[ -n "$DEBUG" ] && echo "OMP_PLACES=$OMP_PLACES"
}

do_governors_measurement()
{
	turn_fan ON
	change_temp_limits MAX

	local g=
	local i=
	local apps_running=${#APP_NAME[@]}
	local pid_apps=

	#case "$MACHINE" in
	#	odroid)
	#		define_cores_list 4 4
	#		ARRAY_TOTAL_CORES=(8 8)
	#	;;
	#	tegra-ubuntu)
	#		define_cores_list 2 4
	#		ARRAY_TOTAL_CORES=(6 6)
	#	;;
	#esac

	echo "Apps to run #$apps_running app(s)"

    local list_big_cores
    local list_little_cores

    list_big_cores=$(head -n1 "$CONFIG_LIST")
    list_little_cores=$(tail -n1 "$CONFIG_LIST")

	IFS=', ' read -r -a big_cores_array <<< "${list_big_cores}"
	IFS=', ' read -r -a little_cores_array <<< "${list_little_cores}"	

	[ -n "$DEBUG" ] && echo "list_big_cores=$list_big_cores"
	[ -n "$DEBUG" ] && echo "list_little_cores=$list_little_cores"
	[ -n "$DEBUG" ] && echo "big_cores_array=${big_cores_array[*]}"
	[ -n "$DEBUG" ] && echo "little_cores_array=${little_cores_array[*]}"

	do_emc_gpu 'MAX'

    for g in  "${GOVERNORS_LIST[@]}"
    do
    	change_freq "$BIG_MIN_F" "$BIG_MAX_F" "$LITTLE_MIN_F" "$LITTLE_MAX_F" "$g" 
		sleep 1

		for core_index in "${!big_cores_array[@]}";
		do	
			BIG_SELECT=${big_cores_array[core_index]}
			LITTLE_SELECT=${little_cores_array[core_index]}

			echo "LITTLE_SELECT $LITTLE_SELECT BIG_SELECT $BIG_SELECT"

			define_cores_list "${BIG_SELECT}" "${LITTLE_SELECT}"
			do_hotplug SHIELD "$CORE_RUN"

			set_app_exec_variable "shield"

		    for i in $(seq 1 "$NUM_RUNS")
		    do
		    	echo "run $i/$NUM_RUNS"

		    	result_path_n="$RESULTS_IPE_PATH/governors/$RESULT_FOLDER/${g}_${BIG_SELECT}_${LITTLE_SELECT}/Run_$i"
				mkdir -v -p "$result_path_n"

				echo ""	
				
				[ -n "$DEBUG" ] && echo "starting checkings"
				$CHECK_TEMP "$MACHINE" "$KERNEL_VERSION" "$SLEEP_CHECK_" &> "$result_path_n/temp.csv" &
				pid_temp=$!
				$CHECK_AFF "${APP_NAME[*]}" "$SLEEP_CHECK_" &> "$result_path_n/aff.csv" &
				pid_aff=$! 	


				if [ "$ROI_MEASUREMENT" = true ]; then
					#[ -f "/tmp/app.status" ] && rm "/tmp/app.status"
						[ -n "$DEBUG" ] && echo "Saving on file the information"

						info="1\n"
						[ "$MACHINE" = "odroid" ] && info="2\n"
						
						info+="$POWER_SENSORS $SLEEP_SENSORS $MACHINE "
						
						if [ "$MACHINE" = "odroid" ];then
							info+="$result_path_n/sensors.csv\n"
							info+="$SP_SENSOR $SLEEP_SP $result_path_n/sp_data.csv\n"
						else
							info+="$result_path_n/power.csv\n"
						fi

						info+="$result_path_n/roitime\n"

						echo -e "$info" > "$SENSORS_INFO"

					#$WATCH_ROI "$POWER_SENSORS" "$SP_SENSOR"  "$SLEEP_SENSORS" "$SLEEP_SP" "$MACHINE" "$result_path_n" "$APP_NAME"&
				else
					[ -n "$DEBUG" ] && echo "Start power sensors"

					local power_output="$result_path_n/power.csv"

					[ "$MACHINE" = "odroid" ] &&  power_output="$result_path_n/sensors.csv"

					$POWER_SENSORS "$SLEEP_SENSORS"  "$MACHINE" "$power_output"&
					pid_sensor=$!

					if [[ "$MACHINE" = "odroid" ]];then
						$SP_SENSOR "$SLEEP_SP" "$result_path_n/sp_data.csv"& #it does not mater the amount of sleep time
						pid_spusb=$!
					fi
				fi 

				#For now, spurious only run on odroid platform
				#number of runs should be 11
				if [ "$SPURIOUS_PROCESS" = true ]; then
					local local_taskset
					local_taskset=${s_taskset[$((i-1))]}

					[ -n "$DEBUG" ] && echo "RUN SPURIOUS PROCESS METHOD WITH ${s_cpu[$((i-1))]} CPUs AND TASKSET $local_taskset"
					/home/demetrios/Projects/stress-ng/stress-ng --cpu "${s_cpu[$((i-1))]}" --cpu-method callfunc --taskset "$local_taskset"&
					
					sleep 0.5 #wait for creating process
					
					IFS=$'\n' read -r -d '' -a array_stress_pid <<< "$(pgrep stress-ng-cpu)"
					IFS=', ' read -r -a array_s_taskset <<< "$local_taskset"	

					if [ -n "${array_stress_pid[*]}" ]; then
						local count=0
						for l_core in "${array_s_taskset[@]}";do
							taskset -p -c "$l_core" "${array_stress_pid[$count]}"
							count=$((count+1))
						done

					else
						terminate "Error: Cant find stressng pid "
					fi
				fi 				

				for app_i in "${!APP_NAME[@]}";do
					source "$EXEC_APP_SCRIPT" "${APP_EXEC[$app_i]}" "${APP_NAME[$app_i]}" "$result_path_n" "${TASKSETS[*]}" "${THREADS[*]}" "${ROOTS[*]}" "$PLACES_STRING"&
					pid_apps[$app_i]=$!
					[ -n "$DEBUG" ] && echo "${APP_NAME[$app_i]} - pid: ${pid_apps[$app_i]}"
				done 

				# wait for all pids
				[ -n "$DEBUG" ] && date && echo " - Waiting apps"
				for pid in ${pid_apps[*]}; do
				    wait $pid
				done

				if [ "$SPURIOUS_PROCESS" = true ]; then
					[ -n "$DEBUG" ] && echo "killing spurious"
					pkill -9 -c -e stress
				fi 						

				[ -n "$DEBUG" ] && echo "kiling checkers"
				kill -9 $pid_temp #&> /dev/null
				#disown  &> /dev/null  
				
				kill -9 $pid_aff #&> /dev/null
				#disown &> /dev/null

				[ -n "$pid_sensor" ] && kill -9  $pid_sensor #&> /dev/null
				[ -n "$pid_spusb" ] && kill -9  $pid_spusb #&> /dev/null
				#disown  &> /dev/null  
				
				echo ""
		    done
			do_hotplug RESET
			sleep 1	    
		done
	done

	do_emc_gpu 'RESTORE'
	change_freq "$BIG_MIN_F" "$BIG_MAX_F" "$LITTLE_MIN_F" "$LITTLE_MAX_F"
	change_temp_limits RESTORE
	turn_fan OFF
}

do_perf_measurement()
{
	change_freq "$BIG_MAX_F" "$BIG_MAX_F" "$LITTLE_MAX_F" "$LITTLE_MAX_F"

	turn_fan ON
	change_temp_limits MAX

	local cluster=
	local freq=
	local i=
	local freq_array=
	local result_path_n=
	local pid_temp=
	local pid_aff=
	local pid_apps=
	local apps_running=${#APP_NAME[@]}

	echo "Apps to run #$apps_running app(s)"

	set_app_exec_variable "shield"

	for cluster in $CLUSTER_BIG $CLUSTER_LITTLE 
	do
		do_hotplug SHIELD "$cluster"
		
		if [ "$cluster" = "$CLUSTER_BIG" ]
		then
			freq_array=("${BIG_AVAIL_FREQ[@]}")
		else
			freq_array=("${LITTLE_AVAIL_FREQ[@]}")
		fi;

		[ -n "$DEBUG" ] && echo "freq_array=${freq_array[*]}"

		for freq in "${freq_array[@]}"
		do
			change_freq "$freq" "$freq" "$cluster" 

			for i in $(seq 1 "$NUM_RUNS")
			do

				echo "$i/$NUM_RUNS"

				result_path_n="$RESULTS_IPE_PATH/perf_fitting/$RESULT_FOLDER/${APP_NAME[0]}_${cluster}/$freq/Run_${i}"
				mkdir -v -p "$result_path_n"

				[ -n "$DEBUG" ] && echo "starting checkings"
				$CHECK_TEMP "$MACHINE" "$KERNEL_VERSION" "$SLEEP_CHECK_" &> "$result_path_n/temp.csv" &
				pid_temp=$!
			
				$CHECK_AFF "${APP_NAME[*]}" "$SLEEP_CHECK_" &> "$result_path_n/aff.csv" & #
				pid_aff=$! 				
				
				source "$EXEC_APP_SCRIPT" "${APP_EXEC[0]}" "${APP_NAME[0]}" "$result_path_n" "${TASKSETS[*]}" "${THREADS[*]}" "${ROOTS[*]}" "$result_path_n/roitime" "$PLACES_STRING"&
				pid_apps[0]=$!
				[ -n "$DEBUG" ] && echo "${APP_NAME[0]} - pid: ${pid_apps[0]}"
			
		        #
				while true;
				do
					if [[ -f "/tmp/app.status" ]] && [[ $(cat "/tmp/app.status") = "ROI.Start" ]];
					then

						echo "Entering ROI"

						#echo "Changing frequency to $FREQ in core $CLUSTER"

						#cpufreq-set -d $FREQ -u $FREQ -c $CLUSTER
						#cpufreq-info -o

						echo "Sleeping for $PERF_SECS seconds"
						sleep $PERF_SECS
						
						echo "Killing app"
		        		pkill -9 -c -e  "${APP_NAME[0]}" 
		        		
		        		sleep 1

				   		echo "Moving results to folder $result_path_n"
				   		mv -v "/tmp/iterations" "$result_path_n"

				   		#echo "Removing /tmp/roi"
				   		#rm -f "/tmp/roi"

				   		break
					fi;

					sleep $SLEEP_PERF

				done

				[ -n "$DEBUG" ] && echo "kiling checkers"
				kill -9 $pid_temp #&> /dev/null
				#disown  &> /dev/null  
				
				kill -9 $pid_aff #&> /dev/null
				#disown &> /dev/null
		   	done		
		done
	done

	do_hotplug RESET

	change_freq "$BIG_MIN_F" "$BIG_MAX_F" "$LITTLE_MIN_F" "$LITTLE_MAX_F"
	change_temp_limits RESTORE
	turn_fan OFF	

	[ -n "$DEBUG" ] && date && echo "-End-"

	[ "$MACHINE" == "tegra-ubuntu" ] && nvpmodel -m "$NVP_M"
	do_emc_gpu 'RESTORE'

}

create_result_path(){

	local i=

	result_path_n="$RESULTS_IPE_PATH/$RESULT_FOLDER/"

	for i in "${!APP_NAME[@]}";do
		result_path_n+="${APP_NAME["$i"]}"
		result_path_n+="_"
	done

	#result_path_n="${result_path_n%?}" # removes last character
	#result_path_n+="/"

	for i in "${!LIST_BIG_CORES[@]}";do
		result_path_n+="${LIST_BIG_CORES["$i"]}"
		result_path_n+="_"
		result_path_n+="${LIST_LITTLE_CORES["$i"]}"
		result_path_n+="_"
	done

	result_path_n="${result_path_n%?}" # removes last character
	result_path_n+="/${BIG_SELECT}_${LITTLE_SELECT}/Run_$1"

	mkdir -v -p "$result_path_n"
}

get_data(){

	local apps_running=${#APP_NAME[@]}
	local big_freq_array=
	local little_freq_array=
	local freq_index=
	local i=
	local pid_temp=
	local pid_aff=
	local app_i=
	local pid=
	local pid_apps=
	local pid_sensor=

	echo "Apps to run #$apps_running app(s)"
	
	#it can be false, shield or proc
	local cset_type="false"

	if [ "$apps_running"  -eq "1" ];then

		define_cores_list "${LIST_BIG_CORES[0]}" "${LIST_LITTLE_CORES[0]}"
		
		do_hotplug SHIELD  "$CORE_RUN"
		do_hotplug HOTPLUG "$CORE_RUN"

		cset_type="shield"
		if [ "$ROI_MEASUREMENT" = true ];then #for now, it is used for nornir
			
			local total_threads=6
			[ "$MACHINE" = "odroid" ] && total_threads=8

			omp_places_string "$total_threads" # it always has to run after 'define_core..' function
		else
			omp_places_string "$TOTAL_CORES" # #threads=cores in case whole measurement is set
		fi
	else  
		do_hotplug SET
		cset_type="proc"
	fi

	set_app_exec_variable "$cset_type"

    #Frequency of all cores inside a  cluster is the same.
	IFS=', ' read -r -a big_freq_array <<< "${BIG_FREQS[0]}"
	IFS=', ' read -r -a little_freq_array <<< "${LITTLE_FREQS[0]}"

	echo "BIG_FREQS ${BIG_FREQS[*]}"
	echo "LITTLE_FREQS ${LITTLE_FREQS[*]}"

	for freq_index in "${!big_freq_array[@]}";
	do
		LITTLE_SELECT=${little_freq_array[freq_index]}
		BIG_SELECT=${big_freq_array[freq_index]}

		echo "LITTLE_SELECT $LITTLE_SELECT BIG_SELECT $BIG_SELECT"
		#Frequencies of appB is the same of appA
		change_freq "$BIG_SELECT" "$BIG_SELECT" "$LITTLE_SELECT" "$LITTLE_SELECT"

		sleep 1 # test if this will make difference later
		
		for i in $(seq 1 "$NUM_RUNS");
		do	
			echo "run $i/$NUM_RUNS"
			
			create_result_path "$i"	

			echo ""	
    		
			[ -n "$DEBUG" ] && echo "starting checkings"
			$CHECK_TEMP "$MACHINE" "$KERNEL_VERSION" "$SLEEP_CHECK_" &> "$result_path_n/temp.csv" &
			pid_temp=$!
			
			$CHECK_AFF "${APP_NAME[*]}" "$SLEEP_CHECK_" &> "$result_path_n/aff.csv" & #
			pid_aff=$! 				

			if [ "$ROI_MEASUREMENT" = true ]; then
				#[ -f "/tmp/app.status" ] && rm "/tmp/app.status"
				[ -n "$DEBUG" ] && echo "Saving on file the information"

				info="1\n"
				[ "$MACHINE" = "odroid" ] && info="2\n"
				
				info+="$POWER_SENSORS $SLEEP_SENSORS $MACHINE "
				
				if [ "$MACHINE" = "odroid" ];then
					info+="$result_path_n/sensors.csv\n"
					info+="$SP_SENSOR $SLEEP_SP $result_path_n/sp_data.csv\n"
				else
					info+="$result_path_n/power.csv\n"
				fi

				info+="$result_path_n/roitime\n"

				echo -e "$info" > "$SENSORS_INFO"

				#$WATCH_ROI "$POWER_SENSORS" "$SP_SENSOR"  "$SLEEP_SENSORS" "$SLEEP_SP" "$MACHINE" "$result_path_n" "$APP_NAME"&
			else
				[ -n "$DEBUG" ] && echo "Start power sensors"

				local power_output="$result_path_n/power.csv"

				[ "$MACHINE" = "odroid" ] &&  power_output="$result_path_n/sensors.csv"

				$POWER_SENSORS "$SLEEP_SENSORS"  "$MACHINE" "$power_output"&
				pid_sensor=$!

				if [[ "$MACHINE" = "odroid" ]];then
					$SP_SENSOR "$SLEEP_SP" "$result_path_n/sp_data.csv"& #it does not mater the amount of sleep time
					pid_spusb=$!
				fi
			fi 

    		
    		#if [ "$apps_running"  -eq "1" ];then
    			[ "$TASKSET_AFFINITY" = true ] && [ -n "$DEBUG" ] && echo "process_affinity"
    			[ "$TASKSET_AFFINITY" = true ] && $PROCE_AFFI "$CORE_RUN" "$APP_NAME" "$SLEEP_PERF" "$IS_PTHREADS" "$DEBUG"& #&> /dev/null &  
    		#else
    			#for app_i in "${!APP_NAME[@]}";do
    			#	$PROCE_AFFI "${ARRAY_CORE_RUN[$app_i]}" "${APP_NAME[$app_i]}" 0.3 &> "$result_path_n/${APP_NAME[$app_i]}_process_aff.log" & 
    			#done
    		#fi
    		[ -n "$DEBUG" ] && [ "$ROI_MEASUREMENT" = true ] && echo "$SENSORS_INFO" && cat "$SENSORS_INFO"
			for app_i in "${!APP_NAME[@]}";do
				source "$EXEC_APP_SCRIPT" "${APP_EXEC[$app_i]}" "${APP_NAME[$app_i]}" "$result_path_n" "${TASKSETS[*]}" "${THREADS[*]}" "${ROOTS[*]}" "$PLACES_STRING"&
				pid_apps[$app_i]=$!
				[ -n "$DEBUG" ] && echo "${APP_NAME[$app_i]} - pid: ${pid_apps[$app_i]}"
			done 		
			
			# wait for all pids
			[ -n "$DEBUG" ] && date && echo " - Waiting apps"
			for pid in ${pid_apps[*]}; do
			    wait $pid
			done

			[ -n "$DEBUG" ] && echo "kiling checkers"
			kill -9  $pid_temp #&> /dev/null
			#disown  &> /dev/null  
			
			kill -9 $pid_aff #&> /dev/null
			#disown &> /dev/null

			[ -n "$pid_sensor" ] && kill -9   $pid_sensor #&> /dev/null
			[ -n "$pid_spusb" ] && kill -9   $pid_spusb #&> /dev/null
			#disown  &> /dev/null  
			
			echo ""

		done
	done

	do_hotplug RESET
}

do_configs_list_measurement(){

	[ -n "$DEBUG" ] && date

	turn_fan ON
	change_temp_limits MAX
	do_emc_gpu 'MAX'

	local line=

	while read -r line
	do

		if [ "$TWO_APPS" = true ];then
		    LIST_BIG_CORES[0]=$line
		    read -r line
			LIST_LITTLE_CORES[0]=$line
			read -r line
			LIST_BIG_CORES[1]=$line
			read -r line
			LIST_LITTLE_CORES[1]=$line
			read -r line
			BIG_FREQS[0]=$line			
			read -r line
			LITTLE_FREQS[0]=$line
			read -r line
			BIG_FREQS[1]=$line			
			read -r line
			LITTLE_FREQS[1]=$line				
		else
			echo ""
		    LIST_BIG_CORES[0]=$line
		    read -r line
			LIST_LITTLE_CORES[0]=$line
			read -r line
			BIG_FREQS[0]=$line
			read -r line
			LITTLE_FREQS[0]=$line
		fi

		get_data

	done < "$CONFIG_LIST"

	change_freq "$BIG_MIN_F" "$BIG_MAX_F" "$LITTLE_MIN_F" "$LITTLE_MAX_F"
	change_temp_limits RESTORE
	do_emc_gpu 'RESTORE'

	turn_fan OFF	

	[ "$MACHINE" == "tegra-ubuntu" ] && nvpmodel -m "$NVP_M"
	
	[ -n "$DEBUG" ] && date && echo "-End-"

}

do_nornir(){

	local pid_apps

	[ -n "$DEBUG" ] && date

	turn_fan ON
	change_temp_limits MAX

	local parameter_xml=

	local apps_running=${#APP_NAME[@]}

	case "$MACHINE" in
		odroid)
			define_cores_list 4 4
			ARRAY_TOTAL_CORES=(8 8)
		;;
		tegra-ubuntu)
			define_cores_list 2 4
			ARRAY_TOTAL_CORES=(6 6)
		;;
	esac

	echo "Apps to run #$apps_running app(s)"

	set_app_exec_variable "false"	

	do_emc_gpu 'MAX'

	change_freq "$BIG_MIN_F" "$BIG_MAX_F" "$LITTLE_MIN_F" "$LITTLE_MAX_F" 

	while read -r parameter_xml
	do
		[ -n "$DEBUG" ] && echo "copying $parameter_xml to p3arsec"
		[ -n "$DEBUG" ] && cat "$NORNIR_PARAM/$parameter_xml"
		
		cp "$NORNIR_PARAM/$parameter_xml" "$IPER_PATH/../p3arsec/parameters.xml"
		
		paramenter_name="${parameter_xml%.*}"

		sleep 1

		for i in $(seq 1 "$NUM_RUNS")
		do
			echo "run $i/$NUM_RUNS"

			result_path_n="$RESULTS_IPE_PATH/nornir/$RESULT_FOLDER/$paramenter_name/Run_$i"
			mkdir -v -p "$result_path_n"

			echo ""	

			[ -n "$DEBUG" ] && echo "starting checkings"
			$CHECK_TEMP "$MACHINE" "$KERNEL_VERSION" "$SLEEP_CHECK_" &> "$result_path_n/temp.csv" &
			pid_temp=$!
			$CHECK_AFF "${APP_NAME[*]}" "$SLEEP_CHECK_" &> "$result_path_n/aff.csv" &
			pid_aff=$!

			if [ "$ROI_MEASUREMENT" = true ]; then
				#[ -f "/tmp/app.status" ] && rm "/tmp/app.status"
				[ -n "$DEBUG" ] && echo "Saving on file the information"

				info="1\n"
				[ "$MACHINE" = "odroid" ] && info="2\n"
				
				info+="$POWER_SENSORS $SLEEP_SENSORS $MACHINE "
				
				if [ "$MACHINE" = "odroid" ];then
					info+="$result_path_n/sensors.csv\n"
					info+="$SP_SENSOR $SLEEP_SP $result_path_n/sp_data.csv\n"
				else
					info+="$result_path_n/power.csv\n"
				fi

				info+="$result_path_n/roitime\n"

				echo -e "$info" > "$SENSORS_INFO"

				#$WATCH_ROI "$POWER_SENSORS" "$SP_SENSOR"  "$SLEEP_SENSORS" "$SLEEP_SP" "$MACHINE" "$result_path_n" "$APP_NAME"&
			else
				[ -n "$DEBUG" ] && echo "Start power sensors"

				local power_output="$result_path_n/power.csv"

				[ "$MACHINE" = "odroid" ] &&  power_output="$result_path_n/sensors.csv"

				$POWER_SENSORS "$SLEEP_SENSORS"  "$MACHINE" "$power_output"&
				pid_sensor=$!

				if [[ "$MACHINE" = "odroid" ]];then
					$SP_SENSOR "$SLEEP_SP" "$result_path_n/sp_data.csv"& #it does not mater the amount of sleep time
					pid_spusb=$!
				fi
			fi 
			#For now, spurious only run on odroid platform
			#number of runs should be 11
			if [ "$SPURIOUS_PROCESS" = true ]; then
				local local_taskset
				local_taskset=${s_taskset[$((i-1))]}

				[ -n "$DEBUG" ] && echo "RUN SPURIOUS PROCESS METHOD WITH ${s_cpu[$((i-1))]} CPUs AND TASKSET $local_taskset"
				/home/demetrios/Projects/stress-ng/stress-ng --cpu "${s_cpu[$((i-1))]}" --cpu-method callfunc --taskset "$local_taskset"&
				
				sleep 0.5 #wait for creating process
				
				IFS=$'\n' read -r -d '' -a array_stress_pid <<< "$(pgrep stress-ng-cpu)"
				IFS=', ' read -r -a array_s_taskset <<< "$local_taskset"	

				if [ -n "${array_stress_pid[*]}" ]; then
					local count=0
					for l_core in "${array_s_taskset[@]}";do
						taskset -p -c "$l_core" "${array_stress_pid[$count]}"
						count=$((count+1))
					done

				else
					terminate "Error: Cant find stressng pid "
				fi
			fi 

			[ -n "$DEBUG" ] && [ "$ROI_MEASUREMENT" = true ] && echo "$SENSORS_INFO" && cat "$SENSORS_INFO"
			for app_i in "${!APP_NAME[@]}";do
				source "$EXEC_APP_SCRIPT" "${APP_EXEC[$app_i]}" "${APP_NAME[$app_i]}" "$result_path_n" "${TASKSETS[*]}" "${THREADS[*]}" "${ROOTS[*]}"&
				pid_apps[$app_i]=$!
				[ -n "$DEBUG" ] && echo "${APP_NAME[$app_i]} - pid: ${pid_apps[$app_i]}"
			done 

			# wait for all pids
			[ -n "$DEBUG" ] && date && echo " - Waiting apps"
			for pid in ${pid_apps[*]}; do
			    wait $pid
			done

			if [ "$SPURIOUS_PROCESS" = true ]; then
				[ -n "$DEBUG" ] && echo "killing spurious"
				pkill -9 -c -e stress
			fi 			

			[ -n "$DEBUG" ] && echo "kiling checkers"
			kill -9  $pid_temp #&> /dev/null
			#disown  &> /dev/null  

			kill -9 $pid_aff #&> /dev/null
			#disown &> /dev/null

			[ -n "$pid_sensor" ] && kill -9 $pid_sensor #&> /dev/null
			[ -n "$pid_spusb" ] && kill -9 $pid_spusb #&> /dev/null
			#disown  &> /dev/null  

			echo "Moving nornir results..."
		   	#mv -v "benchmark.out" "$result_path_n"
		   	mv -v "calibration.csv" "$result_path_n"
		   	mv -v "stats.csv" "$result_path_n"
		   	mv -v "summary.csv" "$result_path_n"

		   	sleep 1 #testing if solve the bodytrack "address already used" issue
			echo ""
		done
	done < "$PARAMENTER_NORNIR_LIST"

	change_freq "$BIG_MIN_F" "$BIG_MAX_F" "$LITTLE_MIN_F" "$LITTLE_MAX_F"
	change_temp_limits RESTORE
	turn_fan OFF	

	[ -n "$DEBUG" ] && date && echo "-End-"

	[ "$MACHINE" == "tegra-ubuntu" ] && nvpmodel -m "$NVP_M"
	do_emc_gpu 'RESTORE'

}

sanity_test(){ 

	echo "**Sanity test**"

	change_freq "$BIG_MAX_F" "$BIG_MAX_F" "$LITTLE_MAX_F" "$LITTLE_MAX_F"

	turn_fan ON
	change_temp_limits MAX

	local cores=
	local result_path_n=
	local pid_temp=
	local pid_aff=
	local pid_sensor=

	case "$MACHINE" in

		odroid)
			cores="0,1,2,3,4,5,6,7"
			TOTAL_CORES="8"
			define_cores_list 4 4
		;;
		tegra-ubuntu)
			cores="0,1,2,3,4,5" 
			define_cores_list 2 4
		;;
	esac

	do_hotplug SHIELD  "$CORE_RUN"
	do_hotplug HOTPLUG "$CORE_RUN"

	set_app_exec_variable "$CORE_RUN" "$TOTAL_CORES" true 

	omp_places_string "${THREADS[0]}"

	result_path_n="$RESULTS_IPE_PATH/sanity/$(date '+%d-%m-%Y')"
	mkdir -v -p "$result_path_n"

    [ -n "$DEBUG" ] && echo "starting checkings"
    $CHECK_TEMP "$MACHINE" "$KERNEL_VERSION" "$SLEEP_CHECK_" &> "$result_path_n/temp.csv" &
    pid_temp=$!
    $CHECK_AFF "$APP_NAME" "$SLEEP_CHECK_" &> "$result_path_n/aff.csv" &
    pid_aff=$!

	[ -n "$DEBUG" ] && echo "Start power sensors"
	local power_output="$result_path_n/power.csv"

	[ "$MACHINE" = "odroid" ] &&  power_output="$result_path_n/sensors.csv"

	$POWER_SENSORS "$SLEEP_SENSORS"  "$MACHINE" "$power_output"&
	pid_sensor=$! 

    [ -n "$DEBUG" ] && [ "$TASKSET_AFFINITY" = true ] && echo "process_affinity"
    [ "$TASKSET_AFFINITY" = true ] && $PROCE_AFFI "$cores" "$APP_NAME" "$SLEEP_PERF" "$IS_PTHREADS" "$DEBUG"& # &> /dev/null &  

	echo "Running $APP_NAME"
	t1=$(date +'%s%N')
	$APP_EXEC &> "$result_path_n/${APP_NAME}.log"
    t2=$(date +'%s%N')
    echo "End $APP_NAME"

	[ -n "$DEBUG" ] && echo "kiling checkers"
	kill -9 $pid_temp #&> /dev/null
	#disown   &> /dev/null 

	kill -9 $pid_aff #&> /dev/null
	#disown  &> /dev/null  

	kill -9 $pid_sensor #&> /dev/null
	#disown  &> /dev/null  	
    
    echo -e "Time\t$t1\t$t2" | tee "$result_path_n/benchmark.csv"		

	do_hotplug RESET
	change_freq "$BIG_MIN_F" "$BIG_MAX_F" "$LITTLE_MIN_F" "$LITTLE_MAX_F"
	change_temp_limits RESTORE
	turn_fan OFF

	[ "$MACHINE" == "tegra-ubuntu" ] && nvpmodel -m "$NVP_M"
	do_emc_gpu 'RESTORE'
	echo "***End Sanity***"
}

parse_json()
{
	echo "parse_json $PARAMETERS_JSON"
		
	local parameters_list=
	local p=
	local value=
	local indices=
	local i=
	local keys=
	local k=

	parameters_list=($(jq '.parameters| keys' $PARAMETERS_JSON | tr -d '[],"'))

	for p in "${parameters_list[@]}"; do
	   	value=$(jq ".parameters.$p" $PARAMETERS_JSON | tr -d '"')

		case "$p" in
	        taskset_affinity)
				[ "$value" = true ] && TASKSET_AFFINITY="$value"
		   		;;
		   	is_pthreads)
				[ "$value" = true ] && IS_PTHREADS="$value"
		   		;;
		   	governors_measurement)
				[ "$value" = true ] && ACTION="GOVERNORS_MEASUREMENT"
		   		;;
		   	perf_paramenter)
				[ "$value" = true ] && ACTION="PERF_MEASUREMENT"
		   		;;
		   	power_measurement)
				[ "$value" = true ] && ACTION="POWER_MEASUREMENT"
		   	;;
		   	roi_measurement)
				[ "$value" = true ] && ROI_MEASUREMENT="$value"
		   	;;
		   	sanity)
				[ "$value" = true ] && ACTION="SANITY_TEST"
		   	;;
		   	nornir)
				[ "$value" = true ] && ACTION="NORNIR_MEASURUMENT"	
			;;	   	
		   	debug)
				[ "$value" = true ] && DEBUG="$value"
		   	;;
		   	runs)
				NUM_RUNS="$value"
		   	;;
		   	save_results)
				RESULT_FOLDER="$value"
		   	;;	   
		   	configs_list)
				CONFIG_LIST="$value"
		   	;;	   
		   	parameters_nornir_list)
				PARAMENTER_NORNIR_LIST="$value"
		   	;;	 		   	
		   	two_apps)
				TWO_APPS="$value"
		   	;;
		   	perf_secs)
				PERF_SECS="$value"
			;;
		   	gpu_mode)
				[ "$value" = true ] && GPU_MODE="$value"
			;;		
		   	spurious_process)
				[ "$value" = true ] && SPURIOUS_PROCESS="$value"
			;;				
		    *)	 
				echo "Unknown option: $p"
				exit 1
				;;
		esac
	done
	[ -n "$DEBUG" ] && echo "ACTION: $ACTION - IS_PTHREADS: $IS_PTHREADS -TASKSET_AFFINITY: $TASKSET_AFFINITY - ROI_MEASUREMENT: $ROI_MEASUREMENT - DEBUG: $DEBUG - NUM_RUNS: $NUM_RUNS - RESULT_FOLDER: $RESULT_FOLDER - PARAMENTER_NORNIR_LIST: $PARAMENTER_NORNIR_LIST - CONFIG_LIST: $CONFIG_LIST - TWO_APPS: $TWO_APPS - PERF_SECS: $PERF_SECS"

	[ -n "$DEBUG" ] && echo ""

	indices=($(jq '.target_apps | keys' $PARAMETERS_JSON | tr -d '[],"' ))

	for i in ${indices[*]};do

	    keys=($(jq ".target_apps[$i] | keys" $PARAMETERS_JSON| tr -d '[],"'))
	  
	    for k in ${keys[*]};do
	    	value=$(jq ".target_apps[$i].$k" $PARAMETERS_JSON | tr -d '"')
	        case "$k" in
	        	path)
	            	APP_PATH["$i"]="$value"            
	            	;;
				name)
	            	APP_NAME["$i"]="$value"
	            	;;
				inputs)
	            	INPUTS["$i"]="$value"
	            	;;
	        	*)	 
		  			echo "Unknown option: $k"
		  			exit 1
					;;
	        esac
	  	done
	done

	[ "${#indices[@]}" -gt 1 ] && TWO_APPS=true

	[ -n "$DEBUG" ] && echo "APP_NAME: ${APP_NAME[*]} - APP_PATH: ${APP_PATH[*]}" && echo "INPUTS: ${INPUTS[*]}"
}

main ()
{

	if [ $# == 0 ]; then
		usage "waiting arguments"
		exit 0
	fi

	while [ -n "$1" ]; do
		case "$1" in
			#-g|--generic)
			#	GENERIC_STRESS=1
			#	APP_NAME=stress-ng
			#	;;
			--governors-measurement)
				ACTION="GOVERNORS_MEASUREMENT"
				;;
			--perf-paramenter)
				ACTION="PERF_MEASUREMENT"
				;;
			--power-measurement)
				ACTION="POWER_MEASUREMENT"
				;;
			--roi-measurement)
				ROI_MEASUREMENT=true
				;;	
			-s|--sanity)				
				ACTION="SANITY_TEST"
				;;				
			-d|--debug)
				DEBUG=true
				;;
			-r|--runs)
				shift
				NUM_RUNS=$1
				;;
			--save-results) #test if the folder already exist
				shift
				RESULT_FOLDER="$1"
				;;
			#--target-app)
			#	shift
			#	APP_NAME="$1"
			#	;;				
			--input-parameter)
				shift
				IN_PARAMETER="$1"
				;;
			--configs-list)
				shift
				CONFIG_LIST="$1"
				;;
			--parse-parameters)
				shift
				PARAMETERS_JSON="$1"
				;;
			-t)
				shift
				STRESS_TIME="$1"
				;;		
			--perf-secs)
				shift
				PERF_SECS="$1"
			;;		
			-h|--help)
				echo "Under construction"
				exit 0
				;;
			*)
				echo "Unknown option: $1"
				exit 1
				;;
		esac
		shift # shift the input argument to the left ./t -a -b -c -> ./t -b -c
	done

	#
	[ "$(whoami)" != "root" ] && \
		echo "Error: Run this script\($0\) as a root user" && exit 1
	#
	if grep "nvidia,tegra186" /proc/device-tree/compatible &>/dev/null; then
		MACHINE="tegra-ubuntu"
		if grep "R32"  /etc/nv_tegra_release &>/dev/null; then KERNEL_VERSION="R32"; fi
	else
		MACHINE="odroid"
		KERNEL_VERSION=$(uname -r)
	fi
	#
	[ -z "$MACHINE" ] || [ -z "$KERNEL_VERSION" ] && \
		echo "Error: Failed to identify platform." && exit 1

	if [ -n "$PARAMETERS_JSON" ]; then
		if [ -f "$PARAMETERS_JSON" ]; then
			parse_json
		else
			echo "Error: File does not exist." && exit 1
		fi
	fi

	#[ -n "$GENERIC_STRESS" ] && [ -n "$APP_NAME" ] && \
	#	echo "Error: You can not use -t and -g at the same time." && exit 1

	#[ -z "$GENERIC_STRESS" ] && [ -z "$APP_NAME" ] && \
	#	echo "Error: You must choose a generic test (-g) or a target application (-target--app)." && exit 1		

	set_variables

	#######
	case "$ACTION" in
		POWER_MEASUREMENT)

			RESULTS_IPE_PATH+="/power_measurement"

			[ -n "$DEBUG" ] && echo "Samplings power measurement"

			[ -z "$NUM_RUNS" ]  && echo "Error: use -r to set number of runs" && exit 1
			[ -z "$RESULT_FOLDER" ] && echo "Error: use -save--results to set result folder" && exit 1
			[ -z "$CONFIG_LIST" ]  && echo "Error: use --configs-list to set a file with a list of configs." && exit 1

			#[ -n "$GENERIC_STRESS" ] && [ -z "$STRESS_TIME" ]  \
			#	&& echo "Error: use -t to set stress duration." && exit 1

			do_configs_list_measurement
		;;
		PERF_MEASUREMENT)
			[ -n "$DEBUG" ] && echo "Perf parameter measurement"

			[ -z "$NUM_RUNS" ]  && echo "Error: use -r to set number of runs" && exit 1
			[ -z "$RESULT_FOLDER" ] && echo "Error: use -save--results to set result folder" && exit 1
			#if [[ "$NUM_RUNS" = ^-?[0-9]+$ ]];then  echo "Error: $NUM_RUNS is not a number"; exit 1; fi it is not working

			do_perf_measurement		
		;;
		GOVERNORS_MEASUREMENT)
			[ -n "$DEBUG" ] && echo "Governors measurement"

			[ -z "$NUM_RUNS" ]  && echo "Error: use -r to set number of runs" && exit 1
			[ -z "$RESULT_FOLDER" ] && echo "Error: use -save--results to set result folder" && exit 1
			#if [[ "$NUM_RUNS" = ^-?[0-9]+$ ]];then  echo "Error: $NUM_RUNS is not a number"; exit 1; fi it is not working
		
			[ -z "$APP_NAME" ] && \
				echo "Error: You must choose a target application (-target--app)." && exit 1		

			do_governors_measurement		
		;;
		NORNIR_MEASURUMENT)
			[ -n "$DEBUG" ] && echo "Nornir measurement"

			[ -z "$NUM_RUNS" ]  && echo "Error: use -r to set number of runs" && exit 1
			[ -z "$RESULT_FOLDER" ] && echo "Error: use -save--results to set result folder" && exit 1
			#if [[ "$NUM_RUNS" = ^-?[0-9]+$ ]];then  echo "Error: $NUM_RUNS is not a number"; exit 1; fi it is not working
		
			[ -z "$APP_NAME" ] && \
				echo "Error: You must choose a target application (-target--app)." && exit 1		

			do_nornir		
		;;		
		SANITY_TEST)
			[ -n "$DEBUG" ] && echo "SANITY_TEST"

			sanity_test
		;;
	esac
}

main "$@" # $@ get all paramenters passed to the script and "send" to the function
exit 0

#./runtime_measurement.sh -g --perf -exec--ops 87555 --runs 1 -save--results perf_test --debug 2>&1 | tee out
#./runtime_measurement.sh -power--measurement -d -t 15 -g -r 1 -save--results teste_power -configs--list ../configurations/teste.data 2>&1 | tee out

