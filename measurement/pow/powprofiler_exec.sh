#!/bin/bash

nvpmodel -m 0 &> /dev/null

NUM_RUNS=$1
CONFIG_LIST=$2
ACTION=$3
TWO_APPS=$4
RESULT_NAME=$5
IFS=', ' read -r -a APP_NAME <<< "$6"
CONFIG_POWPROFILER=$7

GOVERNORS_LIST=("performance" "ondemand" "interactive" "conservative" "schedutil" "powersave")
#APP_NAME=("node" "matrixmulcpu")
POWPROFILER=/home/jetsonuob/Projects/powprofiler

IPER_PATH="/home/jetsonuob/Projects/iperfect"
RESULTS_IPE_PATH="/home/jetsonuob/Projects/hmp_jetson_results/powprofiler"
CHECK_AFF="$IPER_PATH/tools/check_aff.sh"
CHECK_TEMP="$IPER_PATH/tools/check_temp.sh"

echo "0" > "/sys/devices/pwm-fan/temp_control"
echo "255" >"/sys/devices/pwm-fan/target_pwm"


terminate()
{

	[ -n "$DEBUG" ] && echo "terminating..."

	if [ "$1" != "" ]; then
		echo -e "$1"
	fi
	
	[ "$2" != "hotplug" ] && do_hotplug RESET
	[ "$2" != "change_freq" ] && \
		change_freq "$BIG_MIN_F" "$BIG_MAX_F" "$LITTLE_MIN_F" "$LITTLE_MAX_F"
	
	change_temp_limits RESTORE
	turn_fan OFF

	pkill -9 -c -e "check"
	pkill -9 -c -e "sensors"
	#pkill -9 -c -e "watch_roi"

	local app=
	for app in "${APP_NAME[@]}";do pkill -9 -c -e "$app";done

	[ "$MACHINE" == "tegra-ubuntu" ] && nvpmodel -m "$NVP_M"
	do_emc_gpu 'RESTORE'
	exit 0
}

do_emc_gpu()
{

	echo "emc_gpu $1"

	case "$1" in
		MAX)
			
			#EMC#
			local EMC_ISO_CAP="/sys/kernel/nvpmodel_emc_cap/emc_iso_cap"
			local EMC_MAX_FREQ="/sys/kernel/debug/bpmp/debug/clk/emc/max_rate"
			local EMC_UPDATE_FREQ="/sys/kernel/debug/bpmp/debug/clk/emc/rate"
			EMC_FREQ_OVERRIDE="/sys/kernel/debug/bpmp/debug/clk/emc/mrq_rate_locked"
			
			emc_foverride=`cat "${EMC_FREQ_OVERRIDE}"`
			
			local emc_cap=`cat "${EMC_ISO_CAP}"`
			local emc_fmax=`cat "${EMC_MAX_FREQ}"`
			if [ "$emc_cap" -gt 0 ] && [ "$emc_cap" -lt  "$emc_fmax" ]; then
				EMC_MAX_FREQ="${EMC_ISO_CAP}"
			fi
			
			echo 1 > "${EMC_FREQ_OVERRIDE}"
			cat "${EMC_MAX_FREQ}" > "${EMC_UPDATE_FREQ}"

			GPU_MIN_FREQ="/sys/class/devfreq/17000000.gp10b/min_freq"
			GPU_MAX_FREQ="/sys/class/devfreq/17000000.gp10b/max_freq"
			GPU_RAIL_GATE="/sys/class/devfreq/17000000.gp10b/device/railgate_enable"

			gpu_minf=`cat "${GPU_MIN_FREQ}"`
			gpu_maxf=`cat "${GPU_MAX_FREQ}"`
			gpu_railg=`cat "${GPU_RAIL_GATE}"`

			echo 0 > "${GPU_RAIL_GATE}"
			echo "$gpu_maxf" > "${GPU_MIN_FREQ}"

			ret=$?
			if [ ${ret} -ne 0 ]; then
				echo "Error: Failed to max GPU frequency!"
			fi	
			sleep 1
			
			echo "EMC SPEED: "  
			cat $EMC_UPDATE_FREQ
			echo "GPU SPEED: "
			cat $GPU_MIN_FREQ
		;;
		RESTORE)
			echo "${emc_foverride}" > "${EMC_FREQ_OVERRIDE}"

			echo "$gpu_railg" > "${GPU_RAIL_GATE}"
			echo "${gpu_minf}" > "${GPU_MIN_FREQ}"
			ret=$?
			if [ ${ret} -ne 0 ]; then
				echo "Error: Failed to min GPU frequency!"
			fi				
			
		;;
		esac
}

define_core_two_apps()
{
	echo "define two cores"

	local cluster_big_temp=(1 2)
	local cluster_little_temp=(0 3 4 5)

	for big_i in "${!LIST_BIG_CORES[@]}";do
		
		local core_run_by_app=""

	   	for ((i=0; i<LIST_BIG_CORES[$big_i]; i=i+1))
	   	do 		
	    	core_run_by_app+="${cluster_big_temp[$i]}"
	    	core_run_by_app+=","
	   	done   

	   	cluster_big_temp=("${cluster_big_temp[@]:${LIST_BIG_CORES[$big_i]}}")

	   	for ((i=0; i<LIST_LITTLE_CORES[$big_i]; i=i+1))
	   	do
	    	core_run_by_app+="${cluster_little_temp[$i]}"
	    	core_run_by_app+=","
		done   

		cluster_little_temp=("${cluster_little_temp[@]:${LIST_LITTLE_CORES[$big_i]}}")

		core_run_by_app="${core_run_by_app%?}"

		ARRAY_CORE_RUN["$big_i"]=$core_run_by_app
		ARRAY_TOTAL_CORES["$big_i"]=$((LIST_BIG_CORES[$big_i]+LIST_LITTLE_CORES[$big_i]))  	
		
	done
}

define_cores_list()
{
   
   CORE_RUN=
   local BIG_CORES=$1
   local LITTLE_CORES=$2  

    local CLUSTER_B_CORES=(1 2)
	local CLUSTER_L_CORES=(0 3 4 5)

   echo "BIG LITTLE CORES: $BIG_CORES - $LITTLE_CORES" 

   TOTAL_CORES=$(( BIG_CORES+LITTLE_CORES ))

   [ -n "$DEBUG" ] && echo "TOTAL_CORES $TOTAL_CORES"

   local i=
   for ((i=0; i<BIG_CORES; i=i+1))
   do 
      CORE_RUN+="${CLUSTER_B_CORES[$i]}"
      CORE_RUN+=","
   done   

   for ((i=0; i<LITTLE_CORES; i=i+1))
   do 
     
      CORE_RUN+="${CLUSTER_L_CORES[$i]}"
      CORE_RUN+=","
   done   

   CORE_RUN="${CORE_RUN%?}" # removes last character

	ARRAY_CORE_RUN[0]=$CORE_RUN
	ARRAY_TOTAL_CORES[0]=$TOTAL_CORES    

   [ -n "$DEBUG" ] && echo "$BIG_CORES $LITTLE_CORES -> $CORE_RUN"
}

change_freq()
{
	echo "[$# variables] Change frequency"
	local g=

	[ -n "$DEBUG" ] && echo "$@"

	if [ $# == 3 ]; then
		cpufreq-set -d "$1" -u "$2" -c "$3"    -g performance &> /dev/null
		[ $? -ne 0 ] &&  \
			echo "Error: Problem with cpufreq-set. Is it installed?" "change_freq" && exit 0
	elif [ $# -ge 4 ]; then

		if [ $# == 5 ]; then
			g="$5"
		else
			g="performance"
		fi

		cpufreq-set -d "$1" -u "$2" -c 1    -g $g &> /dev/null
		cpufreq-set -d "$3" -u "$4" -c 0 -g $g &> /dev/null
		[ $? -ne 0 ] && \
			echo "Error: Problem with cpufreq-set. Is it installed?" "change_freq" && exit 0
	else
		echo "$# Wrong number of arguments $#" "change_freq" && exit 0
	fi

	cpufreq-info -o
}

create_result_path(){

	local i=

	result_path_n="$RESULTS_IPE_PATH/pareto/$RESULT_NAME/"

	for i in "${!APP_NAME[@]}";do
		result_path_n+="${APP_NAME["$i"]}"
		result_path_n+="_"
	done

	#if [ "$TWO_APPS" != "true" ];then
	#	result_path_n="${result_path_n%?}" # removes last character
	#	result_path_n+="/"
	#fi

	for i in "${!LIST_BIG_CORES[@]}";do
		result_path_n+="${LIST_BIG_CORES["$i"]}"
		result_path_n+="_"
		result_path_n+="${LIST_LITTLE_CORES["$i"]}"
		result_path_n+="_"
	done

	result_path_n="${result_path_n%?}" # removes last character
	result_path_n+="/${BIG_SELECT}_${LITTLE_SELECT}/Run_$1"

	mkdir -v -p "$result_path_n"
}

do_governors()
{
	echo "Governor"
	do_emc_gpu MAX

	for g in "${GOVERNORS_LIST[@]}";do
		
		cpufreq-set -c 0 -g "$g"
		cpufreq-set -c 1 -g "$g"
		cpufreq-info -o

		for i in $(seq 1 "$NUM_RUNS");do
			echo "Run $i/$NUM_RUNS"

			result_path_n="$RESULTS_IPE_PATH/governor/$RESULT_NAME"

			#result_path_n+="${APP_NAME[0]}"
			#if [ "$TWO_APPS" == "true" ];then 
			#	result_path_n+="_"
			#	result_path_n+="${APP_NAME[1]}"
			#fi
			
			#result_path_n+="/governors/${g}"

			mkdir -v -p "$result_path_n"

			$CHECK_AFF "${APP_NAME[*]}" "1.5" &> "$result_path_n/aff_$i.csv" & #
			pid_aff=$! 	

			echo "executing.."
			t1=$(date +'%s%N')
			powprofile "$CONFIG_POWPROFILER" &> "$result_path_n/out_$i.log"
			t2=$(date +'%s%N')

			echo -e "Time\t$t1\t$t2" >> "$result_path_n/twoApps_benchmark.csv"	

			test=$(( NUM_RUNS+1 ))

			if [ "$i" = 1 ];then
				tail "$POWPROFILER/profilers/twoApps.csv" >> "$result_path_n/twoApps.csv"
			elif [ "$i" -lt "$test"  ]; then
				tail -n1 "$POWPROFILER/profilers/twoApps.csv" >> "$result_path_n/twoApps.csv"
			fi
			
			#mv "$POWPROFILER/twoApps.csv" "$result_path_n/twoApps.csv"
			rm "$POWPROFILER/profilers/twoApps.csv"

			kill $pid_aff &> /dev/null
			disown &> /dev/null	
		done
	done
	
	change_freq "345600" "2035200" "345600" "2035200"

	do_emc_gpu RESTORE
}

do_pareto()
{
	echo "Pareto"
	do_emc_gpu MAX

	while read -r line
	do

		if [ "$TWO_APPS" = "true" ];then
		    LIST_BIG_CORES[0]=$line
		    read -r line
			LIST_LITTLE_CORES[0]=$line
			read -r line
			LIST_BIG_CORES[1]=$line
			read -r line
			LIST_LITTLE_CORES[1]=$line
			read -r line
			BIG_FREQS[0]=$line			
			read -r line
			LITTLE_FREQS[0]=$line
			read -r line
			BIG_FREQS[1]=$line			
			read -r line
			LITTLE_FREQS[1]=$line	

			define_core_two_apps
			
			for core_i in "${!ARRAY_CORE_RUN[@]}";do 
				cset set -c "${ARRAY_CORE_RUN[$core_i]}" -s "cpuset$core_i"
			done

			cset set -l	
					
		else
			echo ""
		    LIST_BIG_CORES[0]=$line
		    read -r line
			LIST_LITTLE_CORES[0]=$line
			read -r line
			BIG_FREQS[0]=$line
			read -r line
			LITTLE_FREQS[0]=$line

			define_cores_list "${LIST_BIG_CORES[0]}" "${LIST_LITTLE_CORES[0]}"		

			cset shield -c "$CORE_RUN" -k on --force
		fi		

		echo "LIST_BIG_CORES=${LIST_BIG_CORES[@]}"
		echo "LIST_LITTLE_CORES=${LIST_LITTLE_CORES[@]}"
		echo "BIG_FREQS=${BIG_FREQS[@]}"
		echo "LITTLE_FREQS=${LITTLE_FREQS[@]}"

		IFS=', ' read -r -a big_freq_array <<< "${BIG_FREQS[0]}"
		IFS=', ' read -r -a little_freq_array <<< "${LITTLE_FREQS[0]}"

		for freq_index in "${!big_freq_array[@]}";
		do
			LITTLE_SELECT=${little_freq_array[freq_index]}
			BIG_SELECT=${big_freq_array[freq_index]}

			echo "LITTLE_SELECT $LITTLE_SELECT BIG_SELECT $BIG_SELECT"
			#Frequencies of appB is the same of appA
			change_freq "$BIG_SELECT" "$BIG_SELECT" "$LITTLE_SELECT" "$LITTLE_SELECT"

			sleep 1

			for i in $(seq 1 "$NUM_RUNS");do
				echo "Run $i/$NUM_RUNS"

				create_result_path "$i"	

				$CHECK_AFF "${APP_NAME[*]}" "1.5" &> "$result_path_n/aff.csv" & #
				pid_aff=$! 	

				$CHECK_TEMP "tegra-ubuntu" "4.1" "1.5" &> "$result_path_n/temp.csv" &
				pid_temp=$!		

				echo "executing.."
				t1=$(date +'%s%N')
				powprofile "$CONFIG_POWPROFILER" &> "$result_path_n/out.log"
				t2=$(date +'%s%N')

				echo -e "Time\t$t1\t$t2" > "$result_path_n/twoApps_benchmark.csv"			
				mv "$POWPROFILER/profilers/twoApps.csv" "$result_path_n/twoApps.csv"

				kill $pid_aff &> /dev/null
				disown &> /dev/null

				kill $pid_temp &> /dev/null
				disown &> /dev/null		
			done
		done

		if [ "$TWO_APPS" = true ];then
			for i in "${!ARRAY_CORE_RUN[@]}";do
				cset set -d "cpuset$i"
			done				
		else
			cset shield --reset --force 
		fi

	done < "$CONFIG_LIST"

	change_freq "345600" "2035200" "345600" "2035200"

	do_emc_gpu RESTORE

	echo "END" 
}

date

if [ "$ACTION" == "pareto" ];then
	do_pareto
else
	do_governors
fi

echo "1" > "/sys/devices/pwm-fan/temp_control"
echo "0" >"/sys/devices/pwm-fan/target_pwm"

date
