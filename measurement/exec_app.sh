#!/bin/bash

APP_EXEC="$1"
APP_NAME="$2"
RESULT_FILE="$3"
declare -x -a TASKSETS
declare -x -a THREADS
declare -x -a ROOTS
#declare -x -a OUT_FILE

PLACES_STRING=$7

export OMP_PLACES="$PLACES_STRING"
export OMP_PROC_BIND=true

IFS=' ' read -r -a TASKSETS <<< "$4"
IFS=' ' read -r -a THREADS <<< "$5"
IFS=' ' read -r -a ROOTS <<< "$6"
#IFS=' ' read -r -a OUT_FILE <<< "$7"

[ -n "$DEBUG" ] && echo "OMP_PROC_BIND=$OMP_PROC_BIND"
[ -n "$DEBUG" ] && echo "OMP_PLACES=$OMP_PLACES"
[ -n "$DEBUG" ] && echo "THREADS=$THREADS"
[ -n "$DEBUG" ] && echo "TASKSETS=$TASKSETS"
[ -n "$DEBUG" ] && echo "ROOTS=$ROOTS"
[ -n "$DEBUG" ] && echo "APP_EXEC=$APP_EXEC"
#[ -n "$DEBUG" ] && echo "OUT_FILE=$OUT_FILE"

echo "Running $APP_NAME"

t1=$(date +'%s%N')
eval "$APP_EXEC" &> "${RESULT_FILE}/${APP_NAME}.log"
t2=$(date +'%s%N')
echo "End - $APP_NAME"

echo -e "Time\t$t1\t$t2" > "$RESULT_FILE/${APP_NAME}_benchmark.csv"